"""
This module contains all the utility functions and UDFs used in the spark job.
"""
import argparse
import json
from typing import List

import happybase
import numpy as np
import pandas as pd
import psycopg2
from psycopg2.extensions import AsIs
from psycopg2.extensions import connection as Connection
from pyspark.sql import DataFrame
from pyspark.sql.functions import udf, when, size, split, col
from pyspark.sql.types import ArrayType, StringType, BinaryType


def get_parser():
    """
    Parse arguments from command line for running spark job.
    """
    parser = argparse.ArgumentParser(description="Process arguments for spark job")
    parser.add_argument(
        "-pf",
        "--path_from",
        required=True,
        help="Path to HDFS folder from where fill will be read",
    )
    parser.add_argument(
        "-ht",
        "--hdfs_to",
        required=False,
        help="Path to HDFS folder where fill will be saved",
    )
    parser.add_argument(
        "-phost",
        "--postgres_host",
        required=True,
        help="Postgres host",
    )
    parser.add_argument(
        "-pport",
        "--postgres_port",
        required=True,
        help="Postgres port",
    )
    parser.add_argument(
        "-pdb",
        "--postgres_db",
        required=True,
        help="Postgres database name",
    )
    parser.add_argument(
        "-pu",
        "--postgres_user",
        required=True,
        help="Postgres user name",
    )
    parser.add_argument(
        "-pp",
        "--postgres_password",
        required=True,
        help="Postgres password",
    )
    parser.add_argument(
        "-rt", "--resource_type", required=True, help="Type of resource to process"
    )
    parser.add_argument(
        "-bp", "--bert_path", required=True, help="Path to bert model in HDFS"
    )
    parser.add_argument(
        "-mp",
        "--mapping_path",
        required=True,
        help="Path to csv file with mapping in HDFS",
    )
    parser.add_argument(
        "-ut",
        "--user_tables",
        required=True,
        help="List of user tables for which resource will be updated. In form of aaa,bbb,ccc",
    )
    parser.add_argument(
        "-hs",
        "--hbase_server",
        required=True,
        help="HBase server address",
    )
    parser.add_argument(
        "-hp",
        "--hbase_port",
        required=True,
        help="HBase server port",
    )
    parser.add_argument(
        "-db",
        "--database_used",
        required=True,
        help="Databases that are used for processing",
    )
    parser.add_argument(
        "-mt",
        "--metadata_table",
        required=True,
        help="Metadata table name",
    )
    return parser


def to_binary(data):
    """
    Convert to binary numpy array
    """
    return np.array(data).astype("float32").tobytes()


def load_json(text: str) -> List[str]:
    """
    Load json from string
    """
    return json.loads(text)


def dump_json(text: List[str]) -> str:
    """
    Dump json to string
    """
    if text:
        return json.dumps(text)
    return json.dumps([])


def string_set(string: str) -> str:
    """
    Get string with unique values separated by comma
    """
    return ",".join(list(set(str(string).split(","))))


convert_udf = udf(to_binary, BinaryType())
load_json_udf = udf(load_json, ArrayType(StringType()))
dump_json_udf = udf(dump_json, StringType())
string_set_udf = udf(string_set, StringType())


def distinct_words(string: str) -> List[str]:
    """
    Get list of distinct words from string
    """
    return list(set(str(string).split(" ")))


def generate_mapper_udf(broadcast_mapping):
    """
    Generate udf for mapping words
    """

    def mapper(desc: str) -> str:
        """
        Map words in text to according to some mapping
        """
        if desc:
            return " ".join(
                broadcast_mapping.value.get(word, word) for word in desc.split()
            )
        return " "

    return udf(mapper, StringType())


def add_is_valid_column(df_resource: DataFrame) -> DataFrame:
    """
    Add column isvalid to dataframe.
    """
    set_udf = udf(distinct_words, ArrayType(StringType()))

    cond1 = col("authors").isin(["Test", "test", "Bla-Bla", "bla-bla"])

    cond2 = col("mainTitles").isNull()

    cond3_sub0 = size(split(col("mainTitles"), " ")) < 2
    cond3_sub1 = col("mainTitles") == col("descriptions")
    cond3_sub2 = col("mainTitles") == col("keywordsMerged")
    cond3_sub3 = col("keywordsMerged") == col("descriptions")

    cond3 = (
        (cond3_sub0 & cond3_sub1)
        | (cond3_sub0 & cond3_sub2)
        | (cond3_sub0 & cond3_sub3)
    )

    cond4_sub0 = (
        col("mainTitles").isNull().cast("int")
        + col("keywordsMerged").isNull().cast("int")
        + col("descriptions").isNull().cast("int")
    ) >= 2
    cond4_sub1 = size(split(col("mainTitles"), " ")) < 7

    cond4 = cond4_sub0 & cond4_sub1

    cond5 = (col("mainTitles") == col("descriptions")) & col("keywordsMerged").isNull()

    cond6 = (
        col("descriptions").isNull()
        & (size(split(col("keywordsMerged"), " ")) <= 2)
        & (size(split(col("mainTitles"), " ")) < 7)
    )

    cond7 = (size(set_udf(col("descriptions"))) <= 2) & (
        size(set_udf(col("mainTitles"))) <= 2
    )

    return df_resource.withColumn(
        "isvalid",
        when(cond1 | cond2 | cond3 | cond4 | cond5 | cond6 | cond7, False).otherwise(
            True
        ),
    )


def save_metadata(
    job,
) -> None:
    """
    Save metadata data to postgres
    """
    resource_name = job.resource_type
    url = job.postgres_settings.url
    host, port = url.split("/")[2].split(":")
    database = url.split("/")[3].split("?")[0]

    postgres_table_name = job.table_name

    conn = psycopg2.connect(
        host=host,
        port=port,
        database=database,
        user=job.postgres_settings.user,
        password=job.postgres_settings.password,
    )
    row_count = count_rows(conn, postgres_table_name)
    isvalid_count = count_isvalid(conn, postgres_table_name)

    single_update_sql(conn, resource_name, row_count, isvalid_count)


def count_isvalid(conn: Connection, table_name: str) -> int:
    """
    Count number of valid rows in table
    """
    cur = conn.cursor()
    query = "SELECT COUNT(*) FROM %s WHERE isvalid = TRUE"
    params = (AsIs(table_name),)
    cur.execute(query, params)
    return cur.fetchone()[0]


def count_rows(conn: Connection, table_name: str) -> int:
    """
    Count number of rows in table
    """
    cur = conn.cursor()
    query = "SELECT COUNT(*) FROM %s"
    params = (AsIs(table_name),)
    cur.execute(query, params)
    return cur.fetchone()[0]


def single_update_sql(
    conn: Connection, resource_type: str, row_count: int, isvalid_count: int
):
    """
    Update metadata table in postgres
    """
    cur = conn.cursor()
    time = pd.Timestamp.today().date()
    query = """UPDATE metadata.metadata_resources
                SET "last update" = '{}', "row count" = '{}', "isvalid count" = '{}'
                WHERE "name"='{}'; """
    cur.execute(query.format(time, row_count, isvalid_count, resource_type))
    conn.commit()


def spark_df_to_hbase(spark_df, table_name, hbase_server, hbase_port):
    """
    Function that stores spark dataframe into hbase
    """
    df_rdd = spark_df.rdd
    df_rdd.foreachPartition(
        lambda partition: write_to_hbase(
            partition, hbase_server, hbase_port, table_name
        )
    )


def spark_df_delete_from_hbase(spark_df, table_name, hbase_server, hbase_port):
    """
    Function that deletes spark dataframe into hbase
    """
    df_rdd = spark_df.rdd
    df_rdd.foreachPartition(
        lambda partition: delete_from_hbase(
            partition, hbase_server, hbase_port, table_name
        )
    )


def spark_df_change_resource_id_hbase(spark_df, table_name, hbase_server, hbase_port):
    """
    Function that changes resource id in hbase
    """
    df_rdd = spark_df.rdd
    df_rdd.foreachPartition(
        lambda partition: change_resource_id_hbase(
            partition, hbase_server, hbase_port, table_name
        )
    )


def write_to_hbase(partition, hbase_server, hbase_port, hbase_table_name):
    """
    Function that stores spark partition into hbase
    """

    def put(table, row):
        """
        Function that stores spark row into hbase table
        """
        row_key = row["id"]
        data = {
            "embeddings:embeddings": bytes(row.embeddings),
            "resource_info:categories": json.dumps(row.categories),
            "resource_info:bestaccessright": json.dumps(row.bestaccessright),
            "resource_info:isvalid": json.dumps(str(row.isvalid)),
            "resource_info:relations": json.dumps(str(row.relations)),
            "resource_info:language_prediction": json.dumps(
                str(row.language_prediction)
            ),
        }
        table.put(row_key, data)

    connection = happybase.Connection(hbase_server, port=hbase_port)
    hbase_table = connection.table(hbase_table_name)
    for df_row in partition:
        try:
            put(hbase_table, df_row)
        except Exception:
            try:
                connection = happybase.Connection(hbase_server, port=hbase_port)
                hbase_table = connection.table(hbase_table_name)
                put(hbase_table, df_row)
            except Exception as error:
                raise error


def delete_from_hbase(partition, hbase_server, hbase_port, hbase_table_name):
    """
    Function that deletes spark partition from hbase
    """
    connection = happybase.Connection(hbase_server, port=hbase_port)
    hbase_table = connection.table(hbase_table_name)
    for row in partition:
        try:
            hbase_table.delete(row["id"])
        except Exception:
            try:
                connection = happybase.Connection(hbase_server, port=hbase_port)
                hbase_table = connection.table(hbase_table_name)
                hbase_table.delete(row["id"])
            except Exception as error:
                raise error


def change_resource_id_hbase(partition, hbase_server, hbase_port, hbase_table_name):
    """
    Function that changes resource id in hbase
    """

    def exchange_row(table, row):
        """
        Take row from table and put it back with changed row_key
        """
        row_data = table.row(row["id"])
        if row_data:
            row_key = row["addedid"]
            data = {
                "embeddings:embeddings": row_data[b"embeddings:embeddings"],
                "resource_info:categories": row_data[b"resource_info:categories"],
                "resource_info:bestaccessright": row_data[
                    b"resource_info:bestaccessright"
                ],
                "resource_info:isvalid": row_data[b"resource_info:isvalid"],
                "resource_info:relations": row_data[b"resource_info:relations"],
                "resource_info:language_prediction": row_data[
                    b"resource_info:language_prediction"
                ],
            }
            table.put(row_key, data)

    connection = happybase.Connection(hbase_server, port=hbase_port)
    hbase_table = connection.table(hbase_table_name)
    for df_row in partition:
        try:
            exchange_row(hbase_table, df_row)
        except Exception:
            try:
                connection = happybase.Connection(hbase_server, port=hbase_port)
                hbase_table = connection.table(hbase_table_name)
                exchange_row(hbase_table, df_row)
            except Exception as error:
                raise error


columns_to_drop = [
    "mainTitles",
    "descriptions",
    "keywordsMerged",
    "targetGroups",
    "resourceTypes",
    "levelOfExpertise",
    "accessModes",
    "doi",
    "publicationDate",
    "contentTypes",
    "accessTypes",
    "authors",
    "usageCountsDownloads",
    "usageCountsViews",
    "relationsExtended",
    "publicationDate",
    "resourceType",
    "tags",
    "keywords",
    "isOpenAccess",
    "document",
    "sentence",
    "languages",
]

columns_to_drop_2 = [
    "joined_column",
    "document",
    "sentence",
    "token",
    "embeddings_raw",
    "sentence_embeddings",
    "finished_embeddings",
]

columns_to_select = [
    "id",
    "embeddings",
    "categories",
    "bestaccessright",
    "isvalid",
    "relations",
    "language_prediction",
]


class DatabaseSettings:
    """
    Settings for postgres
    """

    def __init__(
        self,
        url: str = None,
        user: str = None,
        password: str = None,
        port: str = None,
        is_used: bool = False,
        metadata_table: str = None,
    ):
        self.url = url
        self.user = user
        self.password = password
        self.port = port
        self.is_used = is_used
        self.metadata_table = metadata_table
