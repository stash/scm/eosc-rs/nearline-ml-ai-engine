"""
Embedder used for tests.
"""
import numpy as np


class NullEmbedder:
    """
    Embedder used for tests. Can only return a zero vector.
    """

    def __init__(self):
        self.name = "NullEmbedder"

    @staticmethod
    def generate_embeddings(string, to_gpu=False):
        """
        Generate embeddings for a string. Returns a zero vector.
        """
        return np.zeros(256)
