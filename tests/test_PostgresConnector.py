"""
Module for testing PostgresConnector class.
"""
import json
import unittest
from unittest.mock import MagicMock

import numpy as np
import pandas as pd

from nearline_app.database_connectors.PostgresConnector import PostgresConnector
from nearline_app.utils import get_empty_user


class TestPostgresConnector(unittest.TestCase):
    """
    Test class, which tests the PostgresConnector class
    """

    def setUp(self):
        """
        Setup function, which is called before each test
        """
        self.connection = MagicMock()
        self.cursor = MagicMock()
        self.metadata = [
            ["datasets", "datasets"],
            ["publications", "publications"],
            ["services", "services"],
        ]
        self.maps = {
            "datasets": "datasets",
            "publications": "publications",
            "services": "services",
        }
        self.discovered_values = {"args": [], "sql": []}

    def setup_postgres_connector(self):
        """
        Function, which setups PostgresConnector object.
        """
        self.connection.cursor.return_value = self.cursor
        postgres_connector = PostgresConnector(self.connection)
        postgres_connector.current_tables = self.maps
        return postgres_connector

    def test_get_metadata(self):
        """
        Test case for the get_metadata function.
        Check if function do not destroy tabel.
        """
        self.cursor.fetchall.return_value = self.metadata
        self.connection.cursor.return_value = self.cursor
        postgres_connector = PostgresConnector(self.connection)
        metadata = postgres_connector.current_tables

        self.assertEqual(metadata, self.maps)

    def test_get_embeddings(self):
        """
        Test case for the get_embeddings function.
        Check if function returns correct embeddings.
        """
        table_name = "datasets"
        resources_id = ["50|doi_________::014e12fad82df5d98476bf67851d5bcc"]
        self.cursor.fetchall.return_value = pd.DataFrame.from_dict(
            {
                "id": ["50|doi_________::014e12fad82df5d98476bf67851d5bcc"],
                "embeddings": [np.ones((256,)).astype("float32").tobytes()],
            }
        )

        postgres_connector = self.setup_postgres_connector()
        result = postgres_connector.get_embeddings(table_name, resources_id)

        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.iloc[0]["embeddings"].shape[0], 256)

    def test_get_embeddings_with_languages(self):
        """
        Test case for the get_embeddings function.
        Check if function returns correct embeddings.
        """
        table_name = "datasets"
        resources_id = ["50|doi_________::014e12fad82df5d98476bf67851d5bcc"]
        self.cursor.fetchall.return_value = pd.DataFrame.from_dict(
            {
                "id": ["50|doi_________::014e12fad82df5d98476bf67851d5bcc"],
                "embeddings": [np.ones((256,)).astype("float32").tobytes()],
                "language": ["en"],
            }
        )

        postgres_connector = self.setup_postgres_connector()
        result = postgres_connector.get_embeddings_with_languages(
            table_name, resources_id
        )

        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.iloc[0]["embeddings"].shape[0], 256)
        self.assertEqual(result.iloc[0]["language"], "en")

    def test_get_not_existing_embeddings(self):
        """
        Test case for the get_embeddings function. Check if function returns None,
        if embeddings are not in database.
        """
        table_name = "datasets"
        resources_id = ["50|doi_________::014e12fad82df5d98476bf67851d5bgg"]
        self.cursor.fetchall.return_value = pd.DataFrame()
        postgres_connector = self.setup_postgres_connector()
        result = postgres_connector.get_embeddings(table_name, resources_id)
        self.assertEqual(result, None)

    def test_get_not_existing_table(self):
        """
        Test case for the get_table function. Check if function returns None,
        if table is not in database.
        """
        table_name = "not_existing_table"
        resources_id = ["50|doi_________::014e12fad82df5d98476bf67851d5bcc"]
        self.cursor.fetchall.return_value = pd.DataFrame()
        postgres_connector = self.setup_postgres_connector()
        result = postgres_connector.get_embeddings(table_name, resources_id)
        self.assertEqual(result, None)

    def test_single_update_user(self):
        """
        Test case for the update_user function.
        """
        self.cursor.execute.side_effect = self.discover
        postgres_connector = self.setup_postgres_connector()
        postgres_connector.single_update_user(
            "table_name", "user_id", "column", "value"
        )
        self.assertEqual(
            self.discovered_values["args"],
            ["metadata_resources", "table_name", "column", "value", "user_id"],
        )
        self.assertEqual(
            self.discovered_values["sql"][1:],
            ["""UPDATE users.%s SET "%s" = %s WHERE id = \'%s\';"""],
        )

    def test_update_user(self):
        """
        Test case for the update_user function.
        """
        self.cursor.execute.side_effect = self.discover
        postgres_connector = self.setup_postgres_connector()
        update_data = {
            "id": "test_user_2",
            "new_data": json.dumps(["123"]),
            "new_data_column": "data_source:visited",
        }
        postgres_connector.update_user("table_name", update_data)
        self.assertEqual(
            self.discovered_values["args"],
            [
                "metadata_resources",
                "table_name",
                "data_source:visited",
                '["123"]',
                "test_user_2",
            ],
        )
        self.assertEqual(
            self.discovered_values["sql"][1:],
            ["""UPDATE users.%s SET "%s" = %s  WHERE id = \'%s\';"""],
        )

    def test_update_user_with_embeddings(self):
        """
        Test case for the update_user function.
        """
        self.cursor.execute.side_effect = self.discover
        postgres_connector = self.setup_postgres_connector()
        update_data = {
            "id": "test_user_2",
            "new_data": json.dumps(["123"]),
            "new_data_emb": "123",
            "new_data_column": "data_source:visited",
            "new_data_emb_column": "data_source_emb:visited",
        }
        postgres_connector.update_user_with_embeddings("table_name", update_data)
        self.assertEqual(
            self.discovered_values["args"],
            [
                "metadata_resources",
                "table_name",
                "data_source:visited",
                '["123"]',
                "data_source_emb:visited",
                "123",
                "test_user_2",
            ],
        )
        self.assertEqual(
            self.discovered_values["sql"][1:],
            ["""UPDATE users.%s SET "%s" = %s, "%s" = %s  WHERE id = \'%s\';"""],
        )

    def test_insert_new_user_with_embeddings(self):
        """
        Test, if the insert_new_user and _new_user_query() functions works correctly.
        """
        self.cursor.execute.side_effect = self.discover
        postgres_connector = self.setup_postgres_connector()
        insert_data = {
            "id": "user_id",
            "new_data": json.dumps(["123"]),
            "new_data_emb": "123",
            "new_data_column": "column",
            "new_data_emb_column": "column_emb",
        }
        postgres_connector.insert_new_user_with_embeddings("table_name", insert_data)
        self.assertEqual(
            self.discovered_values["args"][2:48], list(get_empty_user().keys())
        )
        self.assertEqual(
            self.discovered_values["args"][48:50], ["column", "column_emb"]
        )
        self.assertEqual(
            self.discovered_values["args"][-2:], [json.dumps(["123"]), "123"]
        )
        self.assertEqual(
            self.discovered_values["sql"][1:],
            [postgres_connector._new_user_query()],
        )

    def test_insert_new_user(self):
        """
        Test, if the insert_new_user and _new_user_query() functions works correctly.
        """
        self.cursor.execute.side_effect = self.discover
        postgres_connector = self.setup_postgres_connector()
        insert_data = {
            "id": "user_id",
            "new_data": json.dumps(["123"]),
            "new_data_column": "column",
        }
        postgres_connector.insert_new_user("table_name", insert_data)
        self.assertEqual(
            self.discovered_values["args"][2:48], list(get_empty_user().keys())
        )
        self.assertEqual(self.discovered_values["args"][48], "column")
        self.assertEqual(self.discovered_values["args"][-1], json.dumps(["123"]))
        self.assertEqual(
            self.discovered_values["sql"][1:],
            [postgres_connector._new_user_query()],
        )

    def test_insert_new_empty_user(self):
        """
        Test, if the insert_new_empty_user function works correctly.
        """
        self.cursor.execute.side_effect = self.discover
        postgres_connector = self.setup_postgres_connector()

        postgres_connector.insert_new_empty_user("table_name", "123")
        self.assertEqual(self.discovered_values["args"][48], "123")

    def test_get_user(self):
        """
        Test case for the get_user function.
        """
        temp = pd.DataFrame.from_dict({"id": ["user_id"], "column": ["value"]})
        self.cursor.fetchone.return_value = temp
        self.cursor.description = [("id", "text"), ("column", "text")]
        postgres_connector = self.setup_postgres_connector()
        result = postgres_connector.get_user("user_id", "table_name")
        self.assertEqual(result["id"], "id")
        self.assertEqual(result["column"], "column")
        self.cursor.fetchone.return_value = None
        result = postgres_connector.get_user("non_exists", "table_name")
        self.assertEqual(result, None)

    # def test_health_check(self):
    #     """
    #     Test case for the health_check function.
    #     """
    #     postgres_connector = self.setup_postgres_connector()
    #     postgres_connector.get_list_of_tables_from_schema = MagicMock()
    #     postgres_connector.get_list_of_tables_from_schema.return_value = (
    #         pd.DataFrame.from_dict(
    #             {"a": [settings.METADATA_TABLE, "datasets", "publications", "services"]}
    #         )
    #     )
    #     postgres_connector.check_if_correct_schema = MagicMock()
    #     postgres_connector.check_if_correct_schema.return_value = "OK"
    #
    #     response = postgres_connector.health_check()
    #     self.assertEqual(
    #         response,
    #         {
    #             "status": "UP",
    #             "database type": "PostgreSQL",
    #             "schema": "OK",
    #             "tables": {
    #                 "metadata": True,
    #                 "datasets": True,
    #                 "publications": True,
    #                 "services": True,
    #             },
    #         },
    #     )
    #
    #     postgres_connector.get_list_of_tables_from_schema.side_effect = Exception
    #     response = postgres_connector.health_check()
    #     self.assertEqual(response["status"], "DOWN")
    #     self.assertEqual(response["error"], "Error while connecting to Postgres:")

    def test_update_service(self):
        """
        Test case for the update_service function.
        """
        self.cursor.execute.side_effect = self.discover
        postgres_connector = self.setup_postgres_connector()
        postgres_connector.update_service("service_id", "column", b"123")
        self.assertEqual(
            self.discovered_values["args"][2:], ["column", b"123", "service_id"]
        )

    def test_insert_new_service(self):
        """
        Test case for the insert_new_service function.
        """
        self.cursor.execute.side_effect = self.discover
        postgres_connector = self.setup_postgres_connector()
        postgres_connector.insert_new_service("service_id", "column", b"123")
        self.assertEqual(
            self.discovered_values["args"][2:],
            ["service_id", "column", b"123", True],
        )

    def test_get_list_of_tables_from_schema(self):
        """
        Test case for the get_list_of_tables_from_schema function.
        """
        self.cursor.execute.side_effect = self.discover
        postgres_connector = self.setup_postgres_connector()
        postgres_connector.get_list_of_tables_from_schema("schema")
        self.assertEqual(
            self.discovered_values["args"], ["metadata_resources", "schema"]
        )

    def discover(self, sql: str, args: tuple) -> None:
        """
        Function, for discovering sql and args, which are passed to
        cursor.execute function.
        """
        for arg in args:
            if (
                not isinstance(arg, str) and not isinstance(arg, int)
            ) and not isinstance(arg, bytes):
                self.discovered_values["args"].append(arg.getquoted().decode("utf-8"))
            else:
                self.discovered_values["args"].append(arg)
        self.discovered_values["sql"].append(sql)

    def test_try_connect(self):
        """
        Test case for the try_connect function.
        """
        postgres_connector = self.setup_postgres_connector()
        postgres_connector.set_postgres_connection = MagicMock()
        a = postgres_connector.try_connect()
        self.assertEqual(a, True)

        # self.cursor.fetchone.side_effect = Exception
        # postgres_connector = self.setup_postgres_connector()
        # postgres_connector.set_postgres_connection = MagicMock()
        # a = postgres_connector.try_connect()
        # print(a)
