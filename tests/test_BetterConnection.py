import happybase
from thriftpy2.transport import TTransportException

from nearline_app.database_connectors.BetterConnection import BetterConnection
from tests.HbaseMocks import HBaseTestCase, MockConnectionPool
from tests.utils_mocks import ErrorMock, MultiVisitFunction


class TestBetterConnection(HBaseTestCase):
    def setUp(self):
        super().setUp()
        self.connection = happybase.Connection()
        self.table = self.connection.table("table-name")

        def temp(itself):
            if not itself.connection_pool:
                itself.connection_pool = MockConnectionPool(2)

        BetterConnection.connect = temp
        self.test_connection = BetterConnection("host", 123)

    def test_table(self):
        table = self.test_connection.table("table-name")
        self.assertEqual(table, self.table)

    def test_raising_exceptions(self):

        args = [
            {"table_name": "table-name", "row_id": "123"},
            {"table_name": "table-name", "rows_ids": ["123"]},
            {
                "table_name": "table-name",
                "row_id": "123",
                "insert_data": {"arg": "kwarg"},
            },
        ]
        self.test_connection.connect = MultiVisitFunction("create_user")
        for idx, function in enumerate(
            [
                self.test_connection.row,
                self.test_connection.rows,
                self.test_connection.put,
            ]
        ):
            self.test_connection.connect = MultiVisitFunction("create_user")
            self.test_connection.table = ErrorMock(TTransportException)
            function(**args[idx])
            self.assertEqual(self.test_connection.connect.flag, 1)
            self.assertEqual(self.test_connection.table.counter, 2)

            self.test_connection.table = ErrorMock(Exception("error"))
            function(**args[idx])
            self.assertEqual(self.test_connection.connect.flag, 2)
            self.assertEqual(self.test_connection.table.counter, 2)
