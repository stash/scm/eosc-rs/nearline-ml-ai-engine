"""
Module for testing KafkaRunner class.
"""
import asyncio
import io
import logging
import unittest
from unittest.mock import MagicMock

import confluent_kafka
import nest_asyncio

from nearline_app.kafka_runner import KafkaRunner
from nearline_app.settings import settings
from tests.HbaseMocks import MockConnection
from tests.KafkaMocks import ConsumerMock, Message
from tests.utils_mocks import sample_topics

nest_asyncio.apply()


class KafkaTestCase(unittest.IsolatedAsyncioTestCase):
    """
    Test class for KafkaRunner. It mocks the Kafka and HBase connections.
    """

    def setUp(self):
        """
        Mocks the Kafka and HBase connections.
        """
        confluent_kafka.oldConsumer = confluent_kafka.Consumer
        confluent_kafka.Consumer = ConsumerMock
        MockConnection().flush()

    def tearDown(self):
        """
        Restores the Kafka and HBase original connections classes.
        """
        confluent_kafka.Consumer = confluent_kafka.oldConsumer


class KafkaMockTest(KafkaTestCase):
    """
    Test class. KafkaRunner needs HBase connection to work.
    """

    async def asyncSetUp(self):
        """
        Creates KafkaRunner instance.
        """
        super(KafkaMockTest, self).setUp()
        self.consumer = confluent_kafka.Consumer()
        self.dynamic_switch = MagicMock()
        self.runner = KafkaRunner(
            consumer=self.consumer, dynamic_switch=self.dynamic_switch
        )

        self.log_stream = io.StringIO()
        self.logger = logging.getLogger("nearline_app.user_entity.KafkaRunner")
        self.old_handlers = self.logger.handlers[:]

        handler = logging.StreamHandler(self.log_stream)
        handler.setFormatter(
            logging.Formatter("%(asctime)s [%(levelname)s] %(message)s")
        )
        self.logger.handlers = [handler]

    async def test_consumer(self):
        """
        Test if KafkaRunner is consuming messages from Kafka
        """
        messages = [
            b'{"id": 1, "name": "test"}',
            b'{"id": 2, "name": "test2"}',
            b'{"id": 3, "name": "test3"}',
        ]
        for message in messages:
            sample_topics[0].append(Message(message, settings.KAFKA_TOPIC_UA))

        asyncio.create_task(self.runner.start())
        for message in messages:
            sample_topics[0].append(Message(message, settings.KAFKA_TOPIC_UA))
        await asyncio.sleep(2)
        for data in sample_topics[0]:
            self.assertFalse(data.was_read)

    async def test_consumer_idle(self):
        """
        Check if KafkaRunner works, even if there is no messages in Kafka
        """
        asyncio.create_task(self.runner.start())
        await asyncio.sleep(2)
        self.assertTrue(self.runner.paused)

    async def test_consumer_stop(self):
        """
        Check if KafkaRunner stop works. For endpoint testing.
        """
        messages = [
            b'{"id": 1, "name": "test"}',
            b'{"id": 2, "name": "test2"}',
            b'{"id": 3, "name": "test3"}',
        ]
        asyncio.create_task(self.runner.start())

        response = self.runner.stop()
        self.assertFalse(response)
        for message in messages:
            sample_topics[0].append(Message(message, settings.KAFKA_TOPIC_UA))
        await asyncio.sleep(2)
        response = self.runner.stop()
        self.assertTrue(response)

    async def test_consumer_restart(self):
        """
        Check if KafkaRunner restart works. For endpoint testing.
        """
        asyncio.create_task(self.runner.start())
        response = self.runner.restart()
        self.assertEqual(response, "Not paused")
        self.runner.paused = True
        response = self.runner.restart()
        self.assertEqual(response, "Cannot stop, no last message")
        self.runner.last_message = Message(
            b'{"id": 1, "name": "test"}', settings.KAFKA_TOPIC_UA
        )
        response = self.runner.restart()
        self.assertEqual(response, "Restarted")

    async def test_health_check(self):
        """
        Check if KafkaRunner health check works
        """
        asyncio.create_task(self.runner.start())
        response = self.runner.health_check()
        self.assertEqual(response, {"status": "UP", "last_read": None})

    async def test_process_message(self):
        self.runner.switches = [MagicMock()]
        message = Message(b'{"id": 1, "name": "test"}', settings.KAFKA_TOPIC_UA)
        output = self.runner.process_message(message)
        self.assertTrue(output)
