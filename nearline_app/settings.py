from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    """
    Description for variables are in READMe.md
    """

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"

    POSTGRES_HOST: str = ""
    POSTGRES_DATABASE: str = ""
    POSTGRES_USER: str = ""
    POSTGRES_PORT: str = "5432"
    POSTGRES_PASSWORD: str = ""

    METADATA_TABLE: str = "metadata_resources"

    ANON_USERS_TABLE: str = "anon_users"
    LOGGED_USERS_TABLE: str = "logged_users"
    LOGGED_USERS_AAI_TABLE: str = "logged_users_aai"

    KAFKA_SERVER: str = ""
    KAFKA_TOPIC_UA: str = "eosc-dev"
    KAFKA_TOPIC_USER: str = "sync-user-dev"
    KAFKA_TOPIC_SERVICE: str = "sync-service-dev"
    KAFKA_TOPIC_TRAINING: str = "training-sync-default"
    KAFKA_LIKE_TOPIC: str = "recommendations-evaluation"

    KAFKA_GROUP_ID: str = "foo"
    KAFKA_AUTO_OFFSET_RESET: str = "latest"

    PREPROCESSOR_DIAG_ENDPOINT: str = ""

    HBASE_SERVER: str = ""
    HBASE_PORT: str = "7710"
    HBASE_METADATA: str = "metadata_resource"

    DATABASE_STORAGE: str = "POSTGRES"
    MODEL_PATH: str = "nearline_app/embedders/resources/best_ckpt.pt"

    LOG_LEVEL: str = "WARNING"
    PROJECT_NAME: str = "nearline_engine"

    NUMBER_OF_PROCESS_SWITCHES: int = 1
    NUMBER_OF_STORING_SWITCHES: int = 1

    SPARK_EXECUTOR_CORES: str = "3"
    SPARK_DRIVER_CORES: str = "3"
    SPARK_NUM_EXECUTORS: str = "2"
    SPARK_EXECUTOR_MEMORY: str = "3g"
    SPARK_DRIVER_MEMORY: str = "3g"
    SPARK_EXECUTOR_MEMORY_OVERHEAD: str = "3g"
    SPARK_DRIVER_MEMORY_OVERHEAD: str = "3g"

    SPARK_COMMAND: str = "/opt/spark/bin/spark-submit"  # spark-submit
    SPARK_PYTHON_ENVIRONMENT: str = ""
    SPARK_BERT_PATH: str = ""
    SPARK_JARS_FOLDER: str = ""
    SPARK_MAPPING_PATH: str = ""

    TEST_VAR: str = "old"


def get_settings(path_to_env: str = None):
    return Settings(_env_file=path_to_env)


settings = Settings(_env_file=".env")

tags_metadata = [
    {
        "name": "embedding",
        "description": "Endpoint used for creating embeddings",
    }
]
