"""
Some utility functions and constant data, that are used in the tests.

sample_topics is very important.
"""
from nearline_app.utils import fill_data

input_data = {
    "sessionId": "sample_session_id",
    "userId": "sample_user_id",
    "isAnonymous": True,
    "numberOfUA": 2,
    "userActions": [
        {
            "timestamp": "2022-08-31 09:15:44.431",
            "pageId": "/datasets/example-dataset",
            "resourceType": "dataset",
            "resourceId": "50|doi_________::042d3a5949413ee3c13ea6c50736d1e3",
            "visitType": "regular",
            "rootType": "other",
            "actionType": "browser action",
            "isOrderAction": False,
        },
        {
            "timestamp": "2022-08-31 09:46:53.587",
            "pageId": "/services/b2find",
            "resourceType": "service",
            "resourceId": "5",
            "visitType": "regular",
            "rootType": "other",
            "actionType": "browser action",
            "isOrderAction": False,
        },
    ],
}

switch_output = {
    "isAnonymous": False,
    "resourceId": "5",
    "isOrderAction": False,
    "resourceType": "service",
    "sessionId": "sample_session_id",
    "aaiUid": "test_aai_uid",
    "marketplaceId": "test_marketplace_id",
}

sample_topics = [[]]


def get_user_data():
    """
    mock function to get user data after parsing from kafka message
    """
    user_data = {
        "isAnonymous": True,
        "resourceId": "84",
        "isOrderAction": False,
        "resourceType": "service",
        "sessionId": "sample_session_id",
    }
    return user_data


def create_user(user_id):
    """
    mock function to create a user from a kafka message
    """
    data = get_user_data()
    data["user"] = "test_user_" + str(user_id)

    return fill_data(data)


def create_user_clean(user_id):
    """
    mock function to create a user
    """
    if user_id:
        data = {"user": "test_user_" + str(user_id)}
        return fill_data(data)

    return fill_data({})


class SingleVisitFunction:
    """
    Class, which mocks the function, that is called when a message is received
    """

    def __init__(self, unique_name):
        self.name = unique_name
        self.flag = False
        self.arg1 = None
        self.arg2 = None
        self.arg3 = None
        self.arg4 = None

    def __call__(self, arg1, arg2=None, arg3=None, arg4=None):
        self.flag = True
        self.arg1 = arg1
        self.arg2 = arg2
        self.arg3 = arg3
        self.arg4 = arg4

    def __str__(self):
        return self.name


class MultiVisitFunction:
    """
    Class, which mocks the function, that is called when a message is received multiple times
    """

    def __init__(self, unique_name):
        self.name = unique_name
        self.flag = 0
        self.arg1 = []
        self.arg2 = []
        self.arg3 = []
        self.arg4 = []

    def __call__(self, arg1="", arg2="", arg3="", arg4=""):
        self.flag += 1
        self.arg1.append(arg1)
        self.arg2.append(arg2)
        self.arg3.append(arg3)
        self.arg4.append(arg4)

    def __str__(self):
        return self.name


class ErrorMock:
    def __init__(self, error):
        self.error = error
        self.counter = 0

    def __call__(self, name):
        self.counter += 1
        raise self.error
