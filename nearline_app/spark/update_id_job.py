"""
This module contains class UpdateIdJob, which is responsible for updating rows id in the table.
"""
# pylint: disable=E0401

from psycopg2.extensions import AsIs
from pyspark.sql import DataFrame
from pyspark.sql.functions import col, explode, coalesce, collect_list

try:
    from deletion_job import DeletionJob
    from udf_and_utils import (
        load_json_udf,
        dump_json_udf,
        spark_df_change_resource_id_hbase,
    )

except ImportError:
    from nearline_app.spark.deletion_job import DeletionJob
    from nearline_app.spark.udf_and_utils import (
        load_json_udf,
        dump_json_udf,
        spark_df_change_resource_id_hbase,
    )


class UpdateIdJob(DeletionJob):
    """
    Job that updates rows id in the table.
    """

    def __init__(self, job_context, user_tables):
        super().__init__(job_context)
        self.user_tables = user_tables.split(",")
        self.columns = [
            self.job_context.resource_type + ":ordered",
            self.job_context.resource_type + ":visited",
            "dislike:" + self.job_context.resource_type,
            "like:" + self.job_context.resource_type,
        ]

    def get_already_in(self, df_map: DataFrame) -> list:
        """
        Get ids that are already in the resource table
        """
        ids_to_check = tuple(df_map.rdd.map(lambda x: x.addedid).collect())
        query = "SELECT * FROM %s where id in %s"
        params = (
            AsIs(self.job_context.table_name),
            ids_to_check,
        )
        out = self.job_context.perform_query_with_return(query, params)
        return list({t[0] for t in out})

    def change_resources_id_postgres(self, df_map: DataFrame) -> None:
        """
        Change ids in the resource table. Load dataframe that maps old
        ids to new ids to postgres, and then perform update query.
        """
        if self.job_context.postgres_settings.is_used:
            self.job_context.spark_df_to_postgres(
                df_map, "overwrite", "resources.exchange_id"
            )
            update_query = """  UPDATE %s
                                SET id = resources.exchange_id.addedid
                                FROM resources.exchange_id
                                WHERE %s.id = resources.exchange_id.id"""

            params = (
                AsIs(self.job_context.table_name),
                AsIs(self.job_context.table_name),
            )
            self.job_context.perform_query(update_query, params)

    def change_resources_id_hbase(self, df_map: DataFrame) -> None:
        """
        Change ids in the resource hbase table.
        """
        if self.job_context.hbase_settings.is_used:
            spark_df_change_resource_id_hbase(
                df_map,
                self.job_context.table_name,
                self.job_context.hbase_settings.url,
                int(self.job_context.hbase_settings.port),
            )

    def update_users_profile(self, df_map: DataFrame) -> None:
        """
        Update users profile for each user table defined in self.user_tables
        """
        for user_table_name in self.user_tables:
            df_user = self.get_part_user_table(user_table_name)
            df_user = self.update_user_profile(df_user, df_map)
            self.update_user_table(df_user, user_table_name)

    def update_user_profile(self, df_user: DataFrame, df_map: DataFrame) -> DataFrame:
        """
        For each column we load json, replace old ids with new ids according to df_map,
        and dump json back
        """
        for column in self.columns:
            df_user = df_user.withColumn(column, load_json_udf(col(column)))
            df_user_exploded = df_user.withColumn("single_id", explode(col(column)))
            updated_ids = df_user_exploded.join(
                df_map, df_user_exploded.single_id == df_map.id, "left_outer"
            ).select(
                df_user_exploded["id"],
                coalesce(col("addedid"), col("single_id")).alias("updated_id"),
            )
            final_table = updated_ids.groupBy("id").agg(
                collect_list("updated_id").alias(column)
            )
            df_user = df_user.drop(column).join(final_table, ["id"], "left")
            df_user = df_user.withColumn(column, dump_json_udf(col(column)))

        return df_user

    def get_part_user_table(self, user_table_name: str) -> DataFrame:
        """
        Get part of user table (columns of resource that we are interested in)
        that contains at least one resource id
        """
        query = """SELECT id, "%s", "%s", "%s", "%s" FROM users.%s
                                        WHERE ("%s" <> '[]')
                                            OR ("%s" <> '[]')
                                            OR ("%s" <> '[]')
                                            OR ("%s" <> '[]')"""
        params = (
            AsIs(self.columns[0]),
            AsIs(self.columns[1]),
            AsIs(self.columns[2]),
            AsIs(self.columns[3]),
            AsIs(user_table_name),
            AsIs(self.columns[0]),
            AsIs(self.columns[1]),
            AsIs(self.columns[2]),
            AsIs(self.columns[3]),
        )
        _, cursor = self.job_context.get_cursor()
        query = cursor.mogrify(query, params).decode("utf-8")

        return self.job_context.spark_read_sql(query)

    def update_user_table(self, df_user: DataFrame, user_table_name: str) -> None:
        """
        Update user table by replacing old ids with new ids
        (these ids represents the same resources)

        Parameters
        df_user: dataframe with user table
        user_table_name: name of the user table
        """
        self.job_context.spark_df_to_postgres(df_user, "overwrite", "users.temp_users")

        update_query = """UPDATE users.%s
                                    SET "%s" = users.temp_users."%s",
                                        "%s" = users.temp_users."%s",
                                        "%s" = users.temp_users."%s",
                                        "%s" = users.temp_users."%s"
                                    FROM users.temp_users
                                    WHERE users.%s.id = users.temp_users.id
                                """
        params = (
            AsIs(user_table_name),
            AsIs(self.columns[0]),
            AsIs(self.columns[0]),
            AsIs(self.columns[1]),
            AsIs(self.columns[1]),
            AsIs(self.columns[2]),
            AsIs(self.columns[2]),
            AsIs(self.columns[3]),
            AsIs(self.columns[3]),
            AsIs(user_table_name),
        )

        self.job_context.perform_query(update_query, params)

    def make_exchange_dataset(self, exchange_path: str) -> DataFrame:
        """
        Make exchange dataset from exchange file. Rename columns
        """
        return (
            self.job_context.spark_read_csv(exchange_path)
            .select("deletedId", "addedId")
            .withColumnRenamed("deletedId", "id")
            .withColumnRenamed("addedId", "addedid")
        )

    def launch(self, exchange_path: str):
        """
        Launch the update of id job
        """
        self.job_context.logger.info("Start Updating rows ids")

        exchange_dataset = self.make_exchange_dataset(exchange_path)

        unique = exchange_dataset.dropDuplicates(subset=["addedid"])
        to_delete = exchange_dataset.subtract(unique)

        already_in = self.get_already_in(unique)

        unique_to_delete = unique.filter(unique.addedid.isin(already_in))
        to_delete = to_delete.union(unique_to_delete)

        self.delete_from_dataframe_postgres(to_delete)
        self.delete_from_dataframe_hbase(to_delete)

        unique_to_exchange = unique.subtract(unique_to_delete)
        self.change_resources_id_postgres(unique_to_exchange)
        self.change_resources_id_hbase(unique_to_exchange)

        self.update_users_profile(exchange_dataset)
