"""
Base class for embedding text.
"""
import os

import numpy as np
import torch
from transformers import AutoModel, AutoTokenizer

os.environ["TOKENIZERS_PARALLELISM"] = "false"


class BaseEmbedder:
    """
    Class for embedding text.
    """

    def __init__(self, model_name_url="prajjwal1/bert-mini", to_gpu=False):
        self.model = None
        self.tokenizer = None
        self.model_name_url = model_name_url
        self.to_gpu = to_gpu
        # self.load_model(self.model_name_url, to_gpu=False)

    def load_model(self) -> None:
        """
        Loads a model and a tokenizer from a given model name.
        """
        self.model = AutoModel.from_pretrained(self.model_name_url)
        # print("Is cuda available:", torch.cuda.is_available())

        if self.to_gpu:
            device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
            self.model = self.model.to(device)

        self.tokenizer = AutoTokenizer.from_pretrained(self.model_name_url)

    def generate_embeddings(self, string):
        """
        string can be a list of strings or a single string
        """

        inputs = self.tokenizer(
            string, return_tensors="pt", max_length=512, padding=True, truncation=True
        )
        if self.to_gpu:
            inputs.to(torch.device("cuda" if torch.cuda.is_available() else "cpu"))
        outputs = self.model(**inputs)
        if self.to_gpu:
            return outputs[1].detach().cpu().numpy()

        return outputs[1].detach().numpy()

    def generate_embeddings_for_df(self, df, column):
        """
        Generates embeddings for a given column of a dataframe.
        """
        df[column] = df[column].replace(np.nan, " ")
        return df[column].apply(lambda x: self.generate_embeddings(x)[0])
