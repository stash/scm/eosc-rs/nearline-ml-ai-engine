Script for generating user tables is runnable from main catalogs using following command:

```
python initialize.py \
        --anon test_users_likes \
        --logged test_users_likes \
        --logged-aai test_users_likes
```

Script for getting data from Kafka topic is runnable from main catalogs using following command:

```
python -m app.user_entity.scripts.get_data_from_kafka_topic -t artificial-likes
```