"""
Mocks for KafkaRunner test.
"""
import time

from tests.utils_mocks import sample_topics


class Error:
    """
    Mock for Error class for Kafka.
    """

    def __init__(self, error_value):
        self.error_value = error_value

    def code(self):
        """
        Returns error code.
        """
        return self.error_value

    def __str__(self):
        return str(self.error_value)

    def __repr__(self):
        return str(self.error_value)


class Message:
    """
    Mock for Message class for Kafka.
    """

    def __init__(self, value, topic, error=None):
        self.data = value
        self.was_read = False
        self.error_obj = Error(error) if error else None
        self.topic_name = topic
        self.partition_id = 0
        self.last_offset = 0

    def error(self):
        """
        Returns error object.
        """
        if self.error_obj:
            return self.error_obj

        return None

    def value(self):
        """
        Returns message value.
        """
        self.was_read = True
        return self.data

    def topic(self):
        """
        get message topic name
        """
        return self.topic_name

    def partition(self):
        """
        get message partition
        """
        return self.partition_id

    def offset(self):
        """
        get message offset
        """
        return self.last_offset


class MockTopic:
    """
    It is completely useless, but it is needed for the code to work
    """

    def __init__(self, name):
        self.data = sample_topics[0]
        self.name = name

    def get_data(self):
        """
        Returns data from topic.
        """
        return self.data

    def get_name(self):
        """
        Returns name of topic.
        """
        return self.name


class MockTopicList:
    """
    Mock for TopicPartition class for Kafka.
    """

    def __init__(self):
        self.topics = {}

    def get(self, name):
        """
        Returns topic of a given name.
        """
        return self.topics.get(name)

    def add(self, name, data):
        """
        Adds topic of a given name.
        """
        self.topics[name] = data


class ConsumerMock:
    """
    Mock class for KafkaConsumer.
    """

    _instance = None
    topics = MockTopicList()

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(ConsumerMock, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self, *_args, **_kwargs):
        self.current_topic = None

    def subscribe(self, name):
        """
        Make the mock consumer subscribe to a topic.
        """
        topic = self.topics.get(name[0])
        if not topic:
            topic = MockTopic(name[0])
            self.topics.add(name[0], topic)

        self.current_topic = topic

    def poll(self, timeout=0):
        """
        Returns the oldest message from the current topic.
        """
        if len(self.current_topic.data) == 0:
            time.sleep(timeout)
            return None
        data = self.current_topic.data.pop(0)
        return data

    @staticmethod
    def close():
        """
        Closes the mock consumer.
        """
        return None

    def list_topics(self):
        """
        Returns the list of topics.
        """
        return self.topics

    def pause(self, _):
        """
        Pause the mock consumer.
        """
        return self.current_topic

    def resume(self, _):
        """
        Resume the mock consumer.
        """
        return self.current_topic
