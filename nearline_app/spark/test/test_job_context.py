"""
Functions to test the job_context module.
"""
import os
import sys

import pytest
from pyspark.sql import SparkSession

from nearline_app.spark.test.utils import (
    get_spark_session,
    get_path_to_test_files,
    setup_job_context,
)
from nearline_app.spark.udf_and_utils import add_is_valid_column

os.environ["PYSPARK_PYTHON"] = sys.executable
os.environ["PYSPARK_DRIVER_PYTHON"] = sys.executable


@pytest.fixture(scope="session", name="spark_session")
def fixture_spark_session(request) -> SparkSession:
    """
    Fixture for creating a SparkSession
    """
    return get_spark_session(request)


@pytest.mark.usefixtures("spark_session")
def test_spark_read_csv(spark_session):
    """
    Test the spark_read_csv function.
    """
    job_context = setup_job_context(spark_session)
    file_path = "file:///" + get_path_to_test_files() + r"\test_data.csv"

    test_df = job_context.spark_read_csv(file_path)
    assert test_df.count() > 0


@pytest.mark.usefixtures("spark_session")
def test_add_is_valid_column(spark_session):
    """
    Test the spark_read_csv function.
    """
    job_context = setup_job_context(spark_session)
    file_path = "file:///" + get_path_to_test_files() + r"\test_data.csv"

    test_df = job_context.spark_read_csv(file_path)
    test_df = add_is_valid_column(test_df)
    test_df = test_df.select("isvalid")
    results = test_df.collect()
    # all correct
    assert results[0]["isvalid"]
    # no title
    assert 1 - results[1]["isvalid"]
    # no description
    assert 1 - results[2]["isvalid"]
    # title == description and keywordsMerged is null
    assert 1 - results[3]["isvalid"]
    # title == description and keywordsMerged is not null
    assert results[4]["isvalid"]
    # author is "Test"
    assert 1 - results[5]["isvalid"]
    # very long title but short description
    assert results[6]["isvalid"]
    # title = egi egi; desc = guides guides tutorials
    assert 1 - results[7]["isvalid"]
