import json
from unittest.mock import MagicMock
import logging

import happybase
import numpy as np

from nearline_app.database_connectors.HBaseConnector import HBaseConnector
from nearline_app.utils import clean_data_test
from tests.HbaseMocks import HBaseTestCase, MockConnectionPool

logging.disable(logging.CRITICAL)


class HBaseConnectorTestCase(HBaseTestCase):
    def setUp(self):
        """
        Set up method. Creates HBase object.
        """
        super().setUp()

        self.connection = happybase.Connection()
        table = self.connection.table("metadata_resource")
        self.connection.table("services")
        table.put(b"services", {b"table_name:table_name": b"services"})

        def res(self):
            if not self.hbase_connection:
                self.hbase_connection = MockConnectionPool(2)

        HBaseConnector.open_pool = res
        self.HBaseConnector = HBaseConnector(clean_data_test)

    def test_get_user(self):
        # user = create_user_clean(1)
        insert_data = {
            "id": "test_user_1",
            "new_data": json.dumps(["123"]),
            "new_data_emb": np.ones((256,)).astype("float32").tobytes(),
            "new_data_column": "data_source:visited",
            "new_data_emb_column": "data_source_emb:visited",
        }
        output_user = self.HBaseConnector.get_user("test_user_1", "user_data")
        self.assertEqual(output_user, None)

        self.HBaseConnector.insert_new_user("user_data", insert_data)
        output_user = self.HBaseConnector.get_user("test_user_1", "user_data")
        self.assertEqual(
            output_user[insert_data["new_data_column"]], insert_data["new_data"]
        )

    def test_insert_new_user(self):
        insert_data = {
            "id": "test_user_2",
            "new_data": json.dumps(["123"]),
            "new_data_emb": "123",
            "new_data_column": "data_source:visited",
            "new_data_emb_column": "data_source_emb:visited",
        }
        self.HBaseConnector.insert_new_user("user_data", insert_data)
        table = self.connection.table("user_data")
        data = list(table.scan())
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0][0], b"test_user_2")
        self.assertEqual(
            data[0][1][insert_data["new_data_column"]], insert_data["new_data"]
        )

    def test_update_service(self):
        self.HBaseConnector.current_tables["services"] = "resource_data"
        self.HBaseConnector.update_service("123", json.dumps(["1"]), bytes(12))
        table = self.connection.table("resource_data")
        data = list(table.scan())
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0][0], b"123")
        self.assertEqual(data[0][1]["resource_info:categories"], json.dumps(["1"]))

    def test_insert_new_service(self):
        self.HBaseConnector.current_tables["services"] = "resource_data"
        self.HBaseConnector.insert_new_service("321", json.dumps(["1"]), bytes(12))
        table = self.connection.table("resource_data")
        data = list(table.scan())
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0][0], b"321")
        self.assertEqual(data[0][1]["embeddings:embeddings"], bytes(12))

    def test_insert_new_empty_user(self):
        self.HBaseConnector.insert_new_empty_user("user_data", "123")
        table = self.connection.table("user_data")
        data = list(table.scan())
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0][0], b"123")
        self.assertEqual(data[0][1]["software:ordered"], json.dumps([]))

    def test_single_update_user(self):
        self.HBaseConnector.insert_new_empty_user("user_data", "123")
        table = self.connection.table("user_data")
        data = list(table.scan())
        self.assertEqual(data[0][1]["software:ordered"], json.dumps([]))
        self.HBaseConnector.single_update_user(
            "user_data", "123", "software:ordered", json.dumps(["456"])
        )
        self.assertEqual(data[0][1]["software:ordered"], json.dumps(["456"]))

    def test_update_user(self):
        self.HBaseConnector.insert_new_empty_user("user_data", "test_user_2")
        table = self.connection.table("user_data")
        data = list(table.scan())
        self.assertEqual(data[0][1]["software:ordered"], json.dumps([]))
        update_data = {
            "id": "test_user_2",
            "new_data": json.dumps(["123"]),
            "new_data_emb": "123",
            "new_data_column": "data_source:visited",
            "new_data_emb_column": "data_source_emb:visited",
        }
        self.HBaseConnector.update_user("user_data", update_data)
        self.assertEqual(data[0][1]["data_source:visited"], json.dumps(["123"]))

    def test_get_embeddings(self):
        # single, multi, none, with language
        self.HBaseConnector.current_tables["services"] = "resource_data"
        self.HBaseConnector.insert_new_service(
            "1_service", json.dumps(["1"]), np.ones((256,)).astype("float32").tobytes()
        )
        self.HBaseConnector.insert_new_service(
            "2_service", json.dumps(["2"]), np.ones((256,)).astype("float32").tobytes()
        )
        df_1 = self.HBaseConnector.get_embeddings("services", ["1_service"])
        self.assertEqual(df_1.shape[0], 1)
        self.assertEqual(df_1.iloc[0]["embeddings"].shape[0], 256)
        df_2 = self.HBaseConnector.get_embeddings(
            "services", ["1_service", "2_service"]
        )
        self.assertEqual(df_2.shape[0], 2)
        df_0 = self.HBaseConnector.get_embeddings("services", ["0_service"])
        self.assertEqual(df_0, None)

        df_lang = self.HBaseConnector.get_embeddings_with_languages(
            "services", ["1_service"]
        )
        self.assertEqual(df_lang.iloc[0][1], "en")

    def test_get_current_tables(self):
        self.assertEqual(self.HBaseConnector.current_tables["services"], "services")

    def test_health_check(self):
        """
        Test case for the health_check function.
        """
        response = self.HBaseConnector.health_check()
        self.assertEqual(response, {"status": "UP", "tables": {"services": True}})
        self.HBaseConnector.hbase_connection.tables = MagicMock()
        self.HBaseConnector.hbase_connection.tables.side_effect = Exception
        response = self.HBaseConnector.health_check()
        self.assertEqual(response["status"], "DOWN")
        self.assertEqual(response["error"], "Error while connecting to Hbase:")
