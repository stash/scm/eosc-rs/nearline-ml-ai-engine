"""
This module contain a function (maybe in future functions) that starts a spark job on the cluster (in yarn mode).

Spark job do not see application environments, so all needed variables are passed as arguments to spark-submit command.

In addition, places from spark takes python environment or jars with dependencies can be set.

To have any control over spark job in case when there is no access to cluster, all logs are saved
in a folder called log.
"""
import logging
import subprocess  # nosec
import time
from datetime import datetime

from nearline_app.settings import settings

logger = logging.getLogger(__name__)

PYTHON_FILES = "spark/job.py,spark/logger_provider.py,spark/udf_and_utils.py,spark/update_content_job.py,"
PYTHON_FILES += "spark/adding_job.py,spark/update_id_job.py,spark/deletion_job.py"


def start_subprocess(spark_submit_str: str, start_time) -> None:
    """
    Function that starts spark job as a subprocess.
    """
    logfile = open("log/cmd.log", "w+", encoding="utf-8")
    logfile.write(spark_submit_str)

    process = subprocess.Popen(
        spark_submit_str,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True,
        shell=True,
    )  # nosec

    now = datetime.now()
    log_name = str(now.strftime("%d-%m-%Y_%H-%M-%S"))
    stdout, stderr = process.communicate()

    if process.returncode != 0:
        logfile = open(f"log/stderr_{log_name}.log", "w+")
        for line in stderr:
            logfile.write(line)

    logfile = open(f"log/stdout_{log_name}.log", "w+")
    for line in stdout:
        logfile.write(line)

    total_time = time.time() - start_time
    logger.info("Total time: %s", total_time)
    logfile.write(f"Total time: {total_time}")

    logger.info("end logging")


def start_spark_job_diff(path_from: str, resource_type: str) -> None:
    """
    Function, that starts differential spark job.

    Command `spark_submit_str` decomposition:
        --master yarn -> master is an url for cluster (in our case it is in yarn)
        --deploy-mode cluster -> deploy application on a worker nodes
        --name -> name of the application
        --executor-cores -> count of executor cores
        --driver-cores -> number of driver cores
        --num-executors -> number of executors
        --executor-memory -> available memory per executor
        --driver-memory -> available memory for driver
        --conf spark.*.extraJavaOptions -> need because of cluster settings
        --conf spark.yarn.dist.archives -> path to python environment on hdfs
        --conf spark.yarn.appMasterEnv.PYSPARK_PYTHON -> some needed constant path
        --jars -> paths to jars with spark libraries
        --py-files -> additional python files that will be attached to applications
    """
    start_time = time.time()

    resource_type_name = resource_type[0].upper() + resource_type[1:]
    users = (
        settings.ANON_USERS_TABLE
        + ","
        + settings.LOGGED_USERS_TABLE
        + ","
        + settings.LOGGED_USERS_AAI_TABLE
    )
    jars = f"hdfs://{settings.SPARK_JARS_FOLDER}/postgresql-42.5.4.jar,"
    jars += f"hdfs://{settings.SPARK_JARS_FOLDER}/spark-nlp-assembly-5.0.1.jar "

    java_executor_options = "-XX:+UseG1GC -XX:+PrintGCDetails"

    spark_submit_str = f"""{settings.SPARK_COMMAND} --master yarn --deploy-mode cluster \
                           --name "{resource_type_name} embedding generation" \
                           --executor-cores {settings.SPARK_EXECUTOR_CORES} \
                           --driver-cores {settings.SPARK_DRIVER_CORES} \
                           --num-executors {settings.SPARK_NUM_EXECUTORS} \
                           --executor-memory {settings.SPARK_EXECUTOR_MEMORY} \
                           --driver-memory {settings.SPARK_DRIVER_MEMORY} \
                           --conf spark.network.timeout=1080s \
                           --conf spark.yarn.maxAppAttempts=1 \
                           --conf "spark.executor.extraJavaOptions={java_executor_options}" \
                           --conf spark.driver.extraJavaOptions="-Divy.cache.dir=/tmp -Divy.home=/tmp" \
                           --conf spark.yarn.dist.archives=hdfs://{settings.SPARK_PYTHON_ENVIRONMENT}#environment \
                           --conf spark.yarn.appMasterEnv.PYSPARK_PYTHON=./environment/bin/python \
                           --conf spark.driver.memoryOverhead={settings.SPARK_DRIVER_MEMORY_OVERHEAD} \
                           --conf spark.executor.memoryOverhead={settings.SPARK_EXECUTOR_MEMORY_OVERHEAD} \
                           --jars {jars} \
                           --py-files {PYTHON_FILES} spark/runner_diff.py \
                           -pf {path_from} \
                           -phost {settings.POSTGRES_HOST} -pport {settings.POSTGRES_PORT} \
                           -pdb {settings.POSTGRES_DATABASE} \
                           -pu {settings.POSTGRES_USER} -pp {settings.POSTGRES_PASSWORD} -rt {resource_type} \
                           -bp {settings.SPARK_BERT_PATH} -mp {settings.SPARK_MAPPING_PATH} \
                           -ut {users} -hs {settings.HBASE_SERVER} -hp {settings.HBASE_PORT} \
                           -db {settings.DATABASE_STORAGE} -mt {settings.METADATA_TABLE}"""

    start_subprocess(spark_submit_str, start_time)
