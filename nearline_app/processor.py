"""
A Module that is Processor for PostGreSql database
"""
import json
import logging
from typing import Dict, Optional, Any, List, Tuple, Union

import numpy as np

from nearline_app.database_connectors.DataBaseConnector import DataBaseConnector
from nearline_app.settings import settings
from nearline_app.utils import (
    UserActionParsed,
    fill_data,
    name_map,
    ParsedServiceEvent,
    UserLikeEvent,
)


class Processor:
    """
    A class which processes user actions and updates selected database

    Attributes:
        anon_users_table (str): name of the table with anonymous users
        logged_users_table (str): name of the table with logged users
                                    with marketplaceId
        logged_users_aai_table (str): name of the table with logged users
                                    with aaiUid
    """

    def __init__(
        self,
        name: str,
        anon_users_table: str = settings.ANON_USERS_TABLE,
        logged_users_table: str = settings.LOGGED_USERS_TABLE,
        logged_users_aai_table: str = settings.LOGGED_USERS_AAI_TABLE,
    ):

        self.database_connector_resource = None
        self.database_connector_user = None
        self.resource_embedder = None
        self.user_embedder = None

        self.name = name

        self.anon_users_table = anon_users_table
        self.logged_users_table = logged_users_table
        self.logged_users_aai_table = logged_users_aai_table

    def setup_processor(
        self,
        database_connector_resource: DataBaseConnector,
        database_connector_user: DataBaseConnector,
        resource_embedder,
        user_embedder,
    ):
        """
        Function, which sets up processor with database connectors
        """
        self.database_connector_resource = database_connector_resource
        self.database_connector_user = database_connector_user
        self.resource_embedder = resource_embedder
        self.user_embedder = user_embedder

    def get_processed_user(self, data: UserActionParsed) -> List:
        """
        Checks whether input data is from
        logged or anon user and creates appropriate user data.

        Arguments:
            data (UserActionParsed): parsed user action
        """
        if data["isAnonymous"]:
            data_copy = data.copy()
            data_copy["user"] = data["sessionId"]
            table_name = self.anon_users_table
        elif "aaiUid" in data:
            data_copy = data.copy()
            data_copy["user"] = data["aaiUid"]
            table_name = self.logged_users_aai_table
        else:
            data_copy = data.copy()
            data_copy["user"] = data["marketplaceId"]
            table_name = self.logged_users_table

        old_data = self.get_user_data(data_copy["user"], table_name)
        if old_data is None:
            user_data = self.get_processed_new_user(data_copy)
            proces_status = "new"
        else:
            user_data = self.get_updated_user(data_copy, old_data)
            proces_status = "update"
        return_data = [(user_data, proces_status, table_name)]
        if "aaiUid" in data and "marketplaceId" in data:
            return_data.append((user_data, proces_status, self.logged_users_table))
        return return_data

    def check_user(self, data: UserActionParsed, table_name: str) -> None:
        """
        Function, which checks whether user is in the database and if not,
        creates new user and if yes, updates its.

        Arguments:
            data (UserActionParsed): parsed user action
            table_name (str): name of the table to which save user
        """
        old_data = self.get_user_data(data["user"], table_name)
        if old_data is None:
            self.proces_new_user(data, table_name)
        else:
            self.update_user(data, old_data, table_name)

    def get_user_data(self, user: str, table_name: str) -> Optional[dict]:
        """
        Function, which gets user data from the database.
        If user not found, returns None.
        Else returns old user data.

        Arguments:
            user (str): user id (marketplaceId, aaiUid or sessionId) about
                            which we want to get data
            table_name (str): name of the table from which we want to get data

        Returns:
            user data or None
        """
        user = self.database_connector_user.get_user(user, table_name)
        if user is None:
            return None
        return user

    def get_processed_new_user(
        self, data: UserActionParsed
    ) -> Union[Dict[str, Any], None]:
        """
        Function, which processes new user data and returns it.
        """
        data = fill_data(data)
        resource_type = name_map[data["resourceType"]]
        order_type = ":ordered" if data["isOrderAction"] else ":visited"
        resources_new = [data["resourceId"]]
        try:
            new_data_emb, _ = self.proces([data["resourceId"]], resource_type)
        except TypeError as exc:
            logging.error(
                f"Error while processing new user: {exc} for {self.name} (main reason for this error is "
                f"problem with hbase connection)"
            )
            return None
        insert_data = {
            "id": data["user"],
            "new_data": json.dumps(resources_new),
            "new_data_emb": new_data_emb,
            "new_data_column": resource_type + order_type,
            "new_data_emb_column": resource_type + "_emb" + order_type,
        }
        return insert_data

    def proces_new_user(self, data: Dict[str, Any], table_name: str) -> None:
        """
        Function create entity for new user.
        First it takes empty user from the database.
        Then updates it with new data.
        Then inserts it to the database.

        Arguments:
            data (Dict[str, Any]): parsed user action
            table_name (str): name of the table to which save user
        """
        insert_data = self.get_processed_new_user(data)
        self.database_connector_user.insert_new_user_with_embeddings(
            table_name, insert_data
        )

    def get_updated_user(
        self, data: UserActionParsed, old_data: Dict[str, Any]
    ) -> Union[Dict[str, Any], None]:
        """
        Function, which processes updated user data and returns it.
        """
        resource_type = name_map[data["resourceType"]]
        order_type = ":ordered" if data["isOrderAction"] else ":visited"
        resources = json.loads(old_data[resource_type + order_type])
        if data["resourceId"] in resources:
            resources = [str(x) for x in resources if x != data["resourceId"]]
            resources.append(str(data["resourceId"]))
        else:
            resources.append(str(data["resourceId"]))

        new_data_emb, resources_found = self.proces(resources, resource_type)
        update_data = {
            "id": data["user"],
            "new_data": json.dumps(resources_found),
            "new_data_emb": new_data_emb,
            "new_data_column": resource_type + order_type,
            "new_data_emb_column": resource_type + "_emb" + order_type,
        }
        return update_data

    def update_user(
        self, data: Dict[str, Any], old_data: Dict[str, Any], table_name: str
    ) -> None:
        """
        Function updates user entity in database.

        Arguments:
            data (Dict[str, Any]): parsed user action
            old_data (Dict[str, Any]): old user data
            table_name (str): name of the table to which save user
        """
        update_data = self.get_updated_user(data, old_data)
        self.database_connector_user.update_user_with_embeddings(
            table_name, update_data
        )

    def proces(
        self, resources_id: List[str], resource_type: str
    ) -> Tuple[bytes, List[str]]:
        if settings.TEST_VAR == "old":
            return self.old_proces(resources_id, resource_type)
        elif settings.TEST_VAR == "future":
            return self.future_proces(resources_id, resource_type)
        else:
            logging.error("Wrong TEST_VAR in settings.py")

    def old_proces(
        self, resources_id: List[str], resource_type: str
    ) -> Tuple[bytes, List[str]]:
        """
        Function, which takes vector embeddings for each resource and returns
        mean of them. If cannot fnd any embeddings, returns vector of zeros.

        Arguments:
            resources_id (List[str]): list of resources ids
            resource_type (str): type of resource (service, product, package)

        Returns:
            bytes: vector embeddings
        """
        resources_id = [str(resource) for resource in list(set(resources_id))]
        df_embeddings = self.database_connector_resource.get_embeddings(
            resource_type, resources_id
        )
        if df_embeddings is None:
            values = self.to_binary([0.0 for _ in range(256)])
            return values, []
        if df_embeddings.shape[0] != len(resources_id):
            logging.warning(
                "For %s processor some resource not found %s, %s. They will be removed.",
                self.name,
                resource_type,
                resources_id,
            )
        founded_resources = [
            resource
            for resource in resources_id
            if resource in list(df_embeddings.index)
        ]
        values = df_embeddings.sum(axis=0) / df_embeddings.shape[0]
        values = values.tolist()
        values = self.to_binary(values)
        return values, founded_resources

    def future_proces(
        self, resources_id: List[str], resource_type: str
    ) -> Tuple[bytes, List[str]]:
        """
        Function, which takes vector embeddings for each resource and returns
        mean of them. If cannot fnd any embeddings, returns vector of zeros.
        It will be used in future user embedding.

        Arguments:
            resources_id (List[str]): list of resources ids
            resource_type (str): type of resource (service, product, package)

        Returns:
            bytes: vector embeddings
        """
        resources_id = [str(resource) for resource in list(set(resources_id))]
        df_embeddings = self.database_connector_resource.get_embeddings_with_languages(
            resource_type, resources_id
        )
        if df_embeddings is None:
            values = self.to_binary([0.0 for _ in range(256)])
            return values, []
        if df_embeddings.shape[0] != len(resources_id):
            logging.warning(
                "For %s processor some resource not found %s, %s. They will be removed.",
                self.name,
                resource_type,
                resources_id,
            )
        founded_resources = [
            resource
            for resource in resources_id
            if resource in list(df_embeddings.index)
        ]
        resource_embeddings = np.stack(df_embeddings["embeddings"].values)
        resource_embeddings = resource_embeddings.reshape(1, 1, 256, -1)
        languages = df_embeddings["language"].tolist()
        values = self.user_embedder.get_user_embedding(resource_embeddings, languages)
        values = values.tolist()

        values = self.to_binary(values)
        return values, founded_resources

    @staticmethod
    def to_binary(data: List[float]) -> bytes:
        """
        Function, which converts list of floats to bytes.
        """
        return np.array(data).astype("float32").tobytes()

    def get_resource_info(
        self, new_data: ParsedServiceEvent, resource_type
    ) -> Tuple[str, str, bytes, str]:
        """
        Update information about service in database.
        """
        desc = self.resource_embedder.generate_embeddings(new_data["description"])
        desc = self.to_binary(desc)

        temp_df = self.database_connector_resource.get_embeddings(
            resource_type, [new_data["id"]]
        )
        if temp_df is None:
            proces_status = "new"
        else:
            proces_status = "update"
        return new_data["id"], json.dumps(new_data["categories"]), desc, proces_status

    def get_user_like(self, data: UserLikeEvent) -> List[Dict[str, Any]]:
        """
        Check to which database save user like action.
        """
        user_likes = []
        if "aaiUid" in data:
            data_copy = data.copy()
            data_copy["user"] = data["aaiUid"]
            user_aai = self.check_user_like(data_copy, self.logged_users_aai_table)
            user_likes.append(user_aai)

        if "uniqueId" in data:
            data_copy = data.copy()
            data_copy["user"] = data["uniqueId"]
            user_marketplace = self.check_user_like(data_copy, self.logged_users_table)
            user_likes.append(user_marketplace)

        return user_likes

    def check_user_like(self, data: UserLikeEvent, table_name: str) -> Dict[str, Any]:
        """
        Check if user who make like action exists; if not create new user
        save it to database; if yes, update it.
        """
        user = self.get_user_data(data["user"], table_name)
        if user is None:
            user = self.proces_new_user_like(data)
            process_status = "new"
        else:
            user = self.update_user_like(data, user)
            process_status = "update"
        return {
            "data": user,
            "table_name": table_name,
            "process_status": process_status,
        }

    @staticmethod
    def proces_new_user_like(data: Dict[str, Any]) -> Dict[str, Any]:
        """
        Create new user in database and save like action.
        """
        data = fill_data(data)
        resource_type = name_map[data["resourceType"]]
        like_type = data["action"] + ":"
        insert_data = {
            "id": data["user"],
            "new_data": json.dumps([data["resourceId"]]),
            "new_data_column": like_type + resource_type,
        }
        return insert_data

    @staticmethod
    def update_user_like(
        data: Dict[str, Any], old_data: Dict[str, Any]
    ) -> Dict[str, Any]:
        """
        Update user in database and save like action.
        """
        resource_type = name_map[data["resourceType"]]
        like_type = data["action"] + ":"

        resources = json.loads(old_data[like_type + resource_type])
        resources.append(data["resourceId"])
        resources = [str(resource) for resource in resources]
        resources = list(set(resources))

        update_data = {
            "id": data["user"],
            "new_data": json.dumps(resources),
            "new_data_column": like_type + resource_type,
        }
        return update_data
