import argparse
import asyncio
import os
from contextlib import asynccontextmanager

import uvicorn
from fastapi import FastAPI

from conteiner import Container


@asynccontextmanager
async def lifespan(app: FastAPI):
    asyncio.create_task(container.getter.generate_stats())
    container.start_all_generators()

    yield
    for generator in container.generators:
        generator.running = False
    container.getter.is_working = False


parser = argparse.ArgumentParser(description="Load test for nearline")
parser.add_argument(
    "-na",
    "--nearline_address",
    type=str,
    default="http://localhost:8000/diag",
    help="Address of nearline diagnostic endpoint",
)
parser.add_argument(
    "-nog",
    "--num_of_generators",
    type=int,
    default=2,
    help="Number of generators. The more, the bigger load on nearline",
)
parser.add_argument(
    "-ks", "--kafka_server", type=str, required=True, help="Address of kafka server"
)
parser.add_argument(
    "-kt",
    "--kafka_topic",
    type=str,
    required=True,
    help="Name of kafka topic for artificial datasa",
)
parser.add_argument(
    "-sp",
    "--save_path",
    type=str,
    default="history",
    help="Path to folder where save the history of requests",
)

args = parser.parse_args()
container = Container(
    kafka_server=args.kafka_server,
    kafka_topic=args.kafka_topic,
    save_path=args.save_path,
    num_of_generators=args.num_of_generators,
    nearline_address=args.nearline_address,
)
app = FastAPI(lifespan=lifespan)


@app.get("/total")
async def get_total():
    return (container.get_total_recived(),)


@app.get("/stop_sending")
async def stop_sending():
    for generator in container.generators:
        generator.running = False


if __name__ == "__main__":

    if not os.path.exists(args.save_path):
        os.makedirs(args.save_path)

    uvicorn.run("main:app", host="0.0.0.0", log_level="info", port=8001)
