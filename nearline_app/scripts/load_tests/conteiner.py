import threading

from generator import Generator
from getter import Getter


def start_generator(generator):
    generator.main_loop()


class Container:
    def __init__(
        self,
        kafka_server,
        kafka_topic,
        save_path,
        num_of_generators=2,
        nearline_address="http://localhost:8000/diag",
    ):
        self.generators = [
            Generator(kafka_server, kafka_topic) for i in range(num_of_generators)
        ]
        self.getter = Getter(self, nearline_address, save_path)

    def start_all_generators(self):
        for generator in self.generators:
            thread = threading.Thread(target=start_generator, args=(generator,))
            thread.start()

    def get_total_sended(self):
        return sum([generator.counter for generator in self.generators])

    def get_total_new_names(self):
        return sum([generator.new_names for generator in self.generators])

    def get_total_recived(self):
        return self.getter.get_request()
