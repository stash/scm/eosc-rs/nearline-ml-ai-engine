import logging
from typing import List

import happybase
from thriftpy2.transport import TTransportException

logger = logging.getLogger(__name__)


class BetterConnection:
    def __init__(self, host: str, port: int):

        self.host = host
        self.port = port
        self.connection_pool = None
        self.connect()

    def connect(self) -> None:
        """
        Function that establish connection pool with HBase
        """
        self.connection_pool = happybase.ConnectionPool(
            size=2, host=self.host, port=self.port, timeout=10000
        )

    def table(self, table_name: str) -> happybase.Table:
        """
        Function that returns table object of a given name

        Arguments:
            table_name {str} -- Name of the table

        Returns:
            happybase.Table -- Table object
        """
        with self.connection_pool.connection() as connection:
            return connection.table(table_name)

    def row(
        self, table_name: str, row_id: str, columns: List[str] = None, times: int = 0
    ):
        """
        Function that returns row from a table with a given name

        Arguments:
            table_name {str} -- Name of the table
            row_id {str} -- Row id
            columns {List[str]} -- List of columns to return
            times {int} -- Number of times to try to connect to HBase
        """
        try:
            table = self.table(table_name)
            return table.row(row_id, columns=columns)
        except TTransportException:
            if times < 1:
                self.connect()
                logger.info("Refreshing connection to HBase")
                return self.row(table_name, row_id, columns, times + 1)
            logger.error("TTransportException Error during getting data from hbase")

        except Exception as error:
            if times < 1:
                self.connect()
                return self.row(table_name, row_id, columns, times + 1)
            logger.error("Error while getting data from Hbase: %s", error)

    def put(self, table_name: str, row_id: str, insert_data: dict, times: int = 0):
        """
        Function that puts data into HBase into table with specified name

        Arguments:
            table_name {str} -- Name of the table
            row_id {str} -- Row id
            insert_data {dict} -- Data to insert
            times {int} -- Number of times to try to connect to HBase
        """
        try:
            table = self.table(table_name)
            return table.put(row_id, insert_data)

        except (IOError, TTransportException) as error:
            if times < 1:
                self.connect()
                return self.put(table_name, row_id, insert_data, times + 1)

            logger.error("Error during putting data to hbase, %s", error)

        except Exception as error:
            if times < 1:
                self.connect()
                return self.put(table_name, row_id, insert_data, times + 1)

            logger.error("Error while putting data to Hbase: %s", error)

    def rows(self, table_name: str, rows_ids: list, columns=None, times: int = 0):
        """
        Function that returns some number of rows from a table with a given name
        """
        try:
            table = self.table(table_name)
            return table.rows(rows_ids, columns=columns)
        except (TTransportException, IOError) as exc:
            if times < 1:
                self.connect()
                return self.rows(table_name, rows_ids, columns, times + 1)

            logger.error("Error during getting data from hbase, %s", exc)

        except Exception as error:
            if times < 1:
                self.connect()
                return self.rows(table_name, rows_ids, columns, times + 1)

            logger.error("Error while getting data from Hbase: %s", error)

    def tables(self, times=0):
        """
        Function that returns list of tables from HBase
        """
        try:
            with self.connection_pool.connection() as connection:
                return connection.tables()
        except TTransportException:
            if times < 1:
                self.connect()
                return self.tables(times + 1)
            logger.error("TTransportException Error during getting data from hbase")

        except Exception as error:
            logger.error("Error while getting data from Hbase: %s", error)
