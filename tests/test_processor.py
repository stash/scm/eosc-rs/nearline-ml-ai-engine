"""
Module for testing (Postgres)Processor class.
"""
import json
import logging
import unittest
from unittest.mock import MagicMock

import numpy as np
import pandas as pd

from nearline_app.embedders.NullEmbedder import NullEmbedder

# from nearline_app.embedders.UserEmbedder import UserEmbedder
from nearline_app.processor import Processor
from nearline_app.settings import settings
from nearline_app.utils import name_map
from tests.utils_mocks import (
    get_user_data,
    SingleVisitFunction,
    switch_output,
    MultiVisitFunction,
)

logging.disable(logging.CRITICAL)


class TestProcessor(unittest.TestCase):
    """
    Test class, which tests the Processor class
    """

    def setUp(self):
        """
        Setup function, which is called before each test
        """
        sql_connector = MagicMock()
        sql_connector.get_embeddings_with_languages.return_value = (
            pd.DataFrame.from_dict(
                {
                    "embeddings": 2 * [np.ones(256, dtype=np.float32)],
                    "language": 2 * ["en"],
                    "id": ["3", "5"],
                }
            ).set_index("id")
        )
        self.processor = Processor("Test")
        user_embedder = MagicMock()
        user_embedder.get_user_embedding.return_value = np.zeros(
            (1, 256), dtype=np.float32
        )
        self.processor.setup_processor(
            database_connector_resource=sql_connector,
            database_connector_user=sql_connector,
            resource_embedder=NullEmbedder(),
            user_embedder=user_embedder,
        )

        self.anon_users_table = settings.ANON_USERS_TABLE
        self.logged_users_table = settings.LOGGED_USERS_TABLE
        self.logged_users_aai_table = settings.LOGGED_USERS_AAI_TABLE

    def test_get_processed_user(self):
        user = get_user_data()
        self.processor.get_user_data = SingleVisitFunction("update_resources")

        response = self.processor.get_processed_user(user)
        self.assertEqual(response[0][0]["id"], "sample_session_id")
        self.assertEqual(response[0][1], "new")
        self.assertEqual(response[0][2], self.anon_users_table)
        # print(response)
        self.processor.get_user_data = MagicMock(return_value=["abc"])
        self.processor.get_updated_user = MagicMock(return_value=["abc"])

        user["isAnonymous"] = False
        user_aai = user.copy()
        user_aai["aaiUid"] = "user_aai"
        response = self.processor.get_processed_user(user_aai)

        self.assertEqual(response[0][0], ["abc"])
        self.assertEqual(response[0][1], "update")
        self.assertEqual(response[0][2], self.logged_users_aai_table)

        user_marketplace = user.copy()
        user_marketplace["marketplaceId"] = "user_marketplaceId"
        response = self.processor.get_processed_user(user_marketplace)

        self.assertEqual(response[0][2], self.logged_users_table)

    def test_get_user_data(self):
        self.processor.database_connector_user.get_user.return_value = None
        output_user = self.processor.get_user_data("test_user_1", "user_data")
        self.assertEqual(output_user, None)

        self.processor.database_connector_user.get_user.return_value = {
            "c1": "v1",
            "c2": "v2",
            "c3": "v3",
        }
        output_user = self.processor.get_user_data("test_user_1", "user_data")
        self.assertEqual(output_user, {"c1": "v1", "c2": "v2", "c3": "v3"})

    def test_proces_new_user(self):
        self.processor.database_connector_user.insert_new_user_with_embeddings = (
            SingleVisitFunction("proces_new_user")
        )

        new_data = switch_output.copy()

        for ordered in [True, False]:
            new_data["user"] = "test_user_" + str(ordered)
            new_data["isOrderAction"] = ordered
            expected_input = {
                "id": "test_user_" + str(ordered),
                "new_data": json.dumps([new_data["resourceId"]]),
                "new_data_emb": np.zeros((1, 256)).astype("float32").tobytes(),
                "new_data_column": "services:ordered"
                if ordered
                else "services:visited",
                "new_data_emb_column": "services_emb:ordered"
                if ordered
                else "services_emb:visited",
            }
            self.processor.proces_new_user(new_data, "user_data")
            for key in expected_input:
                self.assertEqual(
                    self.processor.database_connector_user.insert_new_user_with_embeddings.arg2[
                        key
                    ],
                    expected_input[key],
                )

    def test_update_user(self):
        self.processor.database_connector_user.update_user_with_embeddings = (
            SingleVisitFunction("update_user")
        )
        update_user = {
            "services_emb:visited": [],
            "services_emb:ordered": [],
            "services:ordered": json.dumps(["3"]),
            "services:visited": json.dumps(["3"]),
        }

        new_data = switch_output.copy()

        for ordered in [True, False]:
            new_data["user"] = "test_user_" + str(ordered)
            new_data["isOrderAction"] = ordered
            self.processor.update_user(new_data, update_user, "user_data")

            self.assertEqual(
                self.processor.database_connector_user.update_user_with_embeddings.arg1,
                "user_data",
            )
            self.assertEqual(
                self.processor.database_connector_user.update_user_with_embeddings.arg2[
                    "id"
                ],
                "test_user_" + str(ordered),
            )

    # def test_update_user_info(self):
    #     """
    #     Test function, which tests the update_user_info function
    #     """
    #     data = {
    #         "marketplaceId": "marketplaceId_test",
    #         "aaiUid": "aaiUid_test",
    #         "categories": [1, 2, 3],
    #         "scientific_domains": [1, 2, 3],
    #     }
    #     self.processor.database_connector_user.single_update_user = MultiVisitFunction(
    #         "update_user_info"
    #     )
    #     self.processor.update_user_info(data)
    #     self.assertEqual(
    #         set(self.processor.database_connector_user.single_update_user.arg1),
    #         {self.logged_users_table, self.logged_users_aai_table},
    #     )
    #     self.assertEqual(
    #         set(self.processor.database_connector_user.single_update_user.arg3),
    #         {"user_info:scientific_domains", "user_info:categories"},
    #     )
    #     self.assertEqual(
    #         self.processor.database_connector_user.single_update_user.flag, 4
    #     )

    # def test_create_user(self):
    #     """
    #     Test if the user is created correctly
    #     """
    #     counter = MultiVisitFunction("create_user")
    #     self.processor.database_connector_user.insert_new_empty_user = counter
    #     data = {
    #         "marketplaceId": "marketplaceId_test",
    #         "aaiUid": "aaiUid_test",
    #         "categories": [1, 2, 3],
    #         "scientific_domains": [1, 2, 3],
    #     }
    #     self.processor.get_user_data = MagicMock(return_value=None)
    #     self.processor.create_user(data)
    #     self.assertEqual(counter.flag, 2)
    #     self.assertEqual(
    #         counter.arg1, [self.logged_users_table, self.logged_users_aai_table]
    #     )
    #     self.assertEqual(counter.arg2, ["marketplaceId_test", "aaiUid_test"])

    def test_get_service_info(self):
        """
        Test if the service info is updated correctly
        """
        data = {
            "id": "service_id",
            "description": "description",
            "categories": [1, 2, 3],
        }
        response = self.processor.get_service_info(data)

        self.assertEqual(response[0], data["id"])
        self.assertEqual(response[1], json.dumps(data["categories"]))
        self.assertEqual(
            response[3],
            "update",
        )

    # def test_create_service(self):
    #     """
    #     Test case for function create_service
    #     """
    #     data = {
    #         "id": "service_id",
    #         "description": "description",
    #         "categories": [1, 2, 3],
    #     }
    #     self.processor.database_connector_resource.update_service = MultiVisitFunction(
    #         "create_service"
    #     )
    #     self.processor.create_service(data)
    #     self.assertEqual(
    #         self.processor.database_connector_resource.update_service.arg1,
    #         [data["id"]],
    #     )
    #     self.assertEqual(
    #         self.processor.database_connector_resource.update_service.arg2,
    #         [json.dumps(data["categories"])],
    #     )

    def test_get_user_like(self):
        """
        Test case for function user_like
        """
        data = {
            "action": "dislike",
            "resourceId": "service_id",
            "resourceType": "service",
            "visitId": "visit_id",
            "user": "user_test",
        }
        self.processor.check_user_like = MultiVisitFunction("user_like")

        response = self.processor.get_user_like(data)
        self.assertEqual(response, [])

        data["aaiUid"] = "aaiUid_test"
        response = self.processor.get_user_like(data)
        self.assertEqual(response, [None])
        self.assertEqual(
            self.processor.check_user_like.arg2[0], settings.LOGGED_USERS_AAI_TABLE
        )

        data["uniqueId"] = "uniqueId_test"
        response = self.processor.get_user_like(data)
        self.assertEqual(response, [None, None])
        self.assertEqual(
            self.processor.check_user_like.arg2[2], settings.LOGGED_USERS_TABLE
        )

    def test_proces_new_user_like(self):
        """
        Test case for function proces_new_user_like
        """
        data = {
            "action": "dislike",
            "resourceId": "service_id",
            "resourceType": "service",
            "user": "user_test",
        }
        self.processor.database_connector_user.insert_new_user = SingleVisitFunction(
            "proces_new_user_like"
        )
        result = self.processor.proces_new_user_like(data)

        self.assertEqual(
            result,
            {
                "id": data["user"],
                "new_data": json.dumps([data["resourceId"]]),
                "new_data_column": f'{data["action"]}:{name_map[data["resourceType"]]}',
            },
        )

    def test_update_user_like(self):
        data = {
            "action": "dislike",
            "resourceId": "service_id",
            "resourceType": "service",
            "user": "user_test",
        }
        old_data = {
            f"{data['action']}:{name_map[data['resourceType']]}": json.dumps(
                ["a", "b", "c"]
            ),
        }
        output = list({"a", "b", "c", data["resourceId"]})

        self.processor.database_connector_user.update_user = SingleVisitFunction(
            "update_user_like"
        )
        result = self.processor.update_user_like(data, old_data)
        self.assertEqual(
            result,
            {
                "id": data["user"],
                "new_data": json.dumps(output),
                "new_data_column": f'{data["action"]}:{name_map[data["resourceType"]]}',
            },
        )

    def test_delete_non_existing_resource(self):
        update_user = {
            "services_emb:visited": [],
            "services_emb:ordered": [],
            "services:ordered": json.dumps(["3", "4"]),
            "services:visited": json.dumps(["3", "4"]),
        }

        new_data = switch_output.copy()
        new_data["user"] = "test_user"

        self.processor.database_connector_user.update_user_with_embeddings = (
            SingleVisitFunction("update_user")
        )
        self.processor.update_user(new_data, update_user, "user_data")

        output = (
            self.processor.database_connector_user.update_user_with_embeddings.arg2[
                "new_data"
            ]
        )
        self.assertEqual({"5", "3"}, set(json.loads(output)))
