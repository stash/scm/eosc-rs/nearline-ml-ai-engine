"""
Module, that contains DynamicSwitch class.
"""
import asyncio
import logging
from concurrent.futures import ThreadPoolExecutor
from typing import List, Dict

from thriftpy2.transport import TTransportException

from nearline_app.database_connectors.HBaseConnector import HBaseConnector
from nearline_app.database_connectors.PostgresConnector import PostgresConnector
from nearline_app.process_switch import ProcessSwitch
from nearline_app.settings import settings
from nearline_app.storing_switch import StoringSwitch

logger = logging.getLogger(__name__)
executor_process = ThreadPoolExecutor(max_workers=settings.NUMBER_OF_PROCESS_SWITCHES)
MULTIPLAYER = (
    2
    if "HBASE" in settings.DATABASE_STORAGE and "POSTGRES" in settings.DATABASE_STORAGE
    else 1
)
executor_store = ThreadPoolExecutor(
    max_workers=settings.NUMBER_OF_PROCESS_SWITCHES * MULTIPLAYER
)


class DynamicSwitch:
    """
    Class that works on two asynchronous queues: incoming_queue and outgoing_queue.
    First, incoming_queue is filled by KafkaRunner. Next data from there goes to Processor and is processed there.
    After processing, data is put into outgoing_queue. Then, StoringSwitch takes data from outgoing_queue and
    stores it into some database.
    All actions should be asynchronous.
    """

    def __init__(self):
        self.counter = 0
        self.incoming_queue = asyncio.Queue()
        self.outgoing_queue = asyncio.Queue()
        self.update_queue = asyncio.Queue()

        self.process_switches = []
        self.storing_switches = []
        self.shutdown_flag = False
        self.currently_storing = []

    def setup_switches(
        self, amount_of_process_switches: int = 1, amount_of_stored_switches: int = 1
    ) -> None:
        """
        Function, that creates Switches with dependence of settings.
        """
        for i in range(amount_of_process_switches):

            if "POSTGRES" in settings.DATABASE_STORAGE:
                postgres_connector = PostgresConnector()
                postgres_switch = ProcessSwitch(
                    f"Postgres_process_{i}", postgres_connector
                )
                self.process_switches.append(postgres_switch)
            elif "HBASE" in settings.DATABASE_STORAGE:
                try:
                    hbase_connector = HBaseConnector()
                except TTransportException as exc:
                    logger.error("TTransportException Error during connecting to HBASE")
                    raise exc
                hbase_switch = ProcessSwitch(f"HBase_process_{i}", hbase_connector)
                self.process_switches.append(hbase_switch)
            else:
                logger.error("Wrong database storage variable")
                raise Exception("Wrong database storage variable")

        for i in range(amount_of_stored_switches):
            storing_switches = []
            if "POSTGRES" in settings.DATABASE_STORAGE:
                postgres_connector = PostgresConnector()
                postgres_switch = StoringSwitch(
                    f"Postgres_storing_{i}", postgres_connector
                )
                storing_switches.append(postgres_switch)

            if "HBASE" in settings.DATABASE_STORAGE:
                try:
                    hbase_connector = HBaseConnector()
                except TTransportException as exc:
                    logger.error("TTransportException Error during connecting to HBASE")
                    raise exc
                hbase_switch = StoringSwitch(f"HBase_storing_{i}", hbase_connector)
                storing_switches.append(hbase_switch)
            self.storing_switches.append(storing_switches)

    async def add_to_incoming_queue(self, topic: str, message: dict) -> None:
        """
        Function, that adds message to incoming_queue.
        """
        await self.incoming_queue.put((topic, message))

    async def process_messages(self, switch: ProcessSwitch) -> None:
        """
        Function, that processes messages from incoming_queue and puts them into outgoing_queue.
        """
        loop = asyncio.get_event_loop()
        while not self.shutdown_flag:
            message = await self.incoming_queue.get()

            if message[0] == settings.KAFKA_TOPIC_UA:
                if message[1]["sessionId"] in self.currently_storing:
                    await self.incoming_queue.put(message)
                    await asyncio.sleep(1)
                    continue
            try:

                processed_data = await loop.run_in_executor(
                    executor_process,
                    switch.parse_data_from_kafka,
                    message[1],
                    message[0],
                )
                if not processed_data:
                    continue
                for processed_data_part in processed_data:
                    if not processed_data_part:
                        continue
                    await self.outgoing_queue.put(
                        (processed_data_part, message[0], message)
                    )
                    if message[0] == settings.KAFKA_TOPIC_UA:
                        await self.update_queue.put((message[1]["sessionId"], "add"))
                self.counter += 1
            except Exception as exc:
                logger.error("Error during processing message")
                logger.error(exc)
                await asyncio.sleep(60)
                await self.incoming_queue.put(message)

    async def handle_storing_list(self):
        """
        Seperate function for handling stroing list actions.
        """
        while not self.shutdown_flag:
            session_id, operation = await self.update_queue.get()
            if operation == "add":
                self.currently_storing.append(session_id)
            elif operation == "remove":
                self.currently_storing.remove(session_id)

    async def store_messages(self, storing_switches: List[StoringSwitch]) -> None:
        """
        Function, that stores messages from outgoing_queue.
        """
        loop = asyncio.get_event_loop()
        while not self.shutdown_flag:
            message = await self.outgoing_queue.get()
            try:
                for switch in storing_switches:
                    await loop.run_in_executor(
                        executor_store,
                        switch.parse_data_to_database_connector,
                        message[0],
                        message[1],
                    )
                    if message[1] == settings.KAFKA_TOPIC_UA:
                        await self.update_queue.put(
                            (message[2][1]["sessionId"], "remove")
                        )

            except Exception as exc:
                logger.error("Error during storing message %s", exc)
                logger.error(exc)
                await asyncio.sleep(60)
                await self.outgoing_queue.put(message)

    def health_check(self) -> Dict[str, int]:
        """
        Function, that how many messages are in both queue and how many messages was processed.
        """
        # for switch in self.process_switches:
        #     switch.health_check()
        #
        # for switches in self.storing_switches:
        #     for switch in switches:
        #         switch.health_check()

        return {
            "incoming_queue": self.incoming_queue.qsize(),
            "outgoing_queue": self.outgoing_queue.qsize(),
            "counter": self.counter,
        }
