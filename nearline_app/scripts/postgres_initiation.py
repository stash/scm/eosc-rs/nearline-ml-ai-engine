"""
Scripts for initialization Postgres tables.
"""
import pandas as pd
import psycopg2
from psycopg2.extensions import AsIs

from nearline_app.settings import settings


def create_postgres_user(table_name):
    """
    Create empty user table in Postgres.

    Arguments:
        table_name (str): Table name
    """
    conn = psycopg2.connect(
        host=settings.POSTGRES_HOST,
        database=settings.POSTGRES_DATABASE,
        user=settings.POSTGRES_USER,
        password=settings.POSTGRES_PASSWORD,
    )

    cur = conn.cursor()
    query = """CREATE TABLE if not exists users.%s (
            id VARCHAR(100) PRIMARY KEY,
            "datasets_emb:ordered" BYTEA,
            "datasets_emb:visited" BYTEA,
            "publications_emb:ordered" BYTEA,
            "publications_emb:visited" BYTEA,
            "services_emb:ordered" BYTEA,
            "services_emb:visited" BYTEA,
            "software_emb:ordered" BYTEA,
            "software_emb:visited" BYTEA,
            "trainings_emb:ordered" BYTEA,
            "trainings_emb:visited" BYTEA,
            "other_research_product_emb:ordered" BYTEA,
            "other_research_product_emb:visited" BYTEA,
            "data_sources_emb:ordered" BYTEA,
            "data_sources_emb:visited" BYTEA,
            "datasets:ordered" TEXT,
            "datasets:visited" TEXT,
            "publications:ordered" TEXT,
            "publications:visited" TEXT,
            "services:ordered" TEXT,
            "services:visited" TEXT,
            "software:ordered" TEXT,
            "software:visited" TEXT,
            "trainings:ordered" TEXT,
            "trainings:visited" TEXT,
            "other_research_product:ordered" TEXT,
            "other_research_product:visited" TEXT,
            "data_sources:ordered" TEXT,
            "data_sources:visited" TEXT,
            "dislike:datasets" TEXT,
            "dislike:publications" TEXT,
            "dislike:software" TEXT,
            "dislike:trainings" TEXT,
            "dislike:other_research_product" TEXT,
            "dislike:services" TEXT,
            "dislike:data_sources" TEXT,
            "like:datasets" TEXT,
            "like:publications" TEXT,
            "like:software" TEXT,
            "like:trainings" TEXT,
            "like:other_research_product" TEXT,
            "like:services" TEXT,
            "like:data_sources" TEXT,
            "user_info:categories" TEXT,
            "user_info:user_constraint" TEXT,
            "user_info:projects" TEXT,
            "user_info:scientific_domains" TEXT)"""
    param = (AsIs(table_name),)
    cur.execute(query, param)
    conn.commit()


def create_postgres_meta(tables_names, meta_table_name):
    """
    Create metatable for Postgres user tables.
    """
    conn = psycopg2.connect(
        host=settings.POSTGRES_HOST,
        database=settings.POSTGRES_DATABASE,
        user=settings.POSTGRES_USER,
        password=settings.POSTGRES_PASSWORD,
    )
    cur = conn.cursor()
    query = """CREATE TABLE if not exists metadata.%s(
            name TEXT,
            "row count" bigint,
            "table name" TEXT,
            "last update" date
        );"""
    param = (AsIs(meta_table_name),)
    names = ["users_anon", "users", "users_aai"]
    cur.execute(query, param)
    for name, table_name in zip(names, tables_names):
        query = f"""INSERT INTO metadata.metadata_users(name,
                                                        "row count",
                                                        "table name",
                                                        "last update")
                        VALUES('{name}', 0, '{table_name}',
                                        '{pd.Timestamp.today().date()}');"""
        cur.execute(query)

    conn.commit()


def initiate_postgres(
    anon_user_table: str,
    logged_user_table: str,
    logged_user_aai_table: str,
    meta_table_name: str = "metadata_users",
):
    """
    Initiate Postgres tables and metadata table.
    """

    for table in [
        anon_user_table,
        logged_user_table,
        logged_user_aai_table,
    ]:
        create_postgres_user(table)
    create_postgres_meta(
        [anon_user_table, logged_user_table, logged_user_aai_table],
        meta_table_name=meta_table_name,
    )
