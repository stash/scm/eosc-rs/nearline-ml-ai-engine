"""
Module, that is connector between KafkaRunner and some database.
"""
import logging
from typing import Union, List

from nearline_app.processor import Processor
from nearline_app.settings import settings
from nearline_app.utils import (
    KafkaMessage,
    UserEventMessage,
    ServiceEventMessage,
    UserActionParsed,
    UserLikeEvent,
    name_map,
)

logger = logging.getLogger(__name__)


class ProcessSwitch:
    """
    A class responsible for aggregating and parsing messages from
    DataBusses Runners to DataBases Connectors.
    """

    def __init__(self, name, database_connector):

        self.name = name
        self.processor = Processor(name)

        self.counter = 0
        self.is_database_connected = False
        self.database_connector = database_connector

    def setup_processor(self, resource_embedder, user_embedder):
        """
        Function, that setups processor for given database
        """
        if self.database_connector.current_tables is not None:
            self.is_database_connected = True
        self.processor.setup_processor(
            database_connector_resource=self.database_connector,
            database_connector_user=self.database_connector,
            resource_embedder=resource_embedder,
            user_embedder=user_embedder,
        )

    def parse_data_from_kafka(
        self,
        data: Union[KafkaMessage, UserEventMessage, ServiceEventMessage, UserLikeEvent],
        topic: str,
    ) -> Union[bool, list]:
        """
        Parse data from Kafka and send it to processors.
        Make different parsing in dependency from topic of message.
        """
        if topic == settings.KAFKA_TOPIC_UA:
            logger.info(
                "Start processing user action %s to %s",
                data["sessionId"],
                self.name.lower(),
            )
            return self.parse_user_action_data(data)

        elif topic == settings.KAFKA_TOPIC_USER:
            logger.info(
                "Start processing user event: %s to %s",
                data["aaiId"],
                self.name.lower(),
            )
            return self.parsa_user_event(data)

        elif topic == settings.KAFKA_TOPIC_SERVICE:
            logger.info(
                "Start processing service event: %s to %s",
                data["id"],
                self.name.lower(),
            )
            return self.parse_resource_data(data, "services")

        elif topic == settings.KAFKA_TOPIC_TRAINING:
            logger.info(
                "Start processing training event: %s to %s",
                data["id"],
                self.name.lower(),
            )
            return self.parse_resource_data(data, "trainings")

        elif topic == settings.KAFKA_LIKE_TOPIC:
            logger.info(
                "Start processing user like event: %s to %s",
                data["visitId"],
                self.name.lower(),
            )
            return self.parse_user_like_data(data)

        else:
            logger.warning("This topic: %s is not supported by this switch", topic)
            return False

    def parse_user_action_data(self, data: KafkaMessage) -> Union[bool, list]:
        """
        Parse User Action from Kafka and send it to processors

        Arguments:
            data (KafkaMessage): Message from Kafka
        """
        if "userActions" in data:
            action = data["userActions"][-1]
            # get last action, since we get all actions in this session
            if "resourceId" in action and action["resourceId"] != "unknown":
                results = {
                    "isAnonymous": data["isAnonymous"],
                    "resourceId": action["resourceId"],
                    "isOrderAction": action["isOrderAction"],
                    "resourceType": action["resourceType"],
                    "sessionId": data["sessionId"],
                }

                if " " in results["resourceId"]:
                    logger.warning(
                        "Resource id cannot contains space: %s", results["resourceId"]
                    )
                    return False

                if results["resourceType"] not in name_map:
                    logger.warning(" %s is not supported yet", results["resourceType"])
                    return False

                if (
                    "aaiUid" not in data
                    and "marketplaceId" not in data
                    and data["isAnonymous"] is False
                ):
                    logger.warning("NO AAI UID")
                    return False

                if "aaiUid" in data:
                    results["aaiUid"] = data["aaiUid"]
                if "marketplaceId" in data:
                    results["marketplaceId"] = data["marketplaceId"]

                return self.get_user(results)

            logger.warning("NO SERVICE ID")
            return False

        logger.warning("NO USER ACTIONS")
        return False

    def get_user(self, results: UserActionParsed):
        """
        Get user data from processor
        """
        self.counter += 1
        return self.processor.get_processed_user(results)

    def parsa_user_event(self, data: UserEventMessage):
        """
        Parse User Events from Kafka in dependence from eventType and send
        it to processor
        """
        if "eventType" not in data:
            return [False]
        if data["eventType"] not in ["update", "create"]:
            return [False]
        new_data = {
            "marketplaceId": data["marketplaceId"],
            "aaiUid": data["aaiId"],
            "categories": data["categoryValues"],
            "scientific_domains": data["scientificDomainValues"],
            "process_status": data["eventType"],
        }
        user_aai = self.processor.get_user_data(
            new_data["aaiUid"], settings.LOGGED_USERS_AAI_TABLE
        )
        user_market = self.processor.get_user_data(
            new_data["marketplaceId"], settings.LOGGED_USERS_TABLE
        )
        return [{"data": new_data, "aaiUid": user_aai, "marketplaceId": user_market}]

    def parse_resource_data(
        self, data: ServiceEventMessage, resource_type: str
    ) -> Union[bool, List]:
        """
        Parse Resource Events from Kafka
        """
        if "description" not in data:
            return False

        new_data = {
            "id": data["id"],
            "description": data["description"],
        }
        if "categoryValues" in data:
            new_data["categories"] = data["categoryValues"]
        else:
            new_data["categories"] = []

        return [self.processor.get_resource_info(new_data, resource_type)]

    def parse_user_like_data(self, data: UserLikeEvent):
        """
        Parse User Like Events from Kafka and send it to processor
        """
        result = []
        if "action" in data:

            if data["action"] == "like" or data["action"] == "dislike":
                user_likes = self.processor.get_user_like(data)
                for user_like in user_likes:
                    result.append(("user_like", user_like))

                if data["action"] == "like":
                    results = {
                        "isAnonymous": False,
                        "resourceId": data["resourceId"],
                        "isOrderAction": False,
                        "resourceType": data["resourceType"],
                        "sessionId": data["visitId"],
                        "aaiUid": data["aaiUid"] if "aaiUid" in data else None,
                        "marketplaceId": data["uniqueId"]
                        if "uniqueId" in data
                        else None,
                        "user": None,
                    }
                    user = self.get_user(results)
                    for user_profile in user:
                        result.append(("user_profile", user_profile))

        if len(result) == 0:
            logger.warning("Something go wrong with likes %s", data["aaiUid"])
            return [False]
        return result
