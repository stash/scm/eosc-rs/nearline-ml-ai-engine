import argparse

from nearline_app.scripts.hbase_initiation import initiate_hbase
from nearline_app.scripts.postgres_initiation import initiate_postgres
from nearline_app.settings import settings

parser = argparse.ArgumentParser(description="Initialize users tables")
parser.add_argument(
    "--anon",
    help="Name for the anonymous users tables",
    default=settings.ANON_USERS_TABLE,
)
parser.add_argument(
    "--logged",
    help="Name for the logged users tables",
    default=settings.LOGGED_USERS_TABLE,
)
parser.add_argument(
    "--logged_aai",
    help="Name for the logged with aai users tables",
    default=settings.LOGGED_USERS_AAI_TABLE,
)
parser.add_argument(
    "--metadata_table",
    help="Name for the metadata tables",
    default=settings.METADATA_TABLE,
)

if __name__ == "__main__":
    args = parser.parse_args()

    initiate_hbase(args.anon, args.logged, args.logged_aai)
    initiate_postgres(args.anon, args.logged, args.logged_aai, args.metadata_table)
