"""
A module with a class for connecting to Postgres database.

This class can:
- connect to postgres database
- get resource data from postgres database
- insert user data into postgres database
- update user data in postgres database
- get user data from postgres database
- update resource data in postgres database
- check availability of resource tables in postgres database
"""

import logging
import time
from typing import List, Union, Dict, Any

import numpy as np
import pandas as pd
import psycopg2
from psycopg2 import OperationalError
from psycopg2.extensions import AsIs
from psycopg2.extensions import cursor as Cursor

from nearline_app.database_connectors.DataBaseConnector import DataBaseConnector
from nearline_app.settings import settings
from nearline_app.utils import get_empty_user, resource_types, current_columns

logger = logging.getLogger(__name__)

keepalive_kwargs = {
    "keepalives": 1,
    "keepalives_idle": 60,
    "keepalives_interval": 10,
    "keepalives_count": 5,
}


class PostgresConnector(DataBaseConnector):
    """
    A connector with PostgreSQL.

    Parameters

    connection: psycopg2.connection
        A connection to the database.
        If none, a new connection will be created.
    """

    def __init__(self, connection=None):
        self.connection = connection
        if self.connection is None:
            self.set_postgres_connection()

        self.current_tables = self.get_current_tables()
        self.stop_working = None
        self.new_user_query = self._new_user_query()

    def set_postgres_connection(
        self,
        host: str = settings.POSTGRES_HOST,
        user: str = settings.POSTGRES_USER,
        password: str = settings.POSTGRES_PASSWORD,
        database: str = settings.POSTGRES_DATABASE,
    ):
        """
        Set a connection to the database.
        """
        try:
            self.connection = psycopg2.connect(
                host=host,
                database=database,
                user=user,
                password=password,
                **keepalive_kwargs,
            )
        except OperationalError:
            logger.error("Can't connect to postgres database, after 1 min retrying...")
            time.sleep(60)
            self.set_postgres_connection()

    def get_current_tables(self) -> Dict[str, str]:
        """
        Get the current tables with resources from the metadata table.

        Returns:
            a dictionary with resource type as a key and table name as a value.

        """
        cursor = self.connection.cursor()
        query = """SELECT "name", "table name" FROM metadata.%s;"""
        params = (AsIs(settings.METADATA_TABLE),)
        cursor.execute(query, params)
        results = cursor.fetchall()
        return {a[0]: a[1] for a in results}

    def get_embeddings(
        self, table_name: str, resources_id: List[str]
    ) -> Union[pd.DataFrame, None]:
        """
        Get the set of embeddings from given resource type.

        Arguments:
            table_name (str): table name from which we want to get
            resources_id (List[str]): list of resources id of which
                                                we want to get

        Returns:
            embeddings of resources or None if there is no embeddings in the
            table that match the given resources id
        """
        if table_name not in self.current_tables:
            logger.warning("Table not found %s", table_name)
            return None
        query = """SELECT "id", "embeddings" FROM resources.%s
                                                        WHERE id in ('%s');"""
        params = (
            AsIs(self.current_tables[table_name]),
            AsIs("', '".join(resources_id)),
        )

        results = self.get_query_from_table(table_name, query, params)

        embedding_df = pd.DataFrame(results, columns=["id", "embeddings"])
        if embedding_df.shape[0] == 0:
            return None
        embedding_df["embeddings"] = embedding_df["embeddings"].apply(
            lambda x: np.frombuffer(x, dtype="float32")
        )
        embedding_df.set_index("id", inplace=True)

        return embedding_df

    def get_embeddings_with_languages(
        self, table_name: str, resources_id: List[str]
    ) -> Union[pd.DataFrame, None]:
        """
        Get the set of embeddings with their language from given resource type.

        Arguments:
            table_name (str): table name from which we want to get
            resources_id (List[str]): list of resources id of which
                                                we want to get

        Returns:
            DataFrame with 2 columns:
                embeddings of resources
                language of resource
            or None if there is no embeddings in the
            table that match the given resources id
        """
        if table_name not in self.current_tables:
            logger.warning("Table not found %s", table_name)
            return None
        query = """SELECT "id", "embeddings", "language_prediction" FROM resources.%s
                                                                WHERE id in ('%s');"""
        params = (
            AsIs(self.current_tables[table_name]),
            AsIs("', '".join(resources_id)),
        )
        results = self.get_query_from_table(table_name, query, params)
        embedding_df = pd.DataFrame(results, columns=["id", "embeddings", "language"])
        if embedding_df.shape[0] == 0:
            return None
        embedding_df["embeddings"] = embedding_df["embeddings"].apply(
            lambda x: np.frombuffer(x, dtype="float32")
        )
        embedding_df.set_index("id", inplace=True)

        return embedding_df

    def get_query_from_table(
        self, table_name: str, query: str, params: tuple, counter: int = 0
    ) -> Union[pd.DataFrame, None]:
        """
        Perform select query on table with given name.
        """
        cursor = self.connection.cursor()
        try:
            cursor.execute(query, params)
            results = cursor.fetchall()

        except psycopg2.errors.SyntaxError as exc:
            logger.warning("Something wrong with syntax: %s", exc)
            cursor.close()
            self.connection.rollback()
            return None
        except psycopg2.errors.UndefinedTable:
            if counter < 2:
                logger.warning("Reloading list of tables")
                cursor.close()
                self.connection.rollback()
                self.current_tables = self.get_current_tables()
                return self.get_query_from_table(table_name, query, params, counter + 1)
            logger.error("Table not found %s", table_name)
            return None
        return results

    def single_update_user(
        self, table_name: str, user_id: str, column: str, value: Union[str, bytes]
    ) -> None:
        """
        Update user entity (certain(single) column) in the database.

        Arguments:
            table_name (str): table name in which we want to update user
            user_id (str): user id which we want to update
            column (str): column name which we want to update
            value (Union[str, bytes]): new value for given user in given column
        """
        cursor = self.connection.cursor()
        self._update_user(table_name, user_id, column, value, cursor)
        self.connection.commit()

    def update_user_with_embeddings(
        self, table_name: str, update_data: Dict[str, Any]
    ) -> None:
        """
        Update user entity in the database(resources and embeddings for these resources).

        Arguments:
            table_name (str): table name in which we want to update user
            update_data (Dict[str, Any]): data which we want to insert:
                id (str): user id which we want to insert
                new_data (str): new resource
                new_data_column (str): column name which we want to
                                        update with new_data
                new_data_emb (bytes): embedding of new resource
                new_data_emb_column (str): column name which we want to
                                            update with new_data_emb
        """
        query = """UPDATE users.%s SET "%s" = %s, "%s" = %s  WHERE id = '%s';"""

        params = (
            AsIs(table_name),
            AsIs(update_data["new_data_column"]),
            update_data["new_data"],
            AsIs(update_data["new_data_emb_column"]),
            update_data["new_data_emb"],
            AsIs(update_data["id"]),
        )
        self.execute_query(query, params)

    def update_user(self, table_name: str, update_data: Dict[str, Any]) -> None:
        """
        Update user entity in the database(resources).

        Arguments:
            table_name (str): table name in which we want to update user
            update_data (Dict[str, Any]): data which we want to insert:
                id (str): user id which we want to insert
                new_data (str): new resource list
                new_data_column (str): column name which we want to
                                        update with new_data
        """
        query = """UPDATE users.%s SET "%s" = %s  WHERE id = '%s';"""

        params = (
            AsIs(table_name),
            AsIs(update_data["new_data_column"]),
            update_data["new_data"],
            AsIs(update_data["id"]),
        )
        self.execute_query(query, params)

    def insert_new_empty_user(self, table_name: str, user_id: str) -> None:
        """
        Insert a new empty user with given id.

        Arguments:
            table_name (str): table name in which we want to insert user
            user_id (str): user id which we want to insert

        """
        empty_user = get_empty_user()
        self._insert_user(table_name, user_id, empty_user)

    def insert_new_user_with_embeddings(
        self, table_name: str, insert_data: Dict[str, Any]
    ) -> None:
        """
        Insert a new user with given data and embedding for this data.

        Arguments:
            table_name (str): table name in which we want to insert user
            insert_data (Dict[str, Any]): data which we want to insert:
                id (str): user id which we want to insert
                new_data (str): new resource
                new_data_column (str): column name which we want to
                                        update with new_data
                new_data_emb (bytes): embedding of new resource
                new_data_emb_column (str): column name which we want to
                                            update with new_data_emb

        """
        empty_user = get_empty_user()
        empty_user[insert_data["new_data_column"]] = insert_data["new_data"]
        empty_user[insert_data["new_data_emb_column"]] = insert_data["new_data_emb"]
        self._insert_user(table_name, insert_data["id"], empty_user)

    def insert_new_user(self, table_name: str, insert_data: Dict[str, Any]) -> None:
        """
        Insert a new user with given data.

        Arguments:
            table_name (str): table name in which we want to insert user
            insert_data (Dict[str, Any]): data which we want to insert:
                id (str): user id which we want to insert
                new_data (str): new resource
                new_data_column (str): column name which we want to
                                        update with new_data
        """
        empty_user = get_empty_user()
        empty_user[insert_data["new_data_column"]] = insert_data["new_data"]
        self._insert_user(table_name, insert_data["id"], empty_user)

    def _insert_user(self, table_name: str, idx: str, user: Dict[str, Any]) -> None:
        temp1 = []
        temp2 = []
        for column, value in user.items():
            temp1.append(AsIs(column))
            temp2.append(value)
        params = (
            [AsIs(table_name)]
            + temp1
            + [
                AsIs(idx),
            ]
            + temp2
        )
        self.execute_query(self.new_user_query, params)

    @staticmethod
    def _update_user(
        table_name: str,
        user_id: str,
        column: str,
        value: Union[str, bytes],
        cursor: Cursor,
    ) -> None:
        """
        Update user entity(certain column) in the database.

        Arguments:
            table_name (str): table name in which we want to update user
            user_id (str): user id which we want to update
            column (str): column name which we want to update
            value (Union[str, bytes]): new value for given user in given column
            cursor (Cursor): cursor to the database
        """
        query = """UPDATE users.%s SET "%s" = %s WHERE id = '%s';"""

        params = (
            AsIs(table_name),
            AsIs(column),
            value,
            AsIs(user_id),
        )

        cursor.execute(query, params)

    def get_user(
        self, user_id: str, table_name: str, counter: int = 0
    ) -> Union[None, Dict[str, Any]]:
        """
        Get user entity from the database, and change it into
        dict in form column-name: value

        Return None, if no user was found.

        Arguments:
            user_id (str): user id which we want to get
            table_name (str): table name from which take user
            counter (int): number of times we tried to get user

        Returns:
             user entity in form od dict, or None
        """
        cursor = self.connection.cursor()

        query = """SELECT * FROM users.%s WHERE id = '%s';"""
        params = (AsIs(table_name), AsIs(user_id))

        try:
            cursor.execute(query, params)
        except OperationalError as error:
            if counter < 2:
                time.sleep(10)
                self.try_connect()
                return self.get_user(user_id, table_name, counter + 1)

            logger.error("Cannot connect to database")
            raise error
        except psycopg2.errors.InFailedSqlTransaction as error:
            self.connection.rollback()
            if counter < 2:
                time.sleep(10)
                self.try_connect()
                return self.get_user(user_id, table_name, counter + 1)

            logger.error("Cannot connect to database, InFailedSqlTransaction")
            raise error

        columns_names = [desc[0] for desc in cursor.description]
        results = cursor.fetchone()
        if results is None:
            return None
        temp_dict = {}
        for column, column_name in zip(results, columns_names):
            temp_dict[column_name] = column
        return temp_dict

    def update_resource(
        self, resource_id: str, categories: str, desc_embed: bytes, resource_type: str
    ) -> None:
        """
        Update service entity in the database.
        """
        resource_table = self.current_tables[resource_type]
        query = """UPDATE resources.%s SET categories = '%s',embeddings = %s
                                                            WHERE id = '%s';"""
        params = (
            AsIs(resource_table),
            AsIs(categories),
            desc_embed,
            AsIs(resource_id),
        )
        self.execute_query(query, params)

    def execute_query(self, query: str, params, counter=0) -> None:
        """
        Execute query with given params on database.
        """
        try:
            cursor = self.connection.cursor()
            cursor.execute(query, params)
            self.connection.commit()
        except OperationalError as error:
            self.try_connect()
            if counter < 10:
                logger.warning("Trying to reconnect to database after 60s")
                time.sleep(60)
                self.try_connect()
                return self.execute_query(query, params, counter + 1)

            logger.error("Cannot connect to database after 10 tries")
            raise error
        except psycopg2.errors.UndefinedColumn:
            logger.warning(
                "Some of following columns in SQL query does not exists: %s", params
            )
            self.connection.rollback()
        except psycopg2.errors.UniqueViolation:
            logger.warning("Row of given id already exists: %s", str(params[47]))
            self.connection.rollback()

    def insert_new_resource(
        self, resource_id: str, categories: str, desc_embed: bytes, resource_type: str
    ) -> None:
        """
        Create service entity in the database.
        """
        resource_table = self.current_tables[resource_type]
        query = """INSERT INTO resources.%s (id, categories, embeddings, isvalid, relations, language_prediction)
                            VALUES ('%s', '%s', %s, %s, '', 'en');"""
        params = (
            AsIs(resource_table),
            AsIs(resource_id),
            AsIs(categories),
            desc_embed,
            True,
        )
        self.execute_query(query, params)

    def try_connect(self, counter: int = 0):
        """
        Try to connect to the database.
        Keep trying until the connection is established.
        """
        if counter % 10 == 0:
            self.set_postgres_connection()

            logger.info("Establishing new connection to the database")
        try:
            cursor = self.connection.cursor()
            cursor.execute("SELECT 1")
            output = cursor.fetchone()
            if output:
                return True

        except Exception as error:
            if self.stop_working is None:
                self.stop_working = pd.Timestamp.now()
            logger.error(error)
            time.sleep(10)
            return self.try_connect(counter + 1)

    def get_list_of_tables_from_schema(self, schema_name: str) -> pd.DataFrame:
        """
        Get list of all tables in the schema.

        Arguments:
            schema_name (str): schema name from which we want to get
                                list of tables

        Returns:
            list of tables in the schema
        """
        cursor = self.connection.cursor()
        query = """SELECT table_name FROM information_schema.tables
                                WHERE table_schema = '%s';"""
        params = (AsIs(schema_name),)
        cursor.execute(query, params)
        output = cursor.fetchall()
        tables = pd.DataFrame(
            list(output),
        )
        return tables

    def check_if_correct_schema(self, table_name: str) -> str:
        """
        Check if the table has the correct schema.
        """
        cursor = self.connection.cursor()
        query = """SELECT column_name FROM information_schema.columns
                                        WHERE table_name = '%s';"""
        params = (AsIs(table_name),)
        cursor.execute(query, params)
        output = [column[0] for column in cursor.fetchall()]
        if set(output) != current_columns:
            return f"{table_name} has incorrect schema"

        return "OK"

    def health_check(self, counter: int = 0) -> dict:
        """
        Health check. Check if the connection is working.
        """
        response = {"status": "DOWN", "database type": "PostgreSQL"}

        try:
            metadata_tables = self.get_list_of_tables_from_schema("metadata")
            tables_status = {
                "metadata": settings.METADATA_TABLE in metadata_tables.values
            }

            resources_tables = self.get_list_of_tables_from_schema("resources")
            for resource, table_name in self.current_tables.items():
                tables_status[resource] = table_name in resources_tables.values
            response["tables"] = tables_status

            response["schema"] = "OK"
            for resource, table_name in self.current_tables.items():
                result = self.check_if_correct_schema(table_name)
                if result != "OK":
                    response["schema"] = result
                    break

            if response["schema"] == "OK":
                response["status"] = "UP"
        except Exception as error:
            logger.error("Trying to reestablish connection with Postgres")
            self.try_connect()
            if counter < 2:
                return self.health_check(counter + 1)

            logger.error("Error while connecting to Postgres: %s", str(error))
            response["error"] = "Error while connecting to Postgres:" + str(error)
        # except Exception as error:
        #     logger.error("Error while connecting to Postgres: %s ", str(error))
        #     response["error"] = "Error while connecting to Postgres:" + str(error)

        return response

    @staticmethod
    def _new_user_query():
        """(len(resource_types)*6 + 4) = number of columns"""
        begin = """INSERT INTO users.%s"""
        end = """);"""
        temp = ' "%s"'
        middle = """ ( id"""
        for _ in range((len(resource_types) * 6 + 4)):
            middle += "," + temp
        middle += """) VALUES ( '%s'"""
        temp = " %s"
        for _ in range((len(resource_types) * 6 + 4)):
            middle += "," + temp
        return begin + middle + end
