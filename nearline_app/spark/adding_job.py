"""
This module contains class AddingJob, which is responsible for adding new rows to the table.
"""
import happybase
import sparknlp
from psycopg2.extensions import AsIs
from pyspark.ml import Pipeline
from pyspark.sql import DataFrame
from pyspark.sql import functions as sf
from pyspark.sql.types import StringType
from sparknlp import EmbeddingsFinisher
from sparknlp.annotator import (
    SentenceDetectorDLModel,
    BertEmbeddings,
    SentenceEmbeddings,
    Tokenizer,
)
from sparknlp.base import DocumentAssembler

try:
    import udf_and_utils as utils
except ImportError:
    import nearline_app.spark.udf_and_utils as utils


# pylint: disable=E0401


class AddingJob:
    """
    Job that adds new rows to the table.
    """

    def __init__(self, job_context):
        self.job_context = job_context
        self.pipeline = None

    def create_postgres_table(self) -> None:
        """
        Create table in postgres database, that is consisted between all resources types.
        """
        query = """CREATE TABLE IF NOT EXISTS %s (
                            Id varchar(64) PRIMARY KEY,
                            embeddings BYTEA,
                            categories TEXT,
                            bestaccessright TEXT,
                            isvalid BOOLEAN,
                            relations TEXT,
                            language_prediction TEXT
                            );"""
        param = (AsIs(self.job_context.table_name),)

        self.job_context.perform_query(query, param)

    def create_hbase_table(self):
        """
        Creates hbase table that is consisted between all resources types.
        """
        if self.job_context.hbase_settings.is_used:
            self.job_context.logger.info("Creating hbase table")
            connection = happybase.Connection(
                self.job_context.hbase_settings.url,
                port=int(self.job_context.hbase_settings.port),
            )
            current_tables = connection.tables()
            if not bytes(self.job_context.table_name, "utf-8") in current_tables:
                connection.create_table(
                    self.job_context.table_name, {"embeddings": {}, "resource_info": {}}
                )

    def build_pipeline(self) -> Pipeline:
        """
        Build sparknlp pipeline for processing data.
        Piepline works for dataframe with column joined_column, and returned
        dataframe with column document, sentence, token, embeddings,
        sentence_embeddings and finished_embeddings.
        """
        document_assembler = (
            DocumentAssembler().setInputCol("joined_column").setOutputCol("document")
        )

        sentence_detector = (
            SentenceDetectorDLModel.pretrained()
            .setInputCols(["document"])
            .setOutputCol("sentence")
        )

        spark_tokenizer = Tokenizer().setInputCols("sentence").setOutputCol("token")

        embeddings = (
            BertEmbeddings.load(self.job_context.bert_path)
            .setInputCols(["sentence", "token"])
            .setOutputCol("embeddings_raw")
            .setCaseSensitive(True)
        )

        embeddings_sentence = (
            SentenceEmbeddings()
            .setInputCols(["document", "embeddings_raw"])
            .setOutputCol("sentence_embeddings")
            .setPoolingStrategy("AVERAGE")
        )

        embeddings_finisher = (
            EmbeddingsFinisher()
            .setInputCols("sentence_embeddings")
            .setOutputCols("finished_embeddings")
            .setOutputAsVector(True)
        )
        #
        pipeline = Pipeline(
            stages=[
                document_assembler,
                sentence_detector,
                spark_tokenizer,
                embeddings,
                embeddings_sentence,
                embeddings_finisher,
            ]
        )
        return pipeline

    def map_descriptions(self, record_df: DataFrame) -> DataFrame:
        """
        Apply mapping to descriptions column in dataframe.
        """
        return record_df.withColumn(
            "descriptions",
            utils.generate_mapper_udf(self.job_context.broadcast_mapping)(
                sf.col("descriptions")
            ),
        )

    def organize_columns(self, record_df: DataFrame) -> DataFrame:
        """
        Organize columns in record_df according to schema.
        """
        record_df = record_df.withColumn(
            "descriptions",
            sf.when(sf.size(sf.split(record_df.descriptions, " ")) < 2, None).otherwise(
                record_df.descriptions
            ),
        )

        if self.job_context.resource_type not in ["data_sources", "services"]:
            record_df = utils.add_is_valid_column(record_df)
        else:
            record_df = record_df.withColumn("isvalid", sf.lit(True))

        if "categories" not in record_df.columns:
            record_df = record_df.withColumnRenamed("keywords", "categories")

        if "relations" not in record_df.columns:
            record_df = record_df.withColumn(
                "relations", sf.lit(None).cast(StringType())
            )

        if "bestAccessRight" in record_df.columns:
            record_df = record_df.withColumnRenamed(
                "bestAccessRight", "bestaccessright"
            )

        if "keywordsMerged" not in record_df.columns:
            record_df = (
                record_df.withColumn(
                    "categories", sf.regexp_replace("categories", r"""[\[\]"]""", "")
                )
                .withColumn("categories", sf.regexp_replace("categories", """>""", ","))
                .withColumn("keywordsMerged", utils.string_set_udf("categories"))
            )

        return record_df

    @staticmethod
    def add_joined_column(record_df: DataFrame) -> DataFrame:
        """
        Add column `joined_column`, which is concatenation of title and descriptions and keywords
        """
        null_cond = sf.col("mainTitles").isNull() & sf.col("descriptions").isNull()
        return record_df.withColumn(
            "joined_column",
            sf.when(null_cond, "empty").otherwise(
                sf.concat(
                    sf.when(
                        sf.col("mainTitles").isNotNull(), sf.col("mainTitles")
                    ).otherwise(sf.lit(" ")),
                    sf.lit(" "),
                    sf.when(
                        sf.col("keywordsMerged").isNotNull(), sf.col("keywordsMerged")
                    ).otherwise(sf.lit(" ")),
                    sf.lit(" "),
                    sf.when(
                        sf.col("descriptions").isNotNull(), sf.col("descriptions")
                    ).otherwise(sf.lit(" ")),
                )
            ),
        )

    @staticmethod
    def add_language_column(record_df: DataFrame) -> DataFrame:
        """
        Add column `language_prediction`, which is prediction of language of given resource.
        """
        if "languagePrediction" not in record_df.columns:
            record_df = record_df.withColumn("language_prediction", sf.lit("en"))
        else:
            record_df = record_df.withColumnRenamed(
                "languagePrediction", "language_prediction"
            )
            record_df = record_df.withColumn(
                "language_prediction",
                sf.when(
                    (sf.col("isvalid"))
                    & (sf.col("language_prediction").isNull())
                    & (sf.col("languages") == "English"),
                    "en",
                ).otherwise(sf.col("language_prediction")),
            )

        return record_df

    def record_processing(self, record_df: DataFrame) -> DataFrame:
        """
        Process records in dataframe. Starts from mapping descriptions according to mapping file.
        Then add column `isvalid`, which tells if given resource is not trivial.
        Then join columns with title and descriptions and keywords into one column `joined_column`.
        At the end we use sparknlp pipeline to get embeddings for each record.
        """
        rows = record_df.count()
        num_partitions = max(1, rows // 8000)
        record_df = record_df.repartition(num_partitions)

        record_df = self.organize_columns(record_df)
        record_df = self.add_joined_column(record_df)
        record_df = self.add_language_column(record_df)
        record_df = record_df.drop(*utils.columns_to_drop)

        sparknlp.start()

        df_bert = self.pipeline.fit(record_df).transform(record_df)
        df_bert = df_bert.withColumn(
            "embeddings", utils.convert_udf(df_bert.finished_embeddings)
        )
        df_bert = df_bert.drop(*utils.columns_to_drop_2)

        return df_bert

    def launch(self, path_from: str, mode: str):
        """
        Launches the whole process of adding rows to postgres and hbase

        Parameters
        ----------
        path_from : str
            Path to csv file with data
        mode : str
            Mode of writing to postgres table
        """
        self.job_context.logger.info("Start Adding rows")
        self.create_postgres_table()
        self.create_hbase_table()

        self.pipeline = self.build_pipeline()

        add_dataset = self.job_context.spark_read_csv(path_from)
        df_bert = self.record_processing(add_dataset)
        df_to_write = df_bert.select(*utils.columns_to_select)

        # df_to_write.persist(self.job_context.storage_level)
        self.job_context.logger.info("Start writing to postgres and hbase")
        # self.job_context.logger.info(df_to_write.count())
        self.job_context.logger.info(df_to_write.columns)
        self.job_context.spark_df_to_postgres(df_to_write, mode)
        # self.job_context.spark_df_to_hbase(df_to_write)
        # df_to_write.unpersist()
