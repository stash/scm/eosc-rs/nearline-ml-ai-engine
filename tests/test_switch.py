# """
# Module for testing switch class.
# """
# import copy
# import unittest
# from unittest.mock import MagicMock
#
# from nearline_app.settings import settings
# from nearline_app.switch import Switch
# from tests.utils_mocks import (
#     input_data,
#     SingleVisitFunction,
#     switch_output,
#     MultiVisitFunction,
# )
#
#
# class TestSwitch(unittest.TestCase):
#     """
#     Test class, which tests switch class.
#     """
#
#     def setUp(self):
#         """
#         Set up method. Creates.switch object.
#         """
#         self.switch = Switch("Test", MagicMock(), MagicMock())
#         self.switch.processor = MagicMock()
#         # self.switch.hbase_processor = MagicMock()
#         self.data = copy.deepcopy(input_data)
#
#     def test_parse_user_action_data(self):
#         """
#         Test case for the parse_user_action_data function.
#         Check if function returns correct data for logged user.
#         """
#         self.switch.processor.update_resources = SingleVisitFunction("test")
#         self.data["isAnonymous"] = False
#         self.data["aaiUid"] = "test_aai_uid"
#         self.data["marketplaceId"] = "test_marketplace_id"
#         true_output = switch_output
#         self.switch.parse_user_action_data(self.data)
#
#         self.assertEqual(
#             self.switch.processor.update_resources.arg1, true_output
#         )
#
#     def test_parse_user_action_no_resource_id(self):
#         """
#         Test case for the parse_user_action_data function.
#         Check if when there is no resource id, function returns None.
#         """
#         self.data["userActions"][-1].pop("resourceId")
#         output = self.switch.parse_user_action_data(self.data)
#         self.assertEqual(output, False)
#
#     def test_parse_data_from_kafka(self):
#         """
#         Test case for the parse_data_from_kafka function.
#         """
#         self.switch.parse_user_action_data = SingleVisitFunction(
#             settings.KAFKA_TOPIC_UA
#         )
#         self.switch.parsa_user_event = SingleVisitFunction(settings.KAFKA_TOPIC_USER)
#         self.switch.parse_service_data = SingleVisitFunction(
#             settings.KAFKA_TOPIC_SERVICE
#         )
#         self.switch.parse_user_like_data = SingleVisitFunction(
#             settings.KAFKA_LIKE_TOPIC
#         )
#
#         for topic in [
#             settings.KAFKA_TOPIC_UA,
#             settings.KAFKA_TOPIC_USER,
#             settings.KAFKA_TOPIC_SERVICE,
#             settings.KAFKA_LIKE_TOPIC,
#             "other",
#         ]:
#             response = self.switch.parse_data_from_kafka({"id": "data",
#                                                           "sessionId": "data",
#                                                           "aaiId": "data",
#                                                           "visitId": "data"}, topic)
#         self.assertTrue(self.switch.parse_user_action_data.flag)
#         self.assertTrue(self.switch.parsa_user_event.flag)
#         self.assertTrue(self.switch.parse_service_data.flag)
#         self.assertTrue(self.switch.parse_user_like_data.flag)
#         self.assertEqual(response, False)
#
#     def test_parse_user_event(self):
#         """
#         Test case for the parse_user_event function.
#         """
#         data = {
#             "eventType": "test_event_type",
#             "aaiId": "test_aai_id",
#             "marketplaceId": "test_marketplace_id",
#             "scientificDomainValues": [1],
#             "categoryValues": [2],
#             "accessedServiceValues": [3],
#         }
#         self.switch.processor.update_user_info = SingleVisitFunction("update")
#         self.switch.processor.create_user = SingleVisitFunction("create")
#         for event in [
#             "update",
#             "create",
#             "other",
#         ]:
#             data["eventType"] = event
#             response = self.switch.parsa_user_event(data)
#         self.assertTrue(self.switch.processor.update_user_info.flag)
#         self.assertTrue(self.switch.processor.create_user.flag)
#         self.assertEqual(response, False)
#
#     def test_parse_service_data(self):
#         """
#         Test case for the parse_service_data function.
#         """
#         data = {
#             "eventType": None,
#             "id": "test_service_id",
#             "name": "test_service_name",
#             "description": "test_service_description",
#             "scientificDomainValues": [1],
#             "relatedServices": [2],
#             "categoryValues": [3],
#         }
#         self.switch.processor.update_service_info = SingleVisitFunction(
#             "update"
#         )
#         self.switch.processor.create_service = SingleVisitFunction("create")
#         for event in [
#             "update",
#             "create",
#             "other",
#         ]:
#             data["eventType"] = event
#             response = self.switch.parse_service_data(data)
#
#         self.assertTrue(self.switch.processor.update_service_info.flag)
#         self.assertTrue(self.switch.processor.create_service.flag)
#         self.assertEqual(response, False)
#
#     def test_parse_user_like_data(self):
#         """
#         Test case for the parse_user_like_data function.
#         """
#         new_data = {
#             "action": "temp",
#             "resourceId": "test_resource_id",
#             "resourceType": "test_resource_type",
#             "visitId": "test_visit_id",
#             "aaiUid": "test_aai_uid",
#             "uniqueId": "test_unique_id",
#         }
#         self.switch.processor.user_like = MultiVisitFunction("like")
#         for action, reaction in zip(["like", "dislike", "other"], [True, True, False]):
#             new_data["action"] = action
#             response = self.switch.parse_user_like_data(new_data)
#             self.assertEqual(response, reaction)
#
#     def test_resource_with_space(self):
#         self.data["userActions"][1]["resourceId"] = "test resource id"
#         a = self.switch.parse_user_action_data(self.data)
#         self.assertTrue(1 - a)
