"""
Scripts for initialization HBase tables.
"""
import happybase

from nearline_app.settings import settings


def create(name: str):
    """
    Create empty user table in HBase.

    Arguments:
        name (str): Table name
    """
    connection = happybase.Connection(
        settings.HBASE_SERVER, port=int(settings.HBASE_PORT)
    )
    current_tables = connection.tables()
    if bytes(name, "utf-8") not in current_tables:
        connection.create_table(
            name,
            {
                "services": {},
                "datasets": {},
                "publications": {},
                "software": {},
                "trainings": {},
                "other_research_product": {},
                "data_sources": {},
                "services_emb": {},
                "datasets_emb": {},
                "publications_emb": {},
                "software_emb": {},
                "trainings_emb": {},
                "other_research_product_emb": {},
                "data_sources_emb": {},
                "like": {},
                "dislike": {},
                "user_info": {},
            },
        )


def initiate_hbase(anon_user_table, logged_user_table, logged_user_aai_table):
    """
    Create empty user tables in HBase.
    Run this script before running the application.

    Arguments:
        anon_user_table (str): Anonymous user table name
        logged_user_table (str): Logged user table name
        logged_user_aai_table (str): Logged user AAI table name
    """
    connection = happybase.Connection(
        settings.HBASE_SERVER, port=int(settings.HBASE_PORT)
    )
    tables = connection.tables()

    for table in [
        anon_user_table,
        logged_user_table,
        logged_user_aai_table,
    ]:
        if table not in tables:
            create(table)
