"""
Module that contain class that contains all database operations for sub-Jobs.
"""
import copy
import csv
from typing import Dict

import psycopg2
from psycopg2.extensions import AsIs
from psycopg2.extensions import connection as Connection
from py4j.protocol import Py4JJavaError
from pyspark import SparkFiles
from pyspark import StorageLevel
from pyspark.sql import DataFrame

# pylint: disable=E0401

try:
    from logger_provider import LoggerProvider
    from udf_and_utils import (
        save_metadata,
        spark_df_to_hbase,
        spark_df_delete_from_hbase,
    )

except ImportError:
    from nearline_app.spark.logger_provider import LoggerProvider
    from nearline_app.spark.udf_and_utils import (
        save_metadata,
        spark_df_to_hbase,
        spark_df_delete_from_hbase,
    )


class JobContext(LoggerProvider):
    """
    Class that performs all database operations for sub-Jobs.
    """

    def __init__(
        self,
        spark_session,
        postgres_settings,
        hbase_settings,
        resource_type,
        bert_path,
    ):
        self.hbase_table_name = None
        self.spark = spark_session

        self.postgres_settings = postgres_settings
        self.hbase_settings = hbase_settings
        self.table_name = None

        self.resource_type = resource_type
        self.logger = self.get_logger(self.spark)

        self.bert_path = bert_path

        self.broadcast_mapping = None
        self.storage_level = StorageLevel.MEMORY_AND_DISK_2

    def spark_df_to_postgres(
        self, spark_df: DataFrame, mode: str, table_name: str = None
    ) -> None:
        """
        Saves spark dataframe to postgres table.

        :param spark_df: spark dataframe
        :param mode: mode of saving ("append", "overwrite")
        :param table_name: name of table
        """
        if self.postgres_settings.is_used:
            if table_name is None:
                table_name = self.table_name
            try:
                self.logger.info("Writing to postgres")
                spark_df.write.mode(mode).format("jdbc").option(
                    "url", self.postgres_settings.url
                ).option("driver", "org.postgresql.Driver").option(
                    "dbtable", table_name
                ).option(
                    "user", self.postgres_settings.user
                ).option(
                    "password", self.postgres_settings.password
                ).save()
            except Exception:
                self.logger.error("Some error during writing to postgres, skipping")

    def spark_read_csv(self, path: str) -> DataFrame:
        """
        Reads csv file to spark dataframe. Header and delimiter
        are set to True and ";" respectively.

        :param path: path to csv file
        :return: spark dataframe
        """
        try:
            return (
                self.spark.read.option("header", True)
                .option("sep", ";")
                .option("quote", '"')
                .option("escape", '"')
                .csv(path)
            )
        except Py4JJavaError as exc:
            self.logger.error("Some error during reading csv file, probably wrong path")
            raise Py4JJavaError from exc

    def spark_read_sql(self, query: str) -> DataFrame:
        """
        Reads sql query to spark dataframe.

        :param query: sql query
        :return: spark dataframe
        """
        return (
            self.spark.read.format("jdbc")
            .option("url", self.postgres_settings.url)
            .option("driver", "org.postgresql.Driver")
            .option("query", query)
            .option("user", self.postgres_settings.user)
            .option("password", self.postgres_settings.password)
            .load()
        )

    def create_postgres_connection(self) -> Connection:
        """
        Creates connection to postgres database.
        """
        if self.postgres_settings.is_used:
            url = copy.deepcopy(self.postgres_settings.url)
            host, port = url.split("/")[2].split(":")

            database = url.split("/")[3].split("?")[0]

            conn = psycopg2.connect(
                host=host,
                port=port,
                database=database,
                user=self.postgres_settings.user,
                password=self.postgres_settings.password,
            )

            return conn

    def perform_query(self, query: str, params: tuple) -> None:
        """
        Performs query on postgres database. Uses for creating and updating tables.

        :param query: query to perform
        :param params: parameters for query
        """
        if self.postgres_settings.is_used:
            conn, cur = self.get_cursor()
            cur.execute(query, params)
            conn.commit()

            cur.close()
            conn.close()

    def get_cursor(self):
        """
        Returns cursor to postgres database.
        """
        conn = self.create_postgres_connection()
        return conn, conn.cursor()

    def perform_query_with_return(self, query: str, params: tuple) -> list:
        """
        Performs query on postgres database and returns result.
        """
        if self.postgres_settings.is_used:
            conn, cur = self.get_cursor()
            cur.execute(query, params)
            out = cur.fetchall()

            cur.close()
            conn.close()

            return out

    def generate_mapping(self, mapping_path) -> Dict[str, str]:
        """
        Read mapping file from HDFS and return it as a dictionary.
        """
        mapping = {}
        self.spark.sparkContext.addFile(f"hdfs://{mapping_path}")
        file_name = mapping_path.split("/")[-1]

        with open(SparkFiles.get(file_name), "r", encoding="utf-8") as file:
            reader = csv.reader(file, delimiter=";")
            headers = next(reader)
            self.logger.info(f"Headers: {headers}")
            for row in reader:
                mapping[row[0]] = row[1]

        return mapping

    def get_table_name(self) -> str:
        """
        Get Postgres table name from metadata table.
        """
        query = """ SELECT "table name"
                    FROM metadata.%s
                    WHERE name = '%s'"""
        params = (
            AsIs(self.postgres_settings.metadata_table),
            AsIs(self.resource_type),
        )
        value = self.perform_query_with_return(query, params)
        return value[0][0]

    def startup(self, mapping_path):
        """
        Function that should be called at the beginning of the all jobs.
        """
        self.table_name = self.get_table_name()
        if "resources." not in self.table_name:
            self.table_name = f"resources.{self.table_name}"

        self.broadcast_mapping = self.spark.sparkContext.broadcast(
            self.generate_mapping(mapping_path)
        )

    def shutdown(self):
        """
        Function that should be called at the end of the all jobs.
        """
        save_metadata(self)

    def spark_df_to_hbase(self, add_df):
        """
        Saves spark dataframe to hbase table.
        """
        if self.hbase_settings.is_used:
            spark_df_to_hbase(
                add_df,
                self.table_name,
                self.hbase_settings.url,
                int(self.hbase_settings.port),
            )

    def delete_from_hbase(self, delete_df):
        """
        Deletes rows from hbase table.
        """
        if self.hbase_settings.is_used:
            spark_df_delete_from_hbase(
                delete_df,
                self.table_name,
                self.hbase_settings.url,
                int(self.hbase_settings.port),
            )
