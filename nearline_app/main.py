"""
FastApi nearline_app for supervision of user entity update process.
"""
import asyncio
import logging
import os
import sys

import requests
import uvicorn
from fastapi import FastAPI, BackgroundTasks, HTTPException
from psycopg2 import OperationalError
from thriftpy2.transport import TTransportException

from nearline_app.database_connectors.PostgresConnector import PostgresConnector
from nearline_app.dynamic_switch import DynamicSwitch
from nearline_app.embedders.BaseEmbedder import BaseEmbedder
from nearline_app.embedders.UserEmbedder import UserEmbedder
from nearline_app.kafka_runner import KafkaRunner
from nearline_app.settings import settings
from nearline_app.spark_brain import start_spark_job_diff
from nearline_app.utils import resource_types

VERSION = "1.5.4"
app = FastAPI(title=settings.PROJECT_NAME, version=VERSION)

logging.basicConfig(
    stream=sys.stdout,
    level=logging.getLevelName(settings.LOG_LEVEL),
    format="[%(asctime)s] [%(levelname)s] %(name)s: %(message)s",
    datefmt="%Y-%m-%d:%H:%M:%S",
)

logging.getLogger("uvicorn.access").setLevel(logging.getLevelName(settings.LOG_LEVEL))

logger = logging.getLogger(__name__)

try:
    dynamic_switch = DynamicSwitch()
    dynamic_switch.setup_switches(
        settings.NUMBER_OF_PROCESS_SWITCHES, settings.NUMBER_OF_STORING_SWITCHES
    )
    runner = KafkaRunner(dynamic_switch=dynamic_switch)
    postgres_connector = PostgresConnector()
except TTransportException:
    logger.error("HBase is not running")
    runner = None
except OperationalError:
    logger.error("Postgres is not running")
    runner = None


def validate_settings():
    """
    Function that validates environmental variables: checks if are set and if they
    do not contain spaces and if paths starts with `/`.
    """
    for variable in [
        settings.SPARK_COMMAND,
        settings.SPARK_PYTHON_ENVIRONMENT,
        settings.SPARK_JARS_FOLDER,
        settings.POSTGRES_HOST,
        settings.POSTGRES_PORT,
        settings.POSTGRES_DATABASE,
        settings.POSTGRES_USER,
        settings.POSTGRES_PASSWORD,
        settings.SPARK_BERT_PATH,
        settings.SPARK_MAPPING_PATH,
        settings.ANON_USERS_TABLE,
        settings.LOGGED_USERS_TABLE,
        settings.LOGGED_USERS_AAI_TABLE,
    ]:
        if variable == "":
            logger.info("Some environmental variable is not set")
            raise HTTPException(
                status_code=403, detail="Some environmental variable is not set"
            )

        if " " in variable:
            logger.info("No environmental variable can contains spaces")
            raise HTTPException(
                status_code=404, detail="No environmental variable can contains spaces"
            )

    for variable in [
        settings.SPARK_PYTHON_ENVIRONMENT,
        settings.SPARK_JARS_FOLDER,
        settings.SPARK_BERT_PATH,
        settings.SPARK_MAPPING_PATH,
    ]:
        if not variable.startswith("/"):
            logger.info("Environmental variable that is path to has to start with /")
            raise HTTPException(
                status_code=405,
                detail="Path to environmental variable has to start with /",
            )


@app.on_event("startup")
async def startup():
    """
    During initialization of FastAPI, start Kafka consumer and print
    version of the nearline_app.
    """
    if settings.DATABASE_STORAGE not in [
        "POSTGRES",
        "HBASE",
        "POSTGRES:HBASE",
        "HBASE:POSTGRES",
    ]:
        logger.error("DATABASE_STORGE is set incorrectly")
        os._exit(1)

    if not isinstance(settings.NUMBER_OF_STORING_SWITCHES, int) or not isinstance(
        settings.NUMBER_OF_PROCESS_SWITCHES, int
    ):
        logger.error(
            "NUMBER_OF_STORING_SWITCHES or NUMBER_OF_PROCESS_SWITCHES is not int"
        )
        os._exit(1)

    if (
        settings.NUMBER_OF_STORING_SWITCHES < 1
        or settings.NUMBER_OF_PROCESS_SWITCHES < 1
    ):
        logger.error(
            "NUMBER_OF_STORING_SWITCHES or NUMBER_OF_PROCESS_SWITCHES is less than 1"
        )
        os._exit(1)

    logger.warning("%s, Version %s", settings.PROJECT_NAME, VERSION)
    if runner:
        for switch in dynamic_switch.process_switches:
            resource_embedder = BaseEmbedder()
            user_embedder = UserEmbedder()
            resource_embedder.load_model()
            user_embedder.load_models()
            switch.setup_processor(resource_embedder, user_embedder)

        logger.info("All models connected, Starting Kafka consumer")
        asyncio.create_task(runner.start())
        for switch in dynamic_switch.process_switches:
            asyncio.create_task(dynamic_switch.process_messages(switch))

        for switch in dynamic_switch.storing_switches:
            asyncio.create_task(dynamic_switch.store_messages(switch))
        asyncio.create_task(dynamic_switch.handle_storing_list())
    else:
        logger.error("Kafka consumer not started")
        os._exit(1)


@app.on_event("shutdown")
async def shutdown_event():
    """
    During shutdown of FastAPI, stop all running tasks.
    """
    dynamic_switch.shutdown_flag = True


@app.post(
    "/generate_embeddings_diff",
    tags=["spark"],
    summary="Generate embeddings with spark job",
)
async def generate_embeddings_diff(
    background_tasks: BackgroundTasks,
    path_from: str,
    resource_type: str,
):
    """
    Generate embeddings with spark job (differential version). Tables name will be taken from tables
    metadata in postgres.

    ### Args:

    * **path_from** (str): path to the folder on HDFS with data (example: /user/(...)/training). It has to point to
                                the directory with `data` and/or `digest` subdirectories
    * **resource_type** (str): type of resource (one of "data_sources", "services", "software",
                                "other_research_product", "publications", "datasets", "trainings")
    """
    if path_from == "" or resource_type == "":
        raise HTTPException(status_code=400, detail="Missing parameter")

    if resource_type not in resource_types:
        raise HTTPException(status_code=401, detail="Wrong resource type")

    if " " in path_from:
        raise HTTPException(status_code=402, detail="Wrong path")

    validate_settings()

    for switch in dynamic_switch.process_switches:
        resp = switch.database_connector.health_check()
        if resp["status"] != "UP":
            raise HTTPException(
                status_code=406, detail="Postgres or HBase is not running"
            )
    for switch_list in dynamic_switch.storing_switches:
        for switch in switch_list:
            resp = switch.database_connector.health_check()
            if resp["status"] != "UP":
                raise HTTPException(
                    status_code=406, detail="Postgres or HBase is not running"
                )

    background_tasks.add_task(start_spark_job_diff, path_from, resource_type)

    return {"message": "Task started"}


@app.get("/diag", tags=["diagnose"], summary="Diagnostic endpoint")
async def diag():
    """
    Diagnostic endpoint, returns status of:
    * **Kafka** consumer
    * **HBase** connection
    * **Preprocessor**
    * **PostGreSQL** connection and tables
    """
    resp = {"status": "UP", "version": VERSION}

    if settings.PREPROCESSOR_DIAG_ENDPOINT != "":
        request = requests.get(
            settings.PREPROCESSOR_DIAG_ENDPOINT,
            timeout=15,
        )
        resp["preprocessor"] = request.json()

    counter = len(dynamic_switch.process_switches) + len(
        dynamic_switch.storing_switches
    ) * len(dynamic_switch.storing_switches[0])

    for switch in dynamic_switch.process_switches:
        resp[switch.name] = switch.database_connector.health_check()
        if resp[switch.name]["status"] != "UP":
            counter -= 1

    for switch_list in dynamic_switch.storing_switches:
        for switch in switch_list:
            resp[switch.name] = switch.database_connector.health_check()
            if resp[switch.name]["status"] != "UP":
                counter -= 1

    if counter == 0:
        resp["status"] = "DOWN"
    elif counter < len(dynamic_switch.process_switches) + len(
        dynamic_switch.storing_switches
    ):
        resp["status"] = "DEGRADED"

    resp["kafka"] = runner.health_check()
    if resp["kafka"]["status"] != "UP":
        resp["status"] = "DOWN"

    resp["postgres"] = {"status": postgres_connector.health_check()["status"]}
    resp["dynamic_switch"] = dynamic_switch.health_check()
    return resp


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", log_level="info")
