import json
import random
import string
import time

import pandas as pd
from confluent_kafka import Producer

from resources import resources


def random_name():
    return "".join(random.choice(string.ascii_lowercase) for i in range(10))


def random_resource():
    resource_type = random.choice(list(resources.keys()))
    resource_id = random.choice(resources[resource_type])
    return resource_type, resource_id


class Generator:
    def __init__(self, kafka_server, kafka_topic):
        self.name = random_name()
        self.conf = {"bootstrap.servers": kafka_server}
        self.running = True
        self.counter = 0
        self.new_names = 0
        self.kafka_topic = kafka_topic

    def generate(self):
        resource_type, resource_id = random_resource()

        if random.random() < 0.2:
            self.name = random_name()
            self.new_names += 1

        message = {
            "sessionId": self.name,
            "isAnonymous": True,
            "numberOfUA": 1,
            "userActions": [
                {
                    "timestamp": str(pd.Timestamp.now()),
                    "pageId": "no_matter",
                    "resourceType": resource_type,
                    "resourceId": resource_id,
                    "rootType": "other",
                    "actionType": "browser action",
                    "isOrderAction": False,
                }
            ],
        }

        return message

    def main_loop(self):
        while self.running:
            self.counter += 1
            message = self.generate()
            self.produce(message)
            time.sleep(0.1)

    def produce(self, message):
        producer = Producer(**self.conf)
        producer.produce(self.kafka_topic, json.dumps(message))
        producer.flush()
