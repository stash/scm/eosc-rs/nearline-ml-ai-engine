"""
This module contains a class for embedding users.
"""
from typing import List

import torch
from nearline_app.embedders.NewTripletNetwork import NewTripletNetwork
from nearline_app.embedders.resources.vocab import vocab
from nearline_app.settings import settings


class UserEmbedder:
    """
    Class for embedding users.
    """

    def __init__(self, vocab_dict=vocab, model_weights_path=settings.MODEL_PATH):
        self.vocab = vocab_dict
        self.user_model = None
        self.language_model = None
        self.device = torch.device("cpu")
        self.model_weights_path = model_weights_path

    def load_models(self) -> None:
        """
        Loads main model, load weights for it from a given path and then take sub models from it.
        """
        model = NewTripletNetwork(
            mlp_layers=[256, 128, 64, 32], vocab_size=len(self.vocab)
        ).to(self.device)
        model.load_state_dict(
            torch.load(self.model_weights_path, map_location=self.device)
        )

        self.user_model = model.user_encoder
        self.language_model = model.embedding

    def get_language_tokens(self, languages: List[str]) -> torch.Tensor:
        """
        Returns a tensor of tokens of languages.
        """
        return torch.as_tensor(
            [self.vocab.get(word, vocab["unk"]) for word in languages]
        ).reshape(1, -1)

    def get_user_embedding(
        self, resource_embedding: torch.Tensor, languages: List[str]
    ) -> torch.Tensor:
        """
        Returns an embedding of a user.

        Args:
            resource_embedding: a matrix containing embeddings of items visited by a user.
                The shape must be as follows: (1, 1, embedding_size, num_resources)
                    where: batch_size is the number of users in a batch
                    1: the number of channels
                    embedding_size: the size of an embedding (basically 256)
                    num_resources: the number of resources visited by a user
            languages: a list of languages of resources visited by a user.
        """
        resource_embedding = torch.from_numpy(resource_embedding)
        language_tokens = self.get_language_tokens(languages)
        language_embedding = (
            self.language_model(language_tokens)
            .unsqueeze(1)
            .movedim(3, 2)[:, :, :, 0]
            .unsqueeze(3)
            .expand(-1, -1, -1, resource_embedding.shape[-1])
        )
        resource_embedding = torch.cat((resource_embedding, language_embedding), dim=2)
        user_embedding = self.user_model(resource_embedding)  # 1, 1, 256, 1
        return user_embedding.squeeze()
