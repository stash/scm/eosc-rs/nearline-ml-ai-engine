# pylint: disable-all
from typing import List, Union, Dict, Any
from abc import ABC, abstractmethod
import pandas as pd


class DataBaseConnector(ABC):
    """
    Abstract class, which implements interface for Processor class.
    """

    @abstractmethod
    def get_embeddings(
        self, table_name: str, resources_id: List[str]
    ) -> Union[pd.DataFrame, None]:
        raise NotImplementedError

    @abstractmethod
    def get_embeddings_with_languages(
        self, table_name: str, resources_id: List[str]
    ) -> Union[pd.DataFrame, None]:
        raise NotImplementedError

    @abstractmethod
    def single_update_user(
        self, table_name: str, user_id: str, column: str, value: Union[str, bytes]
    ) -> None:
        raise NotImplementedError

    @abstractmethod
    def update_user(self, table_name: str, update_data: Dict[str, Any]) -> None:
        raise NotImplementedError

    @abstractmethod
    def update_user_with_embeddings(
        self, table_name: str, update_data: Dict[str, Any]
    ) -> None:
        raise NotImplementedError

    @abstractmethod
    def insert_new_user_with_embeddings(
        self, table_name: str, insert_data: Dict[str, Any]
    ) -> None:
        raise NotImplementedError

    @abstractmethod
    def insert_new_user(self, table_name: str, insert_data: Dict[str, Any]) -> None:
        raise NotImplementedError

    @abstractmethod
    def get_user(self, user_id: str, table_name: str) -> Union[None, Dict[str, Any]]:
        raise NotImplementedError

    @abstractmethod
    def update_resource(
        self, service_id: str, categories: str, desc_embed: bytes, resource_type: str
    ) -> None:
        raise NotImplementedError

    @abstractmethod
    def insert_new_resource(
        self, service_id: str, categories: str, desc_embed: bytes, resource_type: str
    ) -> None:
        raise NotImplementedError

    @abstractmethod
    def insert_new_empty_user(self, table_name: str, user_id: str) -> None:
        raise NotImplementedError

    @abstractmethod
    def health_check(self) -> dict:
        raise NotImplementedError
