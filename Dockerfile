#FROM openjdk:11.0.15-jdk
FROM python:3.9.13-bullseye

RUN apt-get update && \
    apt-get install -y openjdk-11-jdk && \
    apt-get clean;

# Setup JAVA_HOME, this is useful for docker command-line
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64/
RUN export JAVA_HOME

ENV HADOOP_HOME=/opt/hadoop \
    HADOOP_CONF_DIR=/opt/hadoop/etc/hadoop \
    SPARK_HOME=/opt/spark \
    HADOOP_USER_NAME=hadoop

RUN echo "preproc:x:1001070000:0:::" >> /etc/passwd

RUN \
  wget --progress=dot:giga https://dlcdn.apache.org/hadoop/common/hadoop-3.3.1/hadoop-3.3.1.tar.gz && \
  tar -xzf hadoop-3.3.1.tar.gz && \
  rm hadoop-3.3.1.tar.gz && \
  mv hadoop-3.3.1 $HADOOP_HOME && \
  echo "PATH=$PATH:$HADOOP_HOME/bin" >> ~/.bashrc

RUN \
    wget --progress=dot:giga https://archive.apache.org/dist/spark/spark-3.2.2/spark-3.2.2-bin-hadoop3.2.tgz && \
    tar xvf spark-3.2.2-bin-hadoop3.2.tgz && \
    rm spark-3.2.2-bin-hadoop3.2.tgz && \
    mv spark-3.2.2-bin-hadoop3.2 $SPARK_HOME && \
    echo "export SPARK_HOME=$SPARK_HOME" >> ~/.bashrc && \
    echo "export PATH=$PATH:$SPARK_HOME/bin:$SPARK_HOME/sbin" >> ~/.bashrc

COPY /nearline_app/spark /nearline_app/spark

WORKDIR /nearline_app


COPY requirements.txt requirements.txt

RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir -r requirements.txt

COPY nearline_app/ nearline_app/

RUN mkdir ./cache
RUN mkdir /.cache

RUN chmod -R g=rwx ./cache
RUN chmod -R g=rwx /.cache

RUN mkdir ./log
RUN chmod -R g=rwx ./log

RUN mkdir -p /.ivy2/cache
RUN mkdir -p /.ivy2/jars

RUN chmod -R g=rwx /.ivy2/cache
RUN chmod -R g=rwx /.ivy2/jars

EXPOSE 8000



