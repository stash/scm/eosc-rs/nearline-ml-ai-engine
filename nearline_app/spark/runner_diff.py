"""
This module is used to run the spark job of differential processing of resources.
"""
# pylint: disable=E0401

import os

from pyspark.sql import SparkSession
from pyspark.sql.utils import AnalysisException

from adding_job import AddingJob
from job import JobContext
from deletion_job import DeletionJob
from update_content_job import UpdateContentJob
from update_id_job import UpdateIdJob
from udf_and_utils import get_parser, DatabaseSettings

os.environ["TF_ENABLE_ONEDNN_OPTS"] = "1"

parser = get_parser()
if __name__ == "__main__":
    args = parser.parse_args()

    added_path = args.path_from + "/data/added"
    changed_content_path = args.path_from + "/data/changed_content"
    deleted_path = args.path_from + "/digest/deleted"
    unstable_id_path = args.path_from + "/digest/unstable_id"

    postgres_url = f"jdbc:postgresql://{args.postgres_host}:{args.postgres_port}/{args.postgres_db}"

    postgres_settings = DatabaseSettings(
        postgres_url,
        args.postgres_user,
        args.postgres_password,
        is_used="POSTGRES" in args.database_used,
        metadata_table=args.metadata_table,
    )

    hbase_settings = DatabaseSettings(
        url=args.hbase_server,
        port=args.hbase_port,
        is_used="HBASE" in args.database_used,
    )

    spark_session = SparkSession.builder.getOrCreate()

    job_context = JobContext(
        spark_session,
        postgres_settings,
        hbase_settings,
        args.resource_type,
        args.bert_path,
    )
    job_context.startup(args.mapping_path)

    app = AddingJob(job_context)
    try:
        app.launch(added_path, "append")
    except AnalysisException:
        job_context.logger.warn(f"There is no such path {added_path}")

    app = DeletionJob(job_context)
    try:
        app.launch(deleted_path)
    except AnalysisException:
        job_context.logger.warn(f"There is no such path {deleted_path}")

    app = UpdateIdJob(job_context, args.user_tables)
    try:
        app.launch(unstable_id_path)
    except AnalysisException:
        job_context.logger.warn(f"There is no such path {unstable_id_path}")

    app = UpdateContentJob(job_context)
    try:
        app.launch(changed_content_path, "overwrite")
    except AnalysisException:
        job_context.logger.warn(f"There is no such path {changed_content_path}")

    job_context.shutdown()
