"""
Mocks for Hbase and KafkaRunner test.
"""

import contextlib
import copy
import unittest
from collections import defaultdict

import happybase


class MockTable:
    """
    Mocks for HBase table.
    """

    def __init__(self, table_name):
        self.table_name = table_name
        self.data = defaultdict(dict)

    def put(self, row: bytes, data: dict):
        """
        Put data into the table.
        """
        self.data[row].update(data)

    def row(self, row: str, columns=None) -> dict:
        """
        Return given row data, do not care for columns.
        """
        return self.data[bytes(row, "utf-8")]

    def scan(self, **_options):
        """
        Return all rows in the table, do not care for options.
        """
        return self.data.items()

    def rows(self, rows, columns=None):
        counter = 0
        rows = [bytes(row, "utf-8") for row in rows]
        for row in rows:
            if row in self.data.keys():
                counter += 1
                if columns:
                    for col in columns:
                        self.data[row][bytes(col, "utf-8")] = self.data[row][col]
                else:
                    self.data[row] = self.data[row]
        if counter == 0:
            return None

        return [(row, self.data[row]) for row in rows if self.data[row] != {}]


class MockConnection:
    """
    Mocks for HBase connection.
    """

    _instance = None
    tables = {}

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(MockConnection, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self, *args, **kwargs):
        self._opened = False

    @staticmethod
    def is_table_enabled(_name: str) -> bool:
        """
        Returns True if table is enabled(always).
        """
        return True

    def table(self, name: str) -> MockTable:
        """
        Returns table of a given name.
        """
        name = bytes(name, "utf-8")
        table = self.tables.get(name)
        if not table:
            table = MockTable(name)
            self.tables[name] = table
        return table

    def flush(self):
        """
        Flushes all tables.
        """
        self.tables = {}

    def open(self):
        self._opened = True

    def close(self):
        self._opened = False


class MockConnectionPool:
    def __init__(self, size, **kwargs):
        self._conn = MockConnection(**kwargs)

    @contextlib.contextmanager
    def connection(self, timeout=None):
        self._conn.open()
        yield self._conn

    def rows(self, table_name, rows_ids, columns=None):
        table = self._conn.table(table_name)
        return table.rows(rows_ids, columns)

    def put(self, table_name, row_id, data):
        table = self._conn.table(table_name)
        table.put(bytes(row_id, "utf-8"), data)

    def row(self, table_name, row_id, columns=None):
        table = self._conn.table(table_name)
        return table.row(row_id, columns)

    def tables(self):
        return self._conn.tables


class HBaseTestCase(unittest.TestCase):
    """
    Test class for HBase. It mocks the HBase connections.
    """

    def setUp(self):
        """
        Mocks the HBase connections.
        """

        self.og_connection = copy.deepcopy(happybase.Connection)
        happybase.Connection = MockConnection
        MockConnection().flush()

    def tearDown(self):
        """
        Restores the HBase original connections classes.
        """
        happybase.Connection = self.og_connection
