"""
A bunch of static methods, TypedDict's type hints, and static lists.
"""
import json
import logging
from typing import TypedDict, List, Union, Dict, Optional

import numpy as np
import pandas as pd

from nearline_app.settings import settings

logger = logging.getLogger(__name__)


def transform_float(string: bytes) -> List[float]:
    """
    Transform string to list of floats

    Arguments:
        string (str): String to transform

    Returns:
        List[float]: List of floats
    """
    string = convert_string(string)
    if len(string) > 4:
        return list(map(float, string[1:-1].split(",")))

    return []


def transform_bin(string: bytes) -> Union[object, List[float]]:
    """
    Transform binary string to list of floats

    Arguments:
        string: Bytes to transform

    Returns:
        List[float]: List of floats
    """
    if string != b"[]":
        return np.frombuffer(string, dtype=np.float32).tolist()

    return []


def transform_int(string: bytes) -> List[int]:
    """
    Transform string to list of ints

    Arguments:
        string (str): String to transform

    Returns:
        List[float]: List of ints
    """
    string = convert_string(string)
    if len(string) > 2:
        if "," in string:
            return list(map(int, string[1:-1].split(",")))

        return list(map(int, string[1:-1].split()))

    return []


def transform(data):
    """
    Transform data from HBase to user entity
    """
    try:
        result = np.frombuffer(data[b"embeddings:embeddings"], dtype="float32")
    except Exception as error:
        logging.error(error)
        raise ValueError
    return result


def transform_languages(data):
    """
    Transform data from HBase to user entity
    """
    try:
        result = json.loads(data[b"resource_info:language_prediction"])
    except Exception as error:
        logging.error(error)
        raise ValueError
    return result


def transform_str(string: bytes) -> str:
    """
    Transform bytes of string to list of strings

    Arguments:
        string (bytes): String to transform

    Returns:
        List[str]: List of strings
    """
    string = json.loads(string)
    return json.dumps([str(x) for x in string])


def convert_string(value: bytes) -> Union[str, None, bytes]:
    """
    Convert value to string
    """
    if pd.isna(value):
        return value

    return value.decode("utf8")


def fill_data(data):
    """
    Fill user data with empty dict which represents user entiy
    """
    for suffix in ["", "_emb"]:
        for resource in resource_types:
            for typ in [":visited", ":ordered"]:
                data[resource + suffix + typ] = []

    data["user_info:scientific_domains"] = []
    data["user_info:categories"] = []
    data["user_info:user_constraint"] = []
    return data


def clean_data(data: Dict[bytes, bytes], user_id: str):
    """
    Clean data from HBase to user entity
    Create empty dict, and fill it with data from HBase
    Transform columns with embeddings from binary to list of floats
    Transform columns with lists of resources from binary to list of strings

    Arguments:
        data (dict): Data from HBase
        user_id (str): User ID
    """
    cleaned_data = {}
    embeddings_keys = [a for a in data.keys() if b"emb" in a]
    for key in embeddings_keys:
        cleaned_data[key.decode("utf-8")] = transform_bin(data[key])
    rest_keys = list(set(data.keys()) - set(embeddings_keys))
    for key in rest_keys:
        cleaned_data[key.decode("utf-8")] = transform_str(data[key])

    cleaned_data["user"] = user_id
    return cleaned_data


def clean_data_test(data, user_id):
    """
    Version of clean_data for testing
    """
    cleaned_data = {}
    for key in data.keys():
        if "emb" in key:
            cleaned_data[key] = transform_bin(data[key])
        else:
            cleaned_data[key] = transform_str(data[key])
    cleaned_data["user"] = user_id
    return cleaned_data


def get_empty_user():
    """
    Create empty user. Uses in both HBase- and Postgres-Connectors.
    """
    insert_data = {
        "user_info:scientific_domains": json.dumps([]),
        "user_info:categories": json.dumps([]),
        "user_info:user_constraint": json.dumps([]),
        "user_info:projects": json.dumps([]),
    }
    suffix = ""
    for resource in resource_types:
        for typ in [":visited", ":ordered"]:
            insert_data[resource + suffix + typ] = json.dumps([])
    suffix = "_emb"
    for resource in resource_types:
        for typ in [":visited", ":ordered"]:
            insert_data[resource + suffix + typ] = (
                np.zeros(256).astype("float32").tobytes()
            )
    for resource in resource_types:
        for like_type in ["like", "dislike"]:
            insert_data[like_type + ":" + resource] = json.dumps([])
    return insert_data


name_map = {
    "service": "services",
    "dataset": "datasets",
    "publication": "publications",
    "software": "software",
    "training": "trainings",
    "other": "other_research_product",
    "data source": "data_sources",
}

resource_types = list(name_map.values())

kafka_conf = {
    "bootstrap.servers": settings.KAFKA_SERVER,
    "group.id": settings.KAFKA_GROUP_ID,
    "enable.auto.commit": False,
    "session.timeout.ms": 45000,
    "max.poll.interval.ms": 600000,
    # "error_cb": error_callback,
    "auto.offset.reset": settings.KAFKA_AUTO_OFFSET_RESET,
}

current_columns = {
    "embeddings",
    "isvalid",
    "id",
    "relations",
    "language_prediction",
    "bestaccessright",
    "categories",
}


class UserAction(TypedDict):
    """
    Class for user action
    """

    timestamp: str
    pageId: str
    resourceType: str
    resourceId: str
    rootType: str
    actionType: str
    isOrderAction: bool


class KafkaMessage(TypedDict):
    """
    Class for Kafka message
    """

    sessionId: str
    isAnonymous: str
    aaiUid: str
    marketplaceId: str
    numberOfUA: str
    userActions: List[UserAction]


class UserActionParsed(TypedDict):
    """
    Class for parsed user action
    """

    isAnonymous: bool
    resourceId: str
    isOrderAction: bool
    resourceType: str
    sessionId: str
    aaiUid: Optional[Union[str, None]]
    marketplaceId: Optional[Union[str, None]]
    user: Optional[Union[str, None]]


class UserEventMessage(TypedDict):
    """
    Class for user event message
    """

    eventType: str
    aaiId: str
    marketplaceId: str
    scientificDomainValues: List[int]
    categoryValues: List[int]
    accessedServiceValues: List[int]


class ServiceEventMessage(TypedDict):
    """
    Class for service event message
    """

    eventType: str
    id: str
    name: str
    description: str
    scientificDomainValues: List[int]
    relatedServices: List[int]
    categoryValues: List[int]


class ParsedServiceEvent(TypedDict):
    """
    Class for parsed service event message
    """

    id: str
    description: str
    categories: List[int]


class ParsedEventMessage(TypedDict):
    """
    Class for parsed event message
    """

    marketplaceId: str
    aaiUid: str
    categories: List[int]
    scientific_domains: List[int]


class UserLikeEvent(TypedDict):
    """
    Class for parsed user like event
    """

    action: str
    resourceId: str
    resourceType: str
    visitId: str
    aaiUid: Optional[str]
    uniqueId: Optional[str]
    user: Optional[str]
