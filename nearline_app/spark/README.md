# How to prepare python environment for spark.

This instruction is based on [this](https://gist.github.com/seanorama/094451c21008b72daa1ee8cbd706f2ba).

Most important thing is to upload all files to HDFS from host with **same** Operating System and CPU architecture as the cluster,
such as an edge host.

1. Install python and python-virtualenv
2. Create virtual environment for python and activate it. I named it `sample-env` but it could be any name.
    ```bash
    python3 -m venv sample-env --copies
    source sample-env/bin/activate
    ```
3. Install requirements
    ```bash
    pip install -U pip
    pip install pandas numpy spark-nlp==5.0.1 fastparquet psycopg2-binary happybase
    deactivate
    ```
4. Tar the environment
    ```bash
    cd sample-env
    tar -hzcf ../sample-env.tar.gz *
    cd ..
    ```
5. Put this file into hdfs. I put this into /share/python-envs/ folder.
    ```bash
    hdfs dfs -put -f sample-env.tar.gz /share/python-envs/
    hdfs dfs -chmod 0664 /share/python-envs/sample-env.tar.gz
    ```
6. Set the `SPARK_PYTHON_ENVIRONMENT` variable in .env file to the path of the environment in hdfs.
    ```
    SPARK_PYTHON_ENVIRONMENT=/share/python-envs/test-venv.tar.gz
    ```
7. Put Jars into HDFS and add paths to them in .env file. They can be downloaded from:
[Postgres JAR](https://mvnrepository.com/artifact/org.postgresql/postgresql/42.5.4) and
[Spark-NLP JAR](https://s3.amazonaws.com/auxdata.johnsnowlabs.com/public/jars/spark-nlp-assembly-5.0.1.jar)
    ```bash
    hdfs dfs -put -f postgresql-42.5.4.jar /share/python-envs/jars
    hdfs dfs -put -f spark-nlp-assembly-4.4.4.jar /share/python-envs/jars

    SPARK_JARS_FOLDER=/share/python-envs/jars
    ```
8. Put file with mapping(which is in form of csv file with 2 columns without headers) into HDFS
and add path to it in .env file.
    ```bash
    SPARK_MAPPING_PATH=/share/python-envs/gbif_mapping.csv
    ```
9. Put folder with model into HDFS and add path to it in .env file.
    ```bash
    SPARK_BERT_PATH=/share/python-envs/models/bert-mini
    ```
10. *Optional*: change default values for spark-submit command arguments:
    ```bash
    SPARK_EXECUTOR_CORES=3
    SPARK_DRIVER_CORES=2
    SPARK_NUM_EXECUTORS=2
    SPARK_EXECUTOR_MEMORY=5g
    ```