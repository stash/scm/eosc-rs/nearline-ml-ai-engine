"""
New model implementation
"""
from typing import List

import torch
from torch import nn


class NewTripletNetwork(nn.Module):
    """
    A PyTorch implementation of the Triplet Network.
    """

    def __init__(
        self,
        *args,
        mlp_layers: List[int],
        dropout: float = 0.3,
        vocab_size: int = 44,
        language_embedding_size: int = 16,
        **kwargs
    ):
        """
        Args:
            mlp_layers: number of layers in the MLP. The first layer is the concatenation of user and item embeddings.
            dropout: dropout probability
            vocab_size: size of the vocabulary of languages
            language_embedding_size: size of the language embedding
        """
        super().__init__(*args, **kwargs)

        self.dropout = dropout

        # first part of the network always accepts the embedding a size 528 (256 + 256 + 16), as for the input data
        mlp_modules = [
            nn.Linear(512, mlp_layers[0]),
            nn.Dropout(p=self.dropout),
            nn.ELU(),
        ]
        for i in range(len(mlp_layers) - 1):
            mlp_modules.append(nn.Linear(mlp_layers[i], mlp_layers[i + 1]))
            mlp_modules.append(nn.Dropout(p=self.dropout))
            mlp_modules.append(nn.ELU())
        self.mlp_layers = nn.Sequential(*mlp_modules)
        self.user_encoder = nn.Sequential(
            nn.Conv2d(1, 4, kernel_size=(13, 1)),
            nn.BatchNorm2d(4),
            nn.ELU(),
            nn.Conv2d(4, 8, kernel_size=(11, 1)),
            nn.BatchNorm2d(8),
            nn.ELU(),
            nn.Conv2d(8, 4, kernel_size=(7, 1)),
            nn.BatchNorm2d(4),
            nn.ELU(),
            nn.Conv2d(4, 1, kernel_size=(1, 1)),
            nn.AdaptiveAvgPool2d((256, 1)),
        )

        self.embedding = nn.Embedding(
            vocab_size, language_embedding_size, padding_idx=0
        )

        self.predict_layer = nn.Linear(mlp_layers[-1], 1)
        self.sigmoid = nn.Sigmoid()

    def forward(
        self,
        user_embedding: torch.Tensor,
        item_embedding: torch.Tensor,
        language_tokens: torch.Tensor,
    ):
        """
        Forward loop.

        First step is to aggregate a user matrix into a single vector. Using this vector and item vector,
        the network will predict how likely the user is to visit the item.

        Args:
            user_embedding: a matrix containing embeddings of items visited by a user.
                The shape must be as follows: (batch_size, 1, embedding_size, num_resources)
                    where: batch_size is the number of users in a batch
                    1: the number of channels
                    embedding_size: the size of an embedding (basically 256)
                    num_resources: the number of resources visited by a user
            item_embedding: an embedding of an item
                The shape must be as follows: (batch_size, embedding_size)
                    where: batch_size is the number of users in a batch
                    embedding_size: the size of an embedding (basically 256)
            language_tokens: a tensor containing tokens of languages of resources visited by a user.
                The shape must be as follows: (batch_size, self.max_length) (padded vector of tokens)
        """
        language_embedding = (
            self.embedding(language_tokens)
            .unsqueeze(1)
            .movedim(3, 2)[:, :, :, 0]
            .unsqueeze(3)
            .expand(-1, -1, -1, user_embedding.shape[-1])
        )

        user_embedding = torch.cat((user_embedding, language_embedding), dim=2)

        parsed_user = self.user_encoder(user_embedding)
        parsed_user = torch.squeeze(parsed_user)

        # add dummy batch dimension if needed
        if len(parsed_user.shape) == 1:
            parsed_user = parsed_user.unsqueeze(0)

        x = torch.cat((parsed_user, item_embedding), dim=1).float()

        x = self.mlp_layers(x)
        x = self.predict_layer(x)
        x = self.sigmoid(x)
        return x, parsed_user

    @staticmethod
    def get_name() -> str:
        """
        Utility method for getting the name of the model.
        Returns:
            the name of the model
        """
        return "NewTripletNetwork"
