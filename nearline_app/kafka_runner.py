"""
KafkaRunner Module. It is used to run Kafka consumer.
This Kafka should read topic with user actions.
"""
import asyncio
import json
import logging
from concurrent.futures import ThreadPoolExecutor
from json.decoder import JSONDecodeError

import pandas as pd
from confluent_kafka import Consumer, TopicPartition, cimpl
from thriftpy2.transport import TTransportException

from nearline_app.dynamic_switch import DynamicSwitch
from nearline_app.settings import settings
from nearline_app.utils import kafka_conf

logger_kafka = logging.getLogger("consumer")
logger_kafka.setLevel(logging.getLevelName(settings.LOG_LEVEL))
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter("%(asctime)-15s %(levelname)-8s %(message)s"))
logger_kafka.addHandler(handler)

logger = logging.getLogger(__name__)
_executor = ThreadPoolExecutor(1)


class KafkaRunner:
    """
    Arguments:
        user_action_topic (str): Kafka topic with user actions
        user_topic (str): Kafka topic with user events
        service_topic (str): Kafka topic with service events
        consumer (confluent_kafka.Consumer): Kafka consumer
        switch_creator SwitchCreator: class, that creates switches

    Attributes:
        consumer (confluent_kafka.Consumer): Kafka consumer
        dynamic_switch (DynamicSwitch): DynamicSwitch object
        last_message (confluent_kafka.Message): Last message consumed
        paused (bool): If true, consumer is paused
        last_read (pandas.Timestamp): Timestamp of last message consumed
        topics (str): List of Kafka topics to consume from
    """

    def __init__(
        self,
        user_action_topic: str = settings.KAFKA_TOPIC_UA,
        user_topic: str = settings.KAFKA_TOPIC_USER,
        service_topic: str = settings.KAFKA_TOPIC_SERVICE,
        training_topic: str = settings.KAFKA_TOPIC_TRAINING,
        like_topic: str = settings.KAFKA_LIKE_TOPIC,
        consumer: Consumer = Consumer(kafka_conf, logger=logger_kafka),
        dynamic_switch=DynamicSwitch(),
    ):
        logger.info(kafka_conf)
        self.consumer = consumer
        self.last_message = None
        self.paused = False
        self.last_read = None
        self.topics = [
            service_topic,
            user_action_topic,
            user_topic,
            training_topic,
            like_topic,
        ]
        try:
            self.dynamic_switch = dynamic_switch
        except TTransportException as exc:
            logger.error("Cannot connect to HBase: %s", exc)
            logger.error("Exiting Kafka Runner ...")
            raise exc
        self.executor = ThreadPoolExecutor(max_workers=1)

    async def start(self):
        """
        Start consuming messages from Kafka, can be run from FastAPI startup
        event. Will not start working if there is no connection to HBase

        Raise:
            KafkaException: If connection with Kafka fails
        """
        logger.info(self.topics)
        try:
            self.consumer.subscribe(self.topics)
        except cimpl.KafkaException:
            logger.error(
                "Cannot connect to Kafka, some topics not exists: %s", self.topics
            )
            logger.error("Exiting Kafka Runner ...")
            return False

        try:
            loop = asyncio.get_event_loop()
            while True:
                await asyncio.sleep(0)
                msg_future = loop.run_in_executor(
                    self.executor, self.consumer.poll, 1.0
                )
                msg = await msg_future

                if msg is None:
                    continue
                if msg.error():
                    logger.error("Code %s", msg.error().code())
                    if msg.error().code() == -147 or msg.error().code() == str(-147):
                        logger.info("restarting consumer")
                        self.consumer = Consumer(kafka_conf, logger=logger_kafka)

                        self.consumer.subscribe(self.topics)
                        continue
                    logger.info("raising up a error")
                    logger.error(msg.error())

                self.last_message = msg
                self.last_read = pd.Timestamp.now()
                await self.process_message(msg)

        except KeyboardInterrupt:
            logger.warning("%% Aborted by user\n")

        finally:
            self.consumer.close()
            self.paused = True
            logger.info("closed")

    async def process_message(self, message: cimpl.Message) -> bool:
        """
        Process message from Kafka. Just a wrapper for the connector.
        """
        try:
            message_content = json.loads(message.value().decode("utf-8"))
        except (JSONDecodeError, AttributeError):
            logger.error(
                "Message on topic %s is not JSON: %s", message.topic(), message.value()
            )
            return False

        await self.dynamic_switch.add_to_incoming_queue(
            message.topic(), message_content
        )

    def stop(self):
        """
        Stop consuming messages from Kafka, can be run from FastAPI
        shutdown event
        """
        if self.last_message:
            self.consumer.pause(
                [
                    TopicPartition(
                        self.last_message.topic(),
                        self.last_message.partition(),
                        self.last_message.offset(),
                    )
                ]
            )
            self.paused = True
            return True

        return False

    def restart(self):
        """
        Restart Kafka consumer
        """
        if self.paused:
            if self.last_message:
                self.consumer.resume(
                    [
                        TopicPartition(
                            self.last_message.topic(),
                            self.last_message.partition(),
                            self.last_message.offset(),
                        )
                    ]
                )
                self.paused = False
                return "Restarted"

            return "Cannot stop, no last message"

        return "Not paused"

    def health_check(self) -> dict:
        """
        Check if Kafka consumer is running
        """

        response = {
            "status": "DOWN" if self.paused else "UP",
            "last_read": self.last_read,
        }
        return response
