"""
This module is responsible for sending requests to the nearline service and saving the response in a list.
"""
import asyncio
import json
from datetime import datetime
import time

import requests


class Getter:
    def __init__(self, container, nearline_address, save_path):
        self.is_working = True
        self.history = []
        self.start_time = time.time()
        self.start_hour = datetime.now().strftime("%m_%d_%H_%M")
        self.container = container
        # self.id = 'http://localhost:8000/diag'
        # self.id = 'https://nearline-ml-ai-engine-eosc-rs-dev.apps.paas-dev.psnc.pl/diag'
        self.id = nearline_address
        self.save_path = save_path

    def get_request(self):
        """
        Sends a request to the nearline service and returns the response.
        """
        try:
            response = requests.get(self.id)
            response = response.json()["dynamic_switch"]
            response["time"] = time.time() - self.start_time
            response["total_sended"] = self.container.get_total_sended()
            response["total_new_names"] = self.container.get_total_new_names()
        except Exception:
            response = "Not working"
        return response

    async def generate_stats(self):
        """
        Sends requests to the nearline service and saves the response in a list.
        """
        try:
            while self.is_working:
                await asyncio.sleep(0.05)
                temp = self.get_request()
                if type(temp) is dict:
                    self.history.append(temp)

        finally:
            json.dump(
                self.history,
                open(f"{self.save_path}/history_{self.start_hour}.json", "w"),
            )
