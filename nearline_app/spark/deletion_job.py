"""
Job that deletes rows from the table.
"""
from psycopg2.extensions import AsIs
from pyspark.sql import DataFrame


class DeletionJob:
    """
    Job that deletes rows from the table.
    """

    def __init__(self, job_context):
        self.job_context = job_context

    def delete_from_dataframe_postgres(self, delete_dataframe: DataFrame):
        """
        Delete rows from the postgres table
        """
        if self.job_context.postgres_settings.is_used:

            self.job_context.spark_df_to_postgres(
                delete_dataframe, "overwrite", "resources.to_remove"
            )

            delete_query = """DELETE FROM %s
                                       WHERE id IN (SELECT id FROM resources.to_remove)"""
            params = (AsIs(self.job_context.table_name),)
            self.job_context.perform_query(delete_query, params)

    def delete_from_dataframe_hbase(self, delete_dataframe: DataFrame):
        """
        Delete rows from the hbase table
        """
        self.job_context.delete_from_hbase(delete_dataframe)

    def launch(self, delete_path: str):
        """
        Launch deletion job
        """
        self.job_context.logger.info("Start Deleting rows")
        delete_dataset = self.job_context.spark_read_csv(delete_path).select("id")
        self.delete_from_dataframe_postgres(delete_dataset)
        self.delete_from_dataframe_hbase(delete_dataset)
