"""
Module for testing switch class.
"""
import copy
import unittest
from unittest.mock import MagicMock

from nearline_app.settings import settings
from nearline_app.process_switch import ProcessSwitch
from tests.utils_mocks import (
    input_data,
    SingleVisitFunction,
    switch_output,
    # MultiVisitFunction,
)


class TestSwitch(unittest.TestCase):
    """
    Test class, which tests switch class.
    """

    def setUp(self):
        """
        Set up method. Creates.switch object.
        """
        self.switch = ProcessSwitch("Test", MagicMock())
        self.switch.processor = MagicMock()
        # self.switch.hbase_processor = MagicMock()
        self.data = copy.deepcopy(input_data)

    def test_parse_user_action_data(self):
        """
        Test case for the parse_user_action_data function.
        Check if function returns correct data for logged user.
        """
        self.switch.processor.get_processed_user = SingleVisitFunction("test")
        self.data["isAnonymous"] = False
        self.data["aaiUid"] = "test_aai_uid"
        self.data["marketplaceId"] = "test_marketplace_id"
        true_output = switch_output
        self.switch.parse_user_action_data(self.data)

        self.assertEqual(self.switch.processor.get_processed_user.arg1, true_output)

    def test_parse_user_action_no_resource_id(self):
        """
        Test case for the parse_user_action_data function.
        Check if when there is no resource id, function returns None.
        """
        self.data["userActions"][-1].pop("resourceId")
        output = self.switch.parse_user_action_data(self.data)
        self.assertEqual(output, False)

    async def test_parse_data_from_kafka(self):
        """
        Test case for the parse_data_from_kafka function.
        """
        self.switch.parse_user_action_data = SingleVisitFunction(
            settings.KAFKA_TOPIC_UA
        )
        self.switch.parsa_user_event = SingleVisitFunction(settings.KAFKA_TOPIC_USER)
        self.switch.parse_service_data = SingleVisitFunction(
            settings.KAFKA_TOPIC_SERVICE
        )
        self.switch.parse_user_like_data = SingleVisitFunction(
            settings.KAFKA_LIKE_TOPIC
        )

        for topic in [
            settings.KAFKA_TOPIC_UA,
            settings.KAFKA_TOPIC_USER,
            settings.KAFKA_TOPIC_SERVICE,
            settings.KAFKA_LIKE_TOPIC,
            "other",
        ]:
            response = self.switch.parse_data_from_kafka(
                {"id": "data", "sessionId": "data", "aaiId": "data", "visitId": "data"},
                topic,
            )
        self.assertTrue(self.switch.parse_user_action_data.flag)
        self.assertTrue(self.switch.parsa_user_event.flag)
        self.assertTrue(self.switch.parse_service_data.flag)
        self.assertTrue(self.switch.parse_user_like_data.flag)
        self.assertEqual(response, False)

    def test_parse_user_event(self):
        """
        Test case for the parse_user_event function.
        """
        data = {
            "eventType": "test_event_type",
            "aaiId": "test_aai_id",
            "marketplaceId": "test_marketplace_id",
            "scientificDomainValues": [1],
            "categoryValues": [2],
            "accessedServiceValues": [3],
        }
        self.switch.processor.update_user_info = SingleVisitFunction("update")
        self.switch.processor.create_user = SingleVisitFunction("create")
        for event in [
            "update",
            "create",
        ]:
            data["eventType"] = event
            response = self.switch.parsa_user_event(data)
            response = response[0]["data"]
            self.assertEqual(response["process_status"], event)
        data["eventType"] = "other"
        response = self.switch.parsa_user_event(data)
        self.assertFalse(response[0])

    def test_parse_service_data(self):
        """
        Test case for the parse_service_data function.
        """
        data = {
            "eventType": None,
            "id": "test_service_id",
            "name": "test_service_name",
            "scientificDomainValues": [1],
            "relatedServices": [2],
            "categoryValues": [3],
        }
        self.switch.processor.get_service_info = SingleVisitFunction("update")
        data["eventType"] = "update"
        response = self.switch.parse_service_data(data)
        self.assertEqual(response, False)

        data["description"] = "test_service_description"
        self.switch.parse_service_data(data)
        self.assertTrue(self.switch.processor.get_service_info.flag)

    def test_parse_user_like_data(self):
        """
        Test case for the parse_user_like_data function.
        """
        new_data = {
            "action": "temp",
            "resourceId": "test_resource_id",
            "resourceType": "test_resource_type",
            "visitId": "test_visit_id",
            "aaiUid": "test_aai_uid",
            "uniqueId": "test_unique_id",
            "user": None,
        }
        self.switch.processor.get_processed_user.return_value = ["aa"]
        self.switch.processor.get_user_like.return_value = ["bb"]

        new_data["action"] = "like"
        response = self.switch.parse_user_like_data(new_data)
        self.assertEqual(len(response), 2)
        self.assertEqual(response[1][0], "user_profile")

        new_data["action"] = "dislike"
        response = self.switch.parse_user_like_data(new_data)
        self.assertEqual(len(response), 1)
        self.assertEqual(response[0][0], "user_like")

        new_data["action"] = "other"
        response = self.switch.parse_user_like_data(new_data)
        self.assertEqual(response[0], False)

    def test_resource_with_space(self):
        """
        Test case for the parse_user_action_data function.
        """
        self.data["userActions"][1]["resourceId"] = "test resource id"
        a = self.switch.parse_user_action_data(self.data)
        self.assertTrue(1 - a)
