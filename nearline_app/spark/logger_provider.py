"""
Provides logger for spark jobs
"""
# pylint: disable=W0212
from typing import Optional

from pyspark.sql import SparkSession


class LoggerProvider:
    """
    Provides logger for spark jobs
    """

    def get_logger(
        self,
        spark: SparkSession,
        log_level: str = "INFO",
        custom_prefix: Optional[str] = "",
    ):
        """
        Get logger for spark jobs
        """
        spark.sparkContext.setLogLevel(log_level)
        log4j_logger = spark._jvm.org.apache.log4j  # noqa
        return log4j_logger.LogManager.getLogger(custom_prefix + self.__full_name__())

    def __full_name__(self):
        klass = self.__class__
        module = klass.__module__
        if module == "__builtin__":
            return klass.__name__
        return module + "." + klass.__name__
