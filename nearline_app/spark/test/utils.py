"""
Set of functions that are used in tests.
"""
import csv
import os

from pyspark.sql import SparkSession

from nearline_app.spark.job import JobContext
from nearline_app.spark.udf_and_utils import DatabaseSettings


def get_spark_session(request) -> SparkSession:
    """
    Function, that create SparkSession for tests.
    """
    java_opts_list = [
        "-XX:+UseG1GC",
        "-XX:InitiatingHeapOccupancyPercent=40",
        "-XX:MaxGCPauseMillis=500",
    ]
    extra_java_opts = " ".join(java_opts_list)

    spark_session_for_tests = (
        SparkSession.builder.master("local[*]")
        .appName("LocalSparkSessionForTests")
        .config("spark.hadoop.fs.default.name", "hdfs://localhost:54310")
        .config("spark.hadoop.fs.defaultFS", "hdfs://localhost:54310")
        .config("spark.executor.cores", "1")
        .config("spark.executor.instances", "1")
        .config("spark.sql.shuffle.partitions", "1")
        .config("spark.driver.memory", "2g")
        .config("spark.driver.maxResultSize", "1g")
        .config("spark.executor.memory", "2g")
        .config("spark.driver.extraJavaOptions", extra_java_opts)
        .config("spark.executor.extraJavaOptions", extra_java_opts)
        .config("spark.sql.execution.pythonUDF.arrow.enabled", "true")
        .getOrCreate()
    )
    spark_session_for_tests.sparkContext.setLogLevel("WARN")
    request.addfinalizer(spark_session_for_tests.stop)
    return spark_session_for_tests


def get_path_to_test_files():
    """
    Function, that return path to test files.
    """
    if "nearline_app" in os.getcwd():
        return os.getcwd()
    return os.getcwd() + r"\nearline_app\spark\test"


def setup_job_context(spark_session):
    """
    Function, that create JobContext for tests.
    """
    postgres_settings = DatabaseSettings("postgres", "postgres", "postgres", "5432")
    hbase_settings = DatabaseSettings("hbase", "hbase", "hbase", "2181")
    job_context = JobContext(
        spark_session, postgres_settings, hbase_settings, "services", "bert"
    )
    return job_context


def generate_mapping(mapping_path):
    """
    Function, that generate mapping for tests.
    """
    mapping = {}
    with open(mapping_path, "r", encoding="utf-8") as file:
        reader = csv.reader(file, delimiter=";")
        headers = next(reader)
        print(f"Headers: {headers}")
        for row in reader:
            mapping[row[0]] = row[1]

    return mapping
