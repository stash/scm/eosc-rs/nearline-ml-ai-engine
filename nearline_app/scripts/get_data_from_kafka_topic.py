import time
import json
import argparse

from confluent_kafka import Consumer

from nearline_app.settings import get_settings
from nearline_app.utils import kafka_conf

settings = get_settings(".env")

data = []


def message_to_csv(kafka_message):
    data.append(json.loads(kafka_message.decode("utf-8")))


parser = argparse.ArgumentParser(description="Initialize users tables")

parser.add_argument(
    "-t",
    "--topic",
    help="Kafka topic from which read messages",
    required=True,
)
parser.add_argument(
    "-of",
    "--output_file",
    help="Name of the output file",
    default="output.json",
)

if __name__ == "__main__":
    args = parser.parse_args()

    consumer = Consumer(kafka_conf)
    consumer.subscribe([args.topic])
    last_message_time = time.time()
    while True:
        message = consumer.poll(timeout=1.0)
        if message is None:
            if time.time() - last_message_time > 5:
                break
            continue

        message_to_csv(message.value())
        last_message_time = time.time()

    consumer.close()

    with open(args.output_file, "w+", encoding="utf8") as outfile:
        json.dump(data, outfile)
