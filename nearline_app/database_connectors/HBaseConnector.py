"""
A module with a class for connecting to HBase database.

This class can:
- connect to HBase database
- get resource data from HBase database
- insert user data into HBase database
- update user data in HBase database
- get user data from HBase database
- update resource data in HBase database
- check availability of resource tables in HBase database
"""
import json
import logging
import socket
from typing import List, Union, Dict, Any, Callable

import pandas as pd
from thriftpy2.transport import TTransportException

from nearline_app.database_connectors.BetterConnection import BetterConnection
from nearline_app.database_connectors.DataBaseConnector import DataBaseConnector
from nearline_app.settings import settings
from nearline_app.utils import (
    resource_types,
    clean_data,
    transform,
    convert_string,
    get_empty_user,
    transform_languages,
)

logger = logging.getLogger(__name__)


class HBaseConnector(DataBaseConnector):
    """
    A class for connecting to HBase database.
    """

    def __init__(self, clean_data_function: Callable = clean_data):
        self.hbase_connection = None
        self.open_pool()
        try:
            self.current_tables = self.get_current_tables()
        except TypeError:
            logger.error("Cannot connect with HBASE")
            self.current_tables = None
        self.clean_data = clean_data_function

    def get_current_tables(self):
        """
        Get the current tables with resources from the metadata table.

        Returns:
            a dictionary with resource type as a key and table name as a value.

        """
        current_tables = {}

        out = self.hbase_connection.rows(settings.HBASE_METADATA, resource_types)
        for resource_typ, values in out:
            current_tables[resource_typ.decode("utf-8")] = values[
                b"table_name:table_name"
            ].decode("utf-8")
        return current_tables

    def open_pool(self):
        """
        Open connection pool to Hbase.
        """
        if not self.hbase_connection:
            logger.info("Hbase open connection pool")
            try:
                self.hbase_connection = BetterConnection(
                    host=settings.HBASE_SERVER, port=int(settings.HBASE_PORT)
                )
            except socket.timeout as exc:
                logger.error("Timeout while connecting to Hbase: %s", exc)
                raise exc
            except TTransportException as exc:
                logger.error("Error while connecting to Hbase: %s", exc)
                raise exc

    def get_embeddings(
        self, table_name: str, resources_id: List[str]
    ) -> Union[pd.DataFrame, None]:
        """
        Get the set of embeddings from given resource type.

        Arguments:
            table_name (str): table name from which we want to get
            resources_id (List[str]): list of resources id of which
                                                we want to get

        Returns:
            embeddings of resources or None if there is no embeddings in the
            table that match the given resources id
        """
        rows = self.get_rows_from_table(
            self.current_tables[table_name],
            resources_id,
            columns=["embeddings:embeddings"],
        )

        if rows:
            embedding_df = pd.DataFrame.from_records(rows, columns=["id", "embeddings"])
            try:
                embedding_df["embeddings"] = embedding_df["embeddings"].apply(transform)
                embedding_df["id"] = embedding_df["id"].apply(convert_string)
            except ValueError as exc:
                logger.error(
                    "Some error with decoding data: %s %s %s", table_name, rows, exc
                )
            embedding_df.set_index("id", inplace=True)
            return embedding_df

        return None

    def get_embeddings_with_languages(
        self, table_name: str, resources_id: List[str]
    ) -> Union[pd.DataFrame, None]:
        """
        Get the set of embeddings and languages from given resource type.

        Arguments:
            table_name (str): table name from which we want to get
            resources_id (List[str]): list of resources id of which
                                                we want to get

        Returns:
            embeddings of resources or None if there is no embeddings in the
            table that match the given resources id
        """
        rows = self.get_rows_from_table(
            self.current_tables[table_name],
            resources_id,
            columns=["embeddings:embeddings", "resource_info:language_prediction"],
        )
        if rows:
            embedding_df = pd.DataFrame.from_records(
                rows, columns=["id", "embeddings_temp"]
            )

            try:
                embedding_df["embeddings"] = embedding_df["embeddings_temp"].apply(
                    transform
                )

                embedding_df["id"] = embedding_df["id"].apply(convert_string)
                embedding_df["language"] = embedding_df["embeddings_temp"].apply(
                    transform_languages
                )
            except ValueError as exc:
                logger.error(
                    "Some error with decoding data: %s %s %s", table_name, rows, exc
                )
            embedding_df.set_index("id", inplace=True)
            embedding_df.drop(columns=["embeddings_temp"], inplace=True)
            return embedding_df

        return None

    def get_rows_from_table(self, table_name, ids, columns=None) -> Union[List, None]:
        """
        Get rows from given table.

        Arguments:
            table_name (str): table name from which we want to get
            ids (List[str]): list of ids of which we want to get
            columns (List[str]): list of columns which we want to get

        Returns:
            rows from given table or None if there is no table of a given name
        """
        try:
            rows = self.hbase_connection.rows(
                table_name,
                ids,
                columns=columns,
            )
            return rows
        except KeyError:
            logger.error("Table %s does not exist", table_name)
            return None

    def single_update_user(
        self, table_name: str, user_id: str, column: str, value: Union[str, bytes]
    ) -> None:
        """
        Update user entity (certain(single) column) in the database.

        Arguments:
            table_name (str): table name in which we want to update user
            user_id (str): user id which we want to update
            column (str): column name which we want to update
            value (Union[str, bytes]): new value for given user in given column
        """
        self.hbase_connection.put(table_name, user_id, {column: value})

    def update_user_with_embeddings(
        self, table_name: str, update_data: Dict[str, Any]
    ) -> None:
        """
        Update user entity in the database(resources and embeddings for these resources).

        Arguments:
            table_name (str): table name in which we want to update user
            update_data (Dict[str, Any]): data which we want to insert:
                id (str): user id which we want to insert
                new_data (str): new resource
                new_data_column (str): column name which we want to
                                        update with new_data
                new_data_emb (bytes): embedding of new resource
                new_data_emb_column (str): column name which we want to
                                            update with new_data_emb
        """

        self.hbase_connection.put(
            table_name,
            update_data["id"],
            {
                update_data["new_data_column"]: update_data["new_data"],
                update_data["new_data_emb_column"]: update_data["new_data_emb"],
            },
        )

    def update_user(self, table_name: str, update_data: Dict[str, Any]) -> None:
        """
        Update user entity in the database(resources).

        Arguments:
            table_name (str): table name in which we want to update user
            update_data (Dict[str, Any]): data which we want to insert:
                id (str): user id which we want to insert
                new_data (str): new resource list
                new_data_column (str): column name which we want to
                                        update with new_data
        """
        self.hbase_connection.put(
            table_name,
            update_data["id"],
            {
                update_data["new_data_column"]: update_data["new_data"],
            },
        )

    def insert_new_user_with_embeddings(
        self, table_name: str, insert_data: Dict[str, Any]
    ) -> None:
        """
        Insert a new user with given data and embedding for this data.

        Arguments:
            table_name (str): table name in which we want to insert user
            insert_data (Dict[str, Any]): data which we want to insert:
                id (str): user id which we want to insert
                new_data (str): new resource
                new_data_column (str): column name which we want to
                                        update with new_data
                new_data_emb (bytes): embedding of new resource
                new_data_emb_column (str): column name which we want to
                                            update with new_data_emb
        """
        empty_user = get_empty_user()
        empty_user[insert_data["new_data_column"]] = insert_data["new_data"]
        empty_user[insert_data["new_data_emb_column"]] = insert_data["new_data_emb"]

        self.hbase_connection.put(table_name, insert_data["id"], empty_user)

    def insert_new_user(self, table_name: str, insert_data: Dict[str, Any]) -> None:
        """
        Insert a new user with given data.

        Arguments:
            table_name (str): table name in which we want to insert user
            insert_data (Dict[str, Any]): data which we want to insert:
                id (str): user id which we want to insert
                new_data (str): new resource
                new_data_column (str): column name which we want to
                                        update with new_data
        """
        empty_user = get_empty_user()
        empty_user[insert_data["new_data_column"]] = insert_data["new_data"]

        self.hbase_connection.put(table_name, insert_data["id"], empty_user)

    def get_user(self, user_id: str, table_name: str) -> Union[None, Dict[str, Any]]:
        """
        Get user entity from the database, and change it into
        dict in form column-name: value

        Return None, if no user was found.

        Arguments:
            user_id (str): user id which we want to get
            table_name (str): table name from which take user

        Returns:
             user entity in form od dict, or None
        """
        row = self.hbase_connection.row(table_name, user_id)
        if row is None or row == {}:
            return None
        row = self.clean_data(row, user_id)
        return row

    def update_resource(
        self, service_id: str, categories: str, desc_embed: bytes, resource_type: str
    ) -> None:
        """
        Update service entity in the database.
        """
        self._insert_resource(service_id, categories, desc_embed, resource_type)

    def insert_new_resource(
        self, resource_id: str, categories: str, desc_embed: bytes, resource_type: str
    ) -> None:
        """
        Create service entity in the database.
        """
        self._insert_resource(resource_id, categories, desc_embed, resource_type)

    def _insert_resource(
        self, resource_id: str, categories: str, desc_embed: bytes, resource_type: str
    ) -> None:
        """
        Create service entity in the database.
        """
        insert_data = {
            "embeddings:embeddings": desc_embed,
            "resource_info:bestaccessright": json.dumps([]),
            "resource_info:categories": categories,
            "resource_info:isvalid": json.dumps(str(True)),
            "resource_info:language_prediction": json.dumps("en"),
            "resource_info:relations": json.dumps(""),
        }
        self.hbase_connection.put(
            self.current_tables[resource_type], resource_id, insert_data
        )

    def insert_new_empty_user(self, table_name: str, user_id: str) -> None:
        """
        Insert a new empty user with given id.

        Arguments:
            table_name (str): table name in which we want to insert user
            user_id (str): user id which we want to insert
        """
        insert_data = get_empty_user()
        self.hbase_connection.put(table_name, user_id, insert_data)

    def health_check(self, counter=0) -> dict:
        """
        Health check. Check if the connection is working.
        """
        response = {"status": "DOWN"}
        try:
            all_tables = [
                convert_string(table) for table in self.hbase_connection.tables()
            ]
            tables_status = {}
            for resource, table_name in self.current_tables.items():
                tables_status[resource] = table_name in all_tables
            response["status"] = "UP"
            response["tables"] = tables_status

        except TypeError as error:
            if counter < 1:
                self.health_check(counter + 1)
            else:
                logger.error("Cannot connect with HBASE: %s", error)
                response["error"] = "Error while connecting to Hbase:" + str(error)
        except Exception as error:
            logger.error("Cannot connect with HBASE: %s", error)
            response["error"] = "Error while connecting to Hbase:" + str(error)

        return response
