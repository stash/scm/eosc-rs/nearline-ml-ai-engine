"""
This module contains tests for the AddingJob class.
"""
import os
import sys
from unittest.mock import Mock

import pytest
from pyspark.sql import SparkSession
from pyspark.sql.functions import array, lit

from nearline_app.spark.adding_job import AddingJob
import nearline_app.spark.udf_and_utils as utils
from nearline_app.spark.test.utils import (
    get_spark_session,
    setup_job_context,
    get_path_to_test_files,
    generate_mapping,
)

os.environ["PYSPARK_PYTHON"] = sys.executable
os.environ["PYSPARK_DRIVER_PYTHON"] = sys.executable


@pytest.fixture(scope="session", name="spark_session")
def fixture_spark_session(request) -> SparkSession:
    """
    Fixture for creating a SparkSession
    """
    return get_spark_session(request)


@pytest.mark.usefixtures("spark_session")
def test_map_descriptions(spark_session):
    """
    Test the map_descriptions function.
    """
    job_context = setup_job_context(spark_session)
    mapping_path = get_path_to_test_files() + r"\mapping.csv"

    job_context.broadcast_mapping = job_context.spark.sparkContext.broadcast(
        generate_mapping(mapping_path)
    )
    file_path = "file:///" + get_path_to_test_files() + r"\test_data.csv"

    test_df = job_context.spark_read_csv(file_path)
    assert "yamadae" in test_df.select("descriptions").take(1)[0][0]
    app = AddingJob(job_context)
    output = app.map_descriptions(test_df)
    assert "Schwanniomyces" in output.select("descriptions").take(1)[0][0]


@pytest.mark.usefixtures("spark_session")
def test_organize_columns(spark_session):
    """
    Test the organize_columns function.
    """
    job_context = setup_job_context(spark_session)
    file_path = "file:///" + get_path_to_test_files() + r"\test_data.csv"

    test_df = job_context.spark_read_csv(file_path)
    app = AddingJob(job_context)
    output = app.organize_columns(test_df)
    important_columns = {
        "mainTitles",
        "categories",
        "bestaccessright",
        "keywordsMerged",
        "descriptions",
    }

    assert len(important_columns - set(output.columns)) == 0
    assert output.select("isvalid").take(1)[0][0]


@pytest.mark.usefixtures("spark_session")
def test_add_joined_column(spark_session):
    """
    Test the add_joined_column function.
    """
    job_context = setup_job_context(spark_session)
    file_path = "file:///" + get_path_to_test_files() + r"\test_data.csv"

    test_df = job_context.spark_read_csv(file_path)
    app = AddingJob(job_context)
    title = test_df.select("mainTitles").take(1)[0][0]
    keywords = test_df.select("keywordsMerged").take(1)[0][0]
    description = test_df.select("descriptions").take(1)[0][0]
    all_columns = [title, keywords, description]

    output = app.add_joined_column(test_df)
    joined_column = output.select("joined_column").take(1)[0][0]
    assert " ".join([str(i or " ") for i in all_columns]) == joined_column


@pytest.mark.usefixtures("spark_session")
def test_add_language_column(spark_session):
    """
    Test the add_language_column function.
    """
    job_context = setup_job_context(spark_session)
    file_path = "file:///" + get_path_to_test_files() + r"\test_data.csv"

    test_df = job_context.spark_read_csv(file_path)
    app = AddingJob(job_context)
    output = app.add_language_column(test_df)
    assert "language_prediction" in output.columns
    assert output.select("language_prediction").take(1)[0][0] == "en"


@pytest.mark.usefixtures("spark_session")
def test_record_processing(spark_session):
    """
    Test the record_processing function.
    """
    job_context = setup_job_context(spark_session)
    file_path = "file:///" + get_path_to_test_files() + r"\test_data.csv"

    test_df = job_context.spark_read_csv(file_path)
    new_column = array(*[lit(0) for _ in range(0, 100)])
    returned_df = test_df.withColumn("finished_embeddings", new_column)

    app = AddingJob(job_context)
    embedder = Mock()
    embedder.transform = Mock(return_value=returned_df)
    caller = Mock()
    caller.fit = Mock(return_value=embedder)
    app.pipeline = caller

    output = app.record_processing(test_df)
    assert "embeddings" in output.columns
    assert (set(utils.columns_to_drop_2) & set(output.columns)) == {"embeddings"}
