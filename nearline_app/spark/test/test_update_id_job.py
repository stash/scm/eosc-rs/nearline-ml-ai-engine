"""
This module contains the test cases for the UpdateIdJob class
"""
import os
import sys

import pytest
from pyspark.sql import SparkSession

from nearline_app.spark.test.utils import (
    get_spark_session,
    setup_job_context,
    get_path_to_test_files,
)
from nearline_app.spark.update_id_job import UpdateIdJob

os.environ["PYSPARK_PYTHON"] = sys.executable
os.environ["PYSPARK_DRIVER_PYTHON"] = sys.executable


@pytest.fixture(scope="session", name="spark_session")
def fixture_spark_session(request) -> SparkSession:
    """
    Fixture for creating a SparkSession
    """
    return get_spark_session(request)


@pytest.mark.usefixtures("spark_session")
def test_update_user_profile(spark_session):
    """
    Test the update_user_profile function.
    """
    job_context = setup_job_context(spark_session)
    file_path = "file:///" + get_path_to_test_files() + r"\test_updateid_data.csv"
    user_path = "file:///" + get_path_to_test_files() + r"\test_user.csv"

    user_df = job_context.spark_read_csv(user_path)
    old_user = user_df.select("services:visited").take(1)
    assert "abc" in old_user[0][0]
    app = UpdateIdJob(job_context, "user,user2")
    test_df = app.make_exchange_dataset(file_path)
    assert "id" in test_df.columns
    output = app.update_user_profile(user_df, test_df)
    new_user = output.select("services:visited").take(1)
    assert "abc" not in new_user[0][0]
