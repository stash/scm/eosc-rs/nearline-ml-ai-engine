"""
This module contains class AddingJob, which is responsible for updating rows in the table.
"""
# pylint: disable=E0401

from psycopg2.extensions import AsIs
from pyspark.sql import DataFrame

import udf_and_utils as utils
from adding_job import AddingJob


class UpdateContentJob(AddingJob):
    """
    Job that updates rows in the table.
    """

    def __init__(self, job_context):
        super().__init__(job_context)
        self.pipeline = None

    def update_postgres_rows(self, df_to_write: DataFrame, mode: str):
        """
        Update rows in the main table ny replacing them with data from the temporary table
        `resources.update_content` using id of resources.
        """
        self.job_context.spark_df_to_postgres(
            df_to_write, mode, "resources.update_content"
        )

        update_query = """
                           UPDATE %s
                           SET embeddings = resources.update_content.embeddings,
                               categories = resources.update_content.categories,
                               bestaccessright = resources.update_content.bestaccessright,
                               isvalid = resources.update_content.isvalid,
                               relations = resources.update_content.relations,
                               language_prediction = resources.update_content.language_prediction
                           FROM resources.update_content
                           WHERE %s.id = resources.update_content.id
                       """

        params = (
            AsIs(self.job_context.table_name),
            AsIs(self.job_context.table_name),
        )

        self.job_context.perform_query(update_query, params)

    def launch(self, changed_path: str, mode: str):
        """
        Launches the job of updating rows in the table.
        """
        self.job_context.logger.info("Start Updating rows content")
        update_dataset = self.job_context.spark_read_csv(changed_path)

        self.pipeline = self.build_pipeline()
        df_bert = self.record_processing(update_dataset)

        df_to_write = df_bert.select(*utils.columns_to_select)
        df_to_write.persist(self.job_context.storage_level)
        self.update_postgres_rows(df_to_write, mode)
        self.job_context.spark_df_to_hbase(df_to_write)
        df_to_write.unpersist()
