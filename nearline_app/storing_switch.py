"""
Module with class that is responsible for storing data to database.
"""
import json
import logging
from typing import Dict

from nearline_app.settings import settings

logger = logging.getLogger(__name__)


class StoringSwitch:
    """
    Class that is responsible for storing data to database.
    """

    def __init__(self, name, database_connector):

        self.name = name
        self.counter = 0
        self.is_database_connected = False
        self.database_connector = database_connector

        if self.database_connector.current_tables is not None:
            self.is_database_connected = True

    def parse_data_to_database_connector(
        self,
        data,
        topic: str,
    ) -> None:
        """
        Parse data from Kafka and send it to processors.
        Make different parsing in dependency from topic of message.
        """
        if topic == settings.KAFKA_TOPIC_UA:
            user_data, proces_status, table_name = data
            logger.info(
                "Start storing user action %s on %s", user_data["id"], self.name.lower()
            )
            self.store_user(user_data, proces_status, table_name)

        elif topic == settings.KAFKA_TOPIC_USER:
            logger.info(
                "Start storing user event: %s on %s",
                data["data"]["aaiUid"],
                self.name.lower(),
            )
            self.store_user_event(data)

        elif topic == settings.KAFKA_TOPIC_SERVICE:
            logger.info(
                "Start storing service event: %s on %s", data[0], self.name.lower()
            )
            self.store_resource(data, "services")

        elif topic == settings.KAFKA_TOPIC_TRAINING:
            logger.info(
                "Start storing training event: %s on %s", data[0], self.name.lower()
            )
            self.store_resource(data, "trainings")

        elif topic == settings.KAFKA_LIKE_TOPIC:
            event_type, user_data = data
            self.store_user_like(event_type, user_data)
        else:
            logger.warning("This topic: %s is not supported by this switch", topic)

    def store_user(self, user_data, proces_status, table_name):
        """
        Store user data to database.
        """
        if proces_status == "new":
            self.database_connector.insert_new_user_with_embeddings(
                table_name, user_data
            )
        elif proces_status == "update":
            self.database_connector.update_user_with_embeddings(table_name, user_data)
        else:
            logger.warning("Unknown process status: %s", proces_status)

    def store_resource(self, data, resource_type: str):
        """
        Store resource data to database.
        """
        resource_id, new_categories, new_desc, proces_status = data
        if proces_status == "new":
            self.database_connector.insert_new_resource(
                resource_id, new_categories, new_desc, resource_type
            )
        elif proces_status == "update":
            self.database_connector.update_resource(
                resource_id, new_categories, new_desc, resource_type
            )
        else:
            logger.warning("Unknown process status: %s", resource_type)

    def store_user_like(self, event_type, user):
        """
        Store user like data to database.
        """
        if event_type == "user_like":
            logger.info(
                "Start storing %s event: %s on %s",
                event_type,
                user["data"]["id"],
                self.name.lower(),
            )
            # self.processor.user_like(user_data)
            if user["process_status"] == "new":
                self.database_connector.insert_new_user(
                    user["table_name"], user["data"]
                )
            elif user["process_status"] == "update":
                self.database_connector.update_user(user["table_name"], user["data"])
            else:
                logger.warning("Unknown process status: %s", user["process_status"])
        elif event_type == "user_profile":
            user_data, proces_status, table_name = user
            logger.info(
                "Start storing user action %s to %s", user_data["id"], self.name.lower()
            )
            self.store_user(user_data, proces_status, table_name)
        else:
            logger.warning("Unknown event type: %s", event_type)

    def store_user_event(self, user_data: Dict[str, Dict]):
        """
        Store user event data to database.
        """
        data = user_data["data"]
        user_tables = [settings.LOGGED_USERS_TABLE, settings.LOGGED_USERS_AAI_TABLE]
        user_types = ["marketplaceId", "aaiUid"]

        if data["process_status"] == "update":
            for table_name, user_type in zip(
                user_tables,
                user_types,
            ):
                if user_data[user_type] is None:
                    logger.warning(
                        "User %s (%s) is not logged in %s table",
                        data[user_type],
                        user_type,
                        table_name,
                    )
                    self.database_connector.insert_new_empty_user(
                        table_name, data[user_type]  # type: ignore
                    )
                for columns_type in ["categories", "scientific_domains"]:
                    self.database_connector.single_update_user(
                        table_name,
                        data[user_type],
                        "user_info:" + columns_type,
                        json.dumps(data[columns_type]),
                    )

        elif data["process_status"] == "create":
            for table_name, user_type in zip(
                user_tables,
                user_types,
            ):
                if user_data[user_type] is None:
                    self.database_connector.insert_new_empty_user(
                        table_name, data[user_type]  # type: ignore
                    )
        else:
            logger.warning("Unknown process status: %s", data["process_status"])
